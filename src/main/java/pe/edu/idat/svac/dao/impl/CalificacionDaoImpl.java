package pe.edu.idat.svac.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import pe.edu.idat.svac.dao.CalificacionDao;
import pe.edu.idat.svac.model.Calificacion;

@Component
public class CalificacionDaoImpl implements CalificacionDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	private SqlParameterSource getSqlParameterByModel(Calificacion calificacion) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		if (calificacion != null) {

			paramSource.addValue("id", calificacion.getId());
			paramSource.addValue("documentoVentaId", calificacion.getDocumentoVentaId());
			paramSource.addValue("empleadoId", calificacion.getEmpleadoId());
			paramSource.addValue("calificacion", calificacion.getCalificacion());
		}
		return paramSource;
	}

	private static final class CalificacionMapper implements RowMapper<Calificacion> {

		@Override
		public Calificacion mapRow(ResultSet rs, int rowNum) throws SQLException {
			Calificacion calificacion = new Calificacion();

			calificacion.setId(rs.getInt(1));
			calificacion.setDocumentoVentaId(rs.getInt(2));
			calificacion.setEmpleadoId(rs.getInt(3));
			calificacion.setCalificacion(rs.getInt(4));

			return calificacion;
		}
	}

	@Override
	public void add(Calificacion calificacion) {

		String sql = "INSERT INTO `calificacion` (`idDocumentoVenta`, `idEmpleado`, `calificacion`) VALUES (:documentoVentaId, :empleadoId, :calificacion );";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(calificacion));
	}

	@Override
	public void update(Calificacion calificacion) {

		String sql = "UPDATE `calificacion` SET `calificacion` = :calificacion WHERE `idCalificacion` = :id";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(calificacion));
	}

	@Override
	public Calificacion getById(int id) throws Exception {

		String sql = "SELECT * FROM `calificacion` WHERE idCalificacion= :id LIMIT 1";
		return namedParameterJdbcTemplate.queryForObject(sql, getSqlParameterByModel(new Calificacion(id)),
				new CalificacionMapper());
	}

	@Override
	public Calificacion getByVentaId(int id) throws Exception {

		String sql = "SELECT * FROM `calificacion` WHERE idDocumentoVenta = :documentoVentaId LIMIT 1";

		Calificacion score = new Calificacion();
		score.setDocumentoVentaId(id);

		return namedParameterJdbcTemplate.queryForObject(sql, getSqlParameterByModel(score), new CalificacionMapper());
	}

	@Override
	public void calcularPromedio(Calificacion calificacion) throws Exception {

		String sql = "{ call sp_Calificacion_calcularPromedio(:empleadoId) }";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(calificacion));

	}

}
