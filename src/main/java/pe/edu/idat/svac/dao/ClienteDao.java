package pe.edu.idat.svac.dao;

import java.util.List;
import pe.edu.idat.svac.model.Cliente;

public interface ClienteDao {

	public List<Cliente> listAll();

	public void add(Cliente cliente);

	public void update(Cliente cliente);
	
	public void updateEstado(Cliente cliente);

	public Cliente getById(int id) throws Exception;

}
