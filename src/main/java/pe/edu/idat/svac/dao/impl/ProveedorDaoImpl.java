package pe.edu.idat.svac.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import pe.edu.idat.svac.dao.ProveedorDao;
import pe.edu.idat.svac.model.Proveedor;

@Component
public class ProveedorDaoImpl implements ProveedorDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	private SqlParameterSource getSqlParameterByModel(Proveedor proveedor) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		if (proveedor != null) {

			paramSource.addValue("id", proveedor.getId());
			paramSource.addValue("codigo", proveedor.getCodigo());
			paramSource.addValue("tipo", proveedor.getTipo());
			paramSource.addValue("nombre", proveedor.getNombre());
			paramSource.addValue("documento", proveedor.getDocumento());
			paramSource.addValue("numeroDocumento", proveedor.getNumeroDocumento());
			paramSource.addValue("departamento", proveedor.getDepartamento());
			paramSource.addValue("provincia", proveedor.getProvincia());
			paramSource.addValue("distrito", proveedor.getDistrito());
			paramSource.addValue("direccion", proveedor.getDireccion());
			paramSource.addValue("telefono", proveedor.getTelefono());
			paramSource.addValue("email", proveedor.getEmail());
			paramSource.addValue("numeroCuenta", proveedor.getNumeroCuenta());
			paramSource.addValue("estado", proveedor.getEstado());
		}
		return paramSource;
	}

	private static final class ProveedorMapper implements RowMapper<Proveedor> {

		@Override
		public Proveedor mapRow(ResultSet rs, int rowNum) throws SQLException {
			Proveedor proveedor = new Proveedor();

			proveedor.setId(rs.getInt(1));
			proveedor.setCodigo(rs.getString(2));
			proveedor.setTipo(rs.getString(3));
			proveedor.setNombre(rs.getString(4));
			proveedor.setDocumento(rs.getString(5));
			proveedor.setNumeroDocumento(rs.getString(6));
			proveedor.setDepartamento(rs.getString(7));
			proveedor.setProvincia(rs.getString(8));
			proveedor.setDistrito(rs.getString(9));
			proveedor.setDireccion(rs.getString(10));
			proveedor.setTelefono(rs.getString(11));
			proveedor.setEmail(rs.getString(12));
			proveedor.setNumeroCuenta(rs.getString(13));
			proveedor.setEstado(rs.getInt(14));
			return proveedor;

		}
	}

	@Override
	public List<Proveedor> listAll() {
		String sql = "SELECT * FROM `proveedor`";
		List<Proveedor> list = namedParameterJdbcTemplate.query(sql, new ProveedorMapper());
		return list;
	}

	@Override
	public void add(Proveedor proveedor) {
		String sql = "{call prcProveedorInsert(:nombre, :documento, :numeroDocumento, :departamento, :provincia, :distrito, :direccion, :telefono, :email, :numeroCuenta)}";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(proveedor));

	}

	@Override
	public void update(Proveedor proveedor) {
		String sql = "{call prcProveedorUpdate(:id, :nombre, :documento, :numeroDocumento, :departamento, :provincia, :distrito, :direccion , :telefono, :email, :numeroCuenta, :estado)}";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(proveedor));
	}
	
	@Override
	public void updateEstado(Proveedor proveedor) {
		String sql =  "UPDATE `proveedor` SET estado = :estado WHERE idProveedor= :id";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(proveedor));
	}

	@Override
	public Proveedor getById(int id) throws Exception {
		String sql = "SELECT * FROM `proveedor` WHERE idProveedor= :id";

		return namedParameterJdbcTemplate.queryForObject(sql, getSqlParameterByModel(new Proveedor(id)),
				new ProveedorMapper());
	}
}
