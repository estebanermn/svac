package pe.edu.idat.svac.dao;

import java.util.List;
import pe.edu.idat.svac.model.Unidad;

public interface UnidadDao {

	public List<Unidad> listAll();

	public void add(Unidad unidad);

	public void update(Unidad unidad);

	public void updateEstado(Unidad unidad);

	public Unidad getById(int id) throws Exception;

}
