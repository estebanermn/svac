package pe.edu.idat.svac.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import pe.edu.idat.svac.dao.ArticuloDao;
import pe.edu.idat.svac.model.Articulo;;

@Component
public class ArticuloDaoImpl implements ArticuloDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	private SqlParameterSource getSqlParameterByModel(Articulo articulo) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		if (articulo != null) {

			paramSource.addValue("id", articulo.getId());
			paramSource.addValue("categoriaId", articulo.getCategoriaId());
			paramSource.addValue("unidadId", articulo.getUnidadId());
			paramSource.addValue("codigo", articulo.getCodigo());
			paramSource.addValue("nombre", articulo.getNombre());
			paramSource.addValue("descripcion", articulo.getDescripcion());
			paramSource.addValue("estado", articulo.getEstado());
			paramSource.addValue("stock", articulo.getStock());
			paramSource.addValue("precio", articulo.getPrecio());
			paramSource.addValue("precioProveedor", articulo.getPrecioProveedor());
			paramSource.addValue("categoria", articulo.getCategoria());
			paramSource.addValue("unidad", articulo.getUnidad());

		}
		return paramSource;
	}

	private static final class ArticuloMapper implements RowMapper<Articulo> {

		@Override
		public Articulo mapRow(ResultSet rs, int rowNum) throws SQLException {
			Articulo articulo = new Articulo();

			articulo.setId(rs.getInt(1));
			articulo.setCategoriaId(rs.getInt(2));
			articulo.setUnidadId(rs.getInt(3));
			articulo.setCodigo(rs.getString(4));
			articulo.setNombre(rs.getString(5));
			articulo.setDescripcion(rs.getString(6));
			articulo.setEstado(rs.getInt(7));
			articulo.setStock(rs.getInt("stock"));
			articulo.setPrecio(rs.getDouble("precio"));
			articulo.setPrecioProveedor(rs.getDouble("precioProveedor"));
			articulo.setCategoria(rs.getString("categoria"));
			articulo.setUnidad(rs.getString("unidad"));

			return articulo;
		}
	}

	@Override
	public List<Articulo> listAll() {
		String sql = "{call prcArticuloSelect()}";
		List<Articulo> list = namedParameterJdbcTemplate.query(sql, new ArticuloMapper());
		return list;
	}

	@Override
	public void add(Articulo articulo) {
		String sql = "{call prcArticuloInsert(:categoriaId, :unidadId, :nombre, :descripcion, :precio)}";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(articulo));
	}

	@Override
	public void update(Articulo articulo) {

		String sql = "UPDATE `articulo` SET `idCategoria`=:categoriaId,`idUnidad`=:unidadId,`nombre`=:nombre,`descripcion`=:descripcion,`estado`=:estado, `precio`=:precio, `precioProveedor`=:precioProveedor WHERE  `idArticulo`=:id";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(articulo));
	}
	
	@Override
	public void updateEstado(Articulo articulo) {

		String sql = "UPDATE `articulo` SET `estado`= :estado WHERE `idArticulo`= :id";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(articulo));
	}

	@Override
	public Articulo getById(int id) throws Exception {
		String sql = "SELECT a.*, c.nombre AS categoria, u.nombre AS unidad FROM articulo a JOIN categoria c ON a.idCategoria = c.idCategoria JOIN unidad_medida u ON a.idUnidad = u.idUnidad WHERE a.idArticulo = :id ";

		return namedParameterJdbcTemplate.queryForObject(sql, getSqlParameterByModel(new Articulo(id)),
				new ArticuloMapper());
	}

	@Override
	public void aumentarStock(int articuloId, int cantidad) throws Exception {

		Articulo articulo = new Articulo(articuloId);
		articulo.setStock(cantidad);

		String sql = "{call  sp_Articulo_Aumentar_Stock(:id, :stock)}";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(articulo));

	}

	@Override
	public void descontarStock(int articuloId, int cantidad) throws Exception {

		Articulo articulo = new Articulo(articuloId);
		articulo.setStock(cantidad);

		String sql = "{call sp_Articulo_Descontar_Stock(:id, :stock)}";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(articulo));

	}
}
