package pe.edu.idat.svac.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import pe.edu.idat.svac.dao.PagoDao;
import pe.edu.idat.svac.model.Pago;

@Component
public class PagoDaoImpl implements PagoDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	private SqlParameterSource getSqlParameterByModel(Pago pago) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		if (pago != null) {

			paramSource.addValue("id", pago.getId());
			paramSource.addValue("documentoVentaId", pago.getDocumentoVentaId());
			paramSource.addValue("fecha", pago.getFecha());
			paramSource.addValue("monto", pago.getMonto());
			paramSource.addValue("tipoPago", pago.getTipoPago());

		}
		return paramSource;
	}

	private static final class PagoMapper implements RowMapper<Pago> {

		@Override
		public Pago mapRow(ResultSet rs, int rowNum) throws SQLException {
			Pago pago = new Pago();

			pago.setId(rs.getInt(1));
			pago.setDocumentoVentaId(rs.getInt(2));
			pago.setFecha(rs.getDate(3));
			pago.setMonto(rs.getDouble(4));
			pago.setTipoPago(rs.getString(5));

			return pago;
		}
	}

	@Override
	public List<Pago> listAll() {
		String sql = "SELECT * FROM `pagos`";
		List<Pago> list = namedParameterJdbcTemplate.query(sql, new PagoMapper());
		return list;
	}

	@Override
	public void add(Pago pago) {
		String sql = "{call sp_Pagos_Insert (:documentoVentaId, :fecha, :monto, :tipoPago )}";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(pago));

	}

	

	@Override
	public void updateCanceled(Pago pago) {
		String sql = "UPDATE pagos SET tipo_pago = 'Anulado' WHERE idDocumentoVenta = :documentoVentaId";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(pago));

	}

	@Override
	public Pago getById(int id) throws Exception {
		String sql = "SELECT * FROM `pagos` WHERE idPago = :id";

		return namedParameterJdbcTemplate.queryForObject(sql, getSqlParameterByModel(new Pago(id)), new PagoMapper());
	}

	@Override
	public int getLastId() throws Exception {

		String sql = "SELECT idPago as id FROM pagos ORDER BY idPago DESC LIMIT 1";
		return namedParameterJdbcTemplate.queryForObject(sql, new MapSqlParameterSource(), Integer.class);
	}
}
