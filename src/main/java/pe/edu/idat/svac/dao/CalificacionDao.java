package pe.edu.idat.svac.dao;

import pe.edu.idat.svac.model.Calificacion;

public interface CalificacionDao {

	public void add(Calificacion calificacion);

	public void update(Calificacion calificacion);

	public Calificacion getById(int id) throws Exception;

	public Calificacion getByVentaId(int id) throws Exception;

	public void calcularPromedio(Calificacion calificacion) throws Exception;

}
