package pe.edu.idat.svac.dao;

import java.util.List;
import pe.edu.idat.svac.model.Inventario;

public interface InventarioDao {

	public List<Inventario> listAll();

	public void add(Inventario inventario);

	public void crearIngreso(Inventario inventario);

	public void crearSalida(Inventario inventario);

	public Inventario getById(int id) throws Exception;

	public List<Inventario> getInvetarioByArticulo(int i) throws Exception;

	public List<Inventario> getItemsByIngreso() throws Exception;

	public List<Inventario> getItemsBySalida() throws Exception;

}
