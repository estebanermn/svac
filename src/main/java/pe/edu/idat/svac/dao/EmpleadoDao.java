package pe.edu.idat.svac.dao;

import java.util.List;

import pe.edu.idat.svac.model.EmpleadoCumple;
import pe.edu.idat.svac.model.Empleado;

public interface EmpleadoDao {

	public List<Empleado> listAll();

	public void add(Empleado empleado);

	public void update(Empleado empleado);

	public void updateEstado(Empleado empleado);

	public Empleado getById(int id) throws Exception;

	public List<EmpleadoCumple> listAllCumple();

	public List<Empleado> listAllVenta();

}
