package pe.edu.idat.svac.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import pe.edu.idat.svac.model.EmpleadoCumple;
import pe.edu.idat.svac.dao.EmpleadoDao;
import pe.edu.idat.svac.model.Empleado;

@Component
public class EmpleadoDaoImpl implements EmpleadoDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	private SqlParameterSource getSqlParameterByModel(Empleado empleado) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		if (empleado != null) {

			paramSource.addValue("idEmpleado", empleado.getIdEmpleado());
			paramSource.addValue("codigo", empleado.getCodigo());
			paramSource.addValue("apellidos", empleado.getApellidos());
			paramSource.addValue("nombre", empleado.getNombre());
			paramSource.addValue("dni", empleado.getDni());
			paramSource.addValue("direccion", empleado.getDireccion());
			paramSource.addValue("telefono", empleado.getTelefono());
			paramSource.addValue("email", empleado.getEmail());
			paramSource.addValue("fecha_nacimiento", empleado.getFecha_nacimiento());
			paramSource.addValue("estado", empleado.getEstado());
			paramSource.addValue("calificacioon", empleado.getCalificacion());
		}
		return paramSource;
	}

	private static final class EmpleadoCumpleMapper implements RowMapper<EmpleadoCumple> {
		@Override
		public EmpleadoCumple mapRow(ResultSet rs, int rowNum) throws SQLException {
			EmpleadoCumple empl = new EmpleadoCumple();

			empl.setIdEmpleado(rs.getInt(1));
			empl.setCodigo(rs.getString(2));
			empl.setApellidos(rs.getString(3));
			empl.setNombre(rs.getString(4));
			empl.setDni(rs.getInt(5));
			empl.setDireccion(rs.getString(6));
			empl.setTelefono(rs.getInt(7));
			empl.setEmail(rs.getString(8));
			empl.setFecha_nacimiento(rs.getDate(9));
			empl.setEstado(rs.getInt(10));
			empl.setDia(Integer.parseInt(rs.getString(11)));
			empl.setMes(Integer.parseInt(rs.getString(12)));
			empl.setYear(Integer.parseInt(rs.getString(13)));
			return empl;

		}
	}

	private static final class EmpleadoMapper implements RowMapper<Empleado> {
		@Override
		public Empleado mapRow(ResultSet rs, int rowNum) throws SQLException {
			Empleado empl = new Empleado();

			empl.setIdEmpleado(rs.getInt(1));
			empl.setCodigo(rs.getString(2));
			empl.setApellidos(rs.getString(3));
			empl.setNombre(rs.getString(4));
			empl.setDni(rs.getInt(5));
			empl.setDireccion(rs.getString(6));
			empl.setTelefono(rs.getInt(7));
			empl.setEmail(rs.getString(8));
			empl.setFecha_nacimiento(rs.getDate(9));
			empl.setEstado(rs.getInt(10));
			empl.setCalificacion(rs.getDouble(11));
			return empl;

		}
	}

	@Override
	public List<Empleado> listAll() {
		String sql = "select * from empleado";
		List<Empleado> list = namedParameterJdbcTemplate.query(sql, new EmpleadoMapper());
		return list;
	}

	@Override
	public void add(Empleado empleado) {
		String sql = "{call prcEmpleadoInsert(:apellidos, :nombre, :dni, :direccion, :telefono, :email, :fecha_nacimiento)}";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(empleado));
	}

	@Override
	public void update(Empleado empleado) {

		String sql = "{call prcEmpleadoUpdate(:idEmpleado, :apellidos, :nombre, :dni, :direccion, :telefono, :email, :fecha_nacimiento, :estado)}";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(empleado));
	}

	@Override
	public void updateEstado(Empleado empleado) {

		String sql = "UPDATE empleado SET estado = :estado WHERE idEmpleado= :idEmpleado";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(empleado));
	}

	@Override
	public Empleado getById(int id) throws Exception {
		String sql = "SELECT * FROM `empleado` WHERE idEmpleado= :idEmpleado";

		return namedParameterJdbcTemplate.queryForObject(sql, getSqlParameterByModel(new Empleado(id)),
				new EmpleadoMapper());
	}

	@Override
	public List<EmpleadoCumple> listAllCumple() {

		String sql = "{call SP_ObtenerFechasCumpleEmple()}";
		List<EmpleadoCumple> list = namedParameterJdbcTemplate.query(sql, new EmpleadoCumpleMapper());
		return list;
	}

	@Override
	public List<Empleado> listAllVenta() {
		String sql = "{call `sp_Empleado_Ventas`()}";
		List<Empleado> list = namedParameterJdbcTemplate.query(sql, new EmpleadoMapper());
		return list;
	}

}
