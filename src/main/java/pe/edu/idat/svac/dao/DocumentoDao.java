package pe.edu.idat.svac.dao;

import java.util.List;

import pe.edu.idat.svac.model.Documento;

public interface DocumentoDao {

	public List<Documento> listAll();

	public void add(Documento documento);

	public void update(Documento documento);

	public Documento getById(int id) throws Exception;

}
