package pe.edu.idat.svac.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import pe.edu.idat.svac.dao.CompraDetalleDao;
import pe.edu.idat.svac.model.CompraDetalle;

@Component
public class CompraDetalleDaoImpl implements CompraDetalleDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	private SqlParameterSource getSqlParameterByModel(CompraDetalle compra) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		if (compra != null) {

			paramSource.addValue("id", compra.getId());
			paramSource.addValue("compraId", compra.getCompraId());
			paramSource.addValue("articuloId", compra.getArticuloId());
			paramSource.addValue("cantidad", compra.getCantidad());
			paramSource.addValue("precio", compra.getPrecio());

		}
		return paramSource;
	}

	private static final class CompraDetalleMapper implements RowMapper<CompraDetalle> {

		@Override
		public CompraDetalle mapRow(ResultSet rs, int rowNum) throws SQLException {
			CompraDetalle entidad = new CompraDetalle();

			// @TODO Por completar cuando se le necesite.
			entidad.setId(rs.getInt(1));
			entidad.setCompraId(rs.getInt(2));
			entidad.setArticuloId(rs.getInt(3));
			entidad.setCantidad(rs.getInt(4));
			entidad.setPrecio(rs.getDouble(5));

			return entidad;
		}
	}

	@Override
	public void add(CompraDetalle compraDetalle) {

		String sql = "{call sp_OrdenCompraDetalle_Insert(:compraId, :articuloId, :cantidad ,:precio)}";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(compraDetalle));

	}

	@Override
	public List<CompraDetalle> getItemsById(int id) throws Exception {

		CompraDetalle compraDetalle = new CompraDetalle(id);
		compraDetalle.setCompraId(id);

		String sql = "{call sp_OrdenCompraDetalle_Select (:compraId)}";

		List<CompraDetalle> list;

		list = namedParameterJdbcTemplate.query(sql, getSqlParameterByModel(compraDetalle), new CompraDetalleMapper());
		return list;
	}

}
