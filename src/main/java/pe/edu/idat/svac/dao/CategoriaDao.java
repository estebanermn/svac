package pe.edu.idat.svac.dao;

import java.util.List;
import pe.edu.idat.svac.model.Categoria;

public interface CategoriaDao {

    public List<Categoria> listAll();

    public void add(Categoria categoria);

    public void update(Categoria categoria);
    
    public void updateEstado(Categoria categoria);
    
    public Categoria getById(int id) throws Exception;

}
