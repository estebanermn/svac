package pe.edu.idat.svac.dao;

import java.util.List;

import pe.edu.idat.svac.model.Articulo;

public interface ArticuloDao {

	public List<Articulo> listAll();

	public void add(Articulo articulo);

	public void update(Articulo articulo);
	
	public void updateEstado(Articulo articulo);

	public Articulo getById(int id) throws Exception;

	public void aumentarStock(int articuloId, int cantidad) throws Exception;

	public void descontarStock(int articuloId, int cantidad) throws Exception;

}
