package pe.edu.idat.svac.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import pe.edu.idat.svac.dao.CompraDao;
import pe.edu.idat.svac.model.Compra;

@Component
public class CompraDaoImpl implements CompraDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	private SqlParameterSource getSqlParameterByModel(Compra compra) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		if (compra != null) {

			paramSource.addValue("id", compra.getId());
			paramSource.addValue("ordenCompraNumero", compra.getOrdenCompraNumero());
			paramSource.addValue("proveedorId", compra.getProveedorId());
			paramSource.addValue("fecha", compra.getFecha());
			paramSource.addValue("estado", compra.getEstado());
			paramSource.addValue("empleadoId", compra.getEmpleadoId());
			paramSource.addValue("proveedor", compra.getProveedor());

		}
		return paramSource;
	}

	private static final class CompraMapper implements RowMapper<Compra> {

		@Override
		public Compra mapRow(ResultSet rs, int rowNum) throws SQLException {
			Compra compra = new Compra();

			compra.setId(rs.getInt(1));
			compra.setOrdenCompraNumero(rs.getString(2));
			compra.setProveedorId(rs.getInt(3));
			compra.setFecha(rs.getDate(4));
			compra.setEstado(rs.getString(5));
			compra.setEmpleadoId(rs.getInt(6));
			compra.setProveedor(rs.getString(7));

			return compra;
		}
	}

	@Override
	public List<Compra> listAll() {
		String sql = "{call sp_OrdenCompra_Select()}";
		List<Compra> list = namedParameterJdbcTemplate.query(sql, new CompraMapper());
		return list;
	}

	@Override
	public void add(Compra compra) {

		String sql = "{call sp_OrdenCompra_Insert (:proveedorId,  :fecha,  :empleadoId)}";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(compra));
	}

	@Override
	public void updateApproved(Compra compra) {

		String sql = "UPDATE orden_compra SET estado = 'Aprobado' WHERE idOrdenCompra = :id";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(compra));
	}

	@Override
	public void updatePaid(Compra compra) {
		String sql = "UPDATE orden_compra SET estado = 'Pagado' WHERE idOrdenCompra = :id";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(compra));

	}

	@Override
	public void updateReceived(Compra compra) {
		String sql = "UPDATE orden_compra SET estado = 'Recibido' WHERE idOrdenCompra = :id";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(compra));

	}

	@Override
	public void updateCanceled(Compra compra) {
		String sql = "UPDATE orden_compra SET estado = 'Anulado' WHERE idOrdenCompra = :id";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(compra));

	}

	@Override
	public Compra getById(int id) throws Exception {

		String sql = "SELECT t.*, p.nombre proveedor FROM `orden_compra` t INNER JOIN proveedor p ON t.idProveedor = p.idProveedor WHERE idOrdenCompra = :id";

		return namedParameterJdbcTemplate.queryForObject(sql, getSqlParameterByModel(new Compra(id)),
				new CompraMapper());
	}

	@Override
	public int getLastId() throws Exception {

		String sql = "SELECT idOrdenCompra as id FROM orden_compra ORDER BY idOrdenCompra DESC LIMIT 1";
		return namedParameterJdbcTemplate.queryForObject(sql, new MapSqlParameterSource(), Integer.class);
	}

	@Override
	public List<Compra> getVentasByYear() throws Exception {
		String sql = "{call sp_OrdenCompra_By_Historial_Anual()}";
		List<Compra> lista = namedParameterJdbcTemplate.query(sql, new CompraMapper());
		return lista;
	}

	@Override
	public List<Compra> getVentasByMes() throws Exception {
		String sql = "{call sp_OrdenCompra_By_Historial_Mensual()}";
		List<Compra> lista = namedParameterJdbcTemplate.query(sql, new CompraMapper());
		return lista;
	}

	@Override
	public List<Compra> getVentasByQuincena() throws Exception {
		String sql = "{call sp_OrdenCompra_By_Historial_Quincena()}";
		List<Compra> lista = namedParameterJdbcTemplate.query(sql, new CompraMapper());
		return lista;
	}

	@Override
	public List<Compra> getVentasBySemana() throws Exception {
		String sql = "{call sp_OrdenCompra_By_Historial_Semana()}";
		List<Compra> lista = namedParameterJdbcTemplate.query(sql, new CompraMapper());
		return lista;
	}

}
