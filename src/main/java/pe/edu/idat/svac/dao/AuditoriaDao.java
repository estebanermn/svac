package pe.edu.idat.svac.dao;

import java.util.List;
import pe.edu.idat.svac.model.Auditoria;

public interface AuditoriaDao {

	public List<Auditoria> listAll();

	public void add(Auditoria entidad);

	public void update(Auditoria entidad);

	public void delete(Auditoria entidad);

	public Auditoria getById(int id) throws Exception;

}
