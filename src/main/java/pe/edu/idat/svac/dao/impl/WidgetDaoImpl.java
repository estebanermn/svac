package pe.edu.idat.svac.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import pe.edu.idat.svac.dao.WidgetDao;
import pe.edu.idat.svac.model.Widget;

@Component
public class WidgetDaoImpl implements WidgetDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	private static final class WidgetMapper implements RowMapper<Widget> {

		@Override
		public Widget mapRow(ResultSet rs, int rowNum) throws SQLException {
			Widget data = new Widget();
			data.setKey(rs.getString("nombre"));
			data.setValue(rs.getInt("cantidad"));
			return data;
		}
	}

	@Override
	public int getWidget1() {

		String sql = "{call sp_widget_1_total_clientes()}";
		return namedParameterJdbcTemplate.queryForObject(sql, new MapSqlParameterSource(), Integer.class);
	}

	@Override
	public int getWidget2() {

		String sql = "{call sp_widget_2_productos_vendidos()}";
		return namedParameterJdbcTemplate.queryForObject(sql, new MapSqlParameterSource(), Integer.class);
	}

	@Override
	public int getWidget3() {

		String sql = "{call sp_widget_3_total_ventas_este_mes()}";
		return namedParameterJdbcTemplate.queryForObject(sql, new MapSqlParameterSource(), Integer.class);
	}

	@Override
	public int getWidget4() {

		String sql = "{call sp_widget_4_total_vendido_mes()}";
		return namedParameterJdbcTemplate.queryForObject(sql, new MapSqlParameterSource(), Integer.class);
	}

	@Override
	public List<Widget> getWidgetProductosMasVendido() {
		String sql = "{call sp_widget_chart_productos_mas_vendidos()}";
		List<Widget> list = namedParameterJdbcTemplate.query(sql, new WidgetMapper());
		return list;
	}

	@Override
	public List<Widget> getWidgetProductosMenosVendido() {
		String sql = "{call sp_widget_chart_productos_menos_vendidos()}";
		List<Widget> list = namedParameterJdbcTemplate.query(sql, new WidgetMapper());
		return list;
	}
}
