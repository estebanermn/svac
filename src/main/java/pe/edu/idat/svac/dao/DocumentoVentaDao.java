package pe.edu.idat.svac.dao;

import java.util.List;

import pe.edu.idat.svac.model.DocumentoVenta;

public interface DocumentoVentaDao {

	public List<DocumentoVenta> listAll();

	public void add(DocumentoVenta venta);

	public void updateCanceled(DocumentoVenta venta);

	public void updatePagado(DocumentoVenta venta);

	public void updateFechaPagado(DocumentoVenta venta);

	public void updateCalificacion(DocumentoVenta venta);

	public DocumentoVenta getById(int id) throws Exception;

	public int getLastId() throws Exception;

	public List<DocumentoVenta> getVentasByEmpleadoId(int id) throws Exception;

	public List<DocumentoVenta> getVentasByYear() throws Exception;

	public List<DocumentoVenta> getVentasByMes() throws Exception;

	public List<DocumentoVenta> getVentasByQuincena() throws Exception;

	public List<DocumentoVenta> getVentasBySemana() throws Exception;

}
