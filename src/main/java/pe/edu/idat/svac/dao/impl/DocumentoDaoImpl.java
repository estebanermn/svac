package pe.edu.idat.svac.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import pe.edu.idat.svac.dao.DocumentoDao;
import pe.edu.idat.svac.model.Documento;

@Component
public class DocumentoDaoImpl implements DocumentoDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	private SqlParameterSource getSqlParameterByModel(Documento documento) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		if (documento != null) {

			paramSource.addValue("id", documento.getId());
			paramSource.addValue("nombre", documento.getNombre());
			paramSource.addValue("operacion", documento.getOperacion());
		}
		return paramSource;
	}

	private static final class TipoDocumentoMapper implements RowMapper<Documento> {

		@Override
		public Documento mapRow(ResultSet rs, int rowNum) throws SQLException {
			Documento documento = new Documento();

			documento.setId(rs.getInt(1));
			documento.setNombre(rs.getString(2));
			documento.setOperacion(rs.getString(3));
			return documento;
		}
	}
	@Override
	public List<Documento> listAll() {
		String sql = "SELECT * FROM `tipo_documento`";
		List<Documento> list = namedParameterJdbcTemplate.query(sql, new TipoDocumentoMapper());
		return list;
	}

	@Override
	public void add(Documento documento) {
		String sql = "INSERT INTO `tipo_documento`(`idTipo_documento`, `nombre`, `operacion`) VALUES (id:,:nombre,:operacion)";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(documento));

	}

	@Override
	public void update(Documento documento) {
		String sql = "UPDATE `tipo_documento` SET `nombre` = :nombre, `operacion` = :operacion WHERE `documento_documento`.`idTipo_documento` = :id";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(documento));
	}

	@Override
	public Documento getById(int id) throws Exception {
		String sql = "SELECT * FROM `tipo_documento` WHERE idTipo_documento = :id";

		return namedParameterJdbcTemplate.queryForObject(sql, getSqlParameterByModel(new Documento(id)),
				new TipoDocumentoMapper());
	}
}
