package pe.edu.idat.svac.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import pe.edu.idat.svac.dao.AuditoriaDao;
import pe.edu.idat.svac.model.Auditoria;

@Component
public class AuditoriaDaoImpl implements AuditoriaDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	private SqlParameterSource getSqlParameterByModel(Auditoria auditoria) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		if (auditoria != null) {

			paramSource.addValue("id", auditoria.getId());
			paramSource.addValue("ordenCompraId", auditoria.getOrdenCompraId());
			paramSource.addValue("documentoVentaId", auditoria.getDocumentoVentaId());
			paramSource.addValue("fecha", auditoria.getFecha());
			paramSource.addValue("empleadoId", auditoria.getEmpleadoId());
			paramSource.addValue("estado", auditoria.getEstado());
			paramSource.addValue("descripcion", auditoria.getDescripcion());
		}
		return paramSource;
	}

	private static final class AuditoriaMapper implements RowMapper<Auditoria> {

		@Override
		public Auditoria mapRow(ResultSet rs, int rowNum) throws SQLException {
			Auditoria auditoria = new Auditoria();

			auditoria.setId(rs.getInt(1));
			auditoria.setOrdenCompraId(rs.getInt(2));
			auditoria.setDocumentoVentaId(rs.getInt(3));
			auditoria.setFecha(rs.getDate(4));
			auditoria.setEmpleadoId(rs.getInt(5));
			auditoria.setEstado(rs.getString(6));
			auditoria.setDescripcion(rs.getString(7));
			return auditoria;
		}
	}

	@Override
	public List<Auditoria> listAll() {
		String sql = "SELECT * FROM `auditoria`";
		List<Auditoria> list = namedParameterJdbcTemplate.query(sql, new AuditoriaMapper());
		return list;
	}

	@Override
	public void add(Auditoria auditoria) {
		String sql = "{ call sp_Auditoria_Insert(:ordenCompraId, :documentoVentaId, :fecha, :empleadoId, :estado, :descripcion) }";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(auditoria));
	}

	@Override
	public void update(Auditoria auditoria) {
		// @TODO SP por hacer.
		String sql = "{ call sp_Auditoria_Updated(:id, :ordenCompraId, :documentoVentaId, :fecha, :empleadoId, :estado, :descripcion) }";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(auditoria));
	}

	@Override
	public void delete(Auditoria entidad) {
		// TODO Auto-generated method stub
	}

	@Override
	public Auditoria getById(int id) throws Exception {
		String sql = "SELECT * FROM `auditoria` WHERE idItemPrimaria = :id";

		return namedParameterJdbcTemplate.queryForObject(sql, getSqlParameterByModel(new Auditoria(id)),
				new AuditoriaMapper());
	}

}
