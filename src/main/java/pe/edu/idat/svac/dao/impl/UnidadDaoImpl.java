package pe.edu.idat.svac.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import pe.edu.idat.svac.dao.UnidadDao;
import pe.edu.idat.svac.model.Unidad;

@Component
public class UnidadDaoImpl implements UnidadDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	private SqlParameterSource getSqlParameterByModel(Unidad unidad) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		if (unidad != null) {

			paramSource.addValue("id", unidad.getId());
			paramSource.addValue("categoriaId", unidad.getCategoriaId());
			paramSource.addValue("codigo", unidad.getCodigo());
			paramSource.addValue("nombre", unidad.getNombre());
			paramSource.addValue("prefijo", unidad.getPrefijo());
			paramSource.addValue("estado", unidad.getEstado());
			paramSource.addValue("categoria", unidad.getCategoria());

		}
		return paramSource;
	}

	private static final class UnidadMapper implements RowMapper<Unidad> {

		@Override
		public Unidad mapRow(ResultSet rs, int rowNum) throws SQLException {
			Unidad unidad = new Unidad();

			unidad.setId(rs.getInt(1));
			unidad.setCategoriaId(rs.getInt(2));
			unidad.setCodigo(rs.getString(3));
			unidad.setNombre(rs.getString(4));
			unidad.setPrefijo(rs.getString(5));
			unidad.setEstado(rs.getInt(6));
			unidad.setCategoria(rs.getString(7));
			return unidad;
		}
	}

	@Override
	public List<Unidad> listAll() {
		String sql = "{call prcUnidadSelect()}";
		List<Unidad> list = namedParameterJdbcTemplate.query(sql, new UnidadMapper());
		return list;
	}

	@Override
	public void add(Unidad unidad) {
		String sql = "{call prcUnidadInsert(:nombre , :prefijo , :categoriaId 	)}";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(unidad));

	}

	@Override
	public void update(Unidad unidad) {
		String sql = "UPDATE `unidad_medida` SET idCategoria = :categoriaId ,`nombre` = :nombre, `prefijo` = :prefijo, `estado` = :estado WHERE `unidad_medida`.`idUnidad` = :id";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(unidad));
	}

	@Override
	public void updateEstado(Unidad unidad) {
		String sql = "UPDATE `unidad_medida` SET `estado` = :estado WHERE `idUnidad` = :id";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(unidad));
	}

	@Override
	public Unidad getById(int id) throws Exception {
		String sql = "{call sp_Unidad_Id(:id)}";

		return namedParameterJdbcTemplate.queryForObject(sql, getSqlParameterByModel(new Unidad(id)),
				new UnidadMapper());
	}
}
