package pe.edu.idat.svac.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import pe.edu.idat.svac.dao.ClienteDao;
import pe.edu.idat.svac.model.Cliente;

@Component
public class ClienteDaoImpl implements ClienteDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	private SqlParameterSource getSqlParameterByModel(Cliente cliente) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		if (cliente != null) {

			paramSource.addValue("id", cliente.getId());
			paramSource.addValue("codigo", cliente.getCodigo());
			paramSource.addValue("tipo", cliente.getTipo());
			paramSource.addValue("nombre", cliente.getNombre());
			paramSource.addValue("documento", cliente.getDocumento());
			paramSource.addValue("numeroDocumento", cliente.getNumeroDocumento());
			paramSource.addValue("departamento", cliente.getDepartamento());
			paramSource.addValue("provincia", cliente.getProvincia());
			paramSource.addValue("distrito", cliente.getDistrito());
			paramSource.addValue("direccion", cliente.getDireccion());
			paramSource.addValue("telefono", cliente.getTelefono());
			paramSource.addValue("email", cliente.getEmail());
			paramSource.addValue("numeroCuenta", cliente.getNumeroCuenta());
			paramSource.addValue("estado", cliente.getEstado());
		}
		return paramSource;
	}

	private static final class ClienteMapper implements RowMapper<Cliente> {

		@Override
		public Cliente mapRow(ResultSet rs, int rowNum) throws SQLException {
			Cliente cliente = new Cliente();

			cliente.setId(rs.getInt(1));
			cliente.setCodigo(rs.getString(2));
			cliente.setTipo(rs.getString(3));
			cliente.setNombre(rs.getString(4));
			cliente.setDocumento(rs.getString(5));
			cliente.setNumeroDocumento(rs.getString(6));
			cliente.setDepartamento(rs.getString(7));
			cliente.setProvincia(rs.getString(8));
			cliente.setDistrito(rs.getString(9));
			cliente.setDireccion(rs.getString(10));
			cliente.setTelefono(rs.getString(11));
			cliente.setEmail(rs.getString(12));
			cliente.setNumeroCuenta(rs.getString(13));
			cliente.setEstado(rs.getInt(14));
			return cliente;
		}
	}

	@Override
	public List<Cliente> listAll() {
		String sql = "SELECT * FROM `cliente`";
		List<Cliente> list = namedParameterJdbcTemplate.query(sql, new ClienteMapper());
		return list;
	}

	@Override
	public void add(Cliente cliente) {
		String sql = "{call prcClienteInsert(:nombre,  :documento, :numeroDocumento, :departamento, :provincia, :distrito,"
				+ ":direccion , :telefono, :email ,:numeroCuenta)}";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(cliente));

	}

	@Override
	public void update(Cliente cliente) {
		String sql = "{call prcClienteUpdate(:id, :nombre, :documento, :numeroDocumento, :departamento, :provincia, :distrito,"
				+ ":direccion , :telefono, :email ,:numeroCuenta,:estado)}";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(cliente));
	}
	
	@Override
	public void updateEstado(Cliente cliente) {
		String sql = "UPDATE cliente SET estado=:estado WHERE idCliente = :id";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(cliente));
	}

	@Override
	public Cliente getById(int id) throws Exception {
		String sql = "SELECT * FROM `cliente` WHERE idCliente= :id";

		return namedParameterJdbcTemplate.queryForObject(sql, getSqlParameterByModel(new Cliente(id)),
				new ClienteMapper());
	}
}
