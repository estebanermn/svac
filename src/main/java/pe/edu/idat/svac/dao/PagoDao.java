package pe.edu.idat.svac.dao;

import java.util.List;
import pe.edu.idat.svac.model.Pago;

public interface PagoDao {

	public List<Pago> listAll();

	public void add(Pago pago);

	public void updateCanceled(Pago pago);

	public Pago getById(int id) throws Exception;

	public int getLastId() throws Exception;

}
