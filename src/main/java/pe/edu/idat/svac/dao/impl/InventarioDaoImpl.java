package pe.edu.idat.svac.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import pe.edu.idat.svac.dao.InventarioDao;
import pe.edu.idat.svac.model.Inventario;
import pe.edu.idat.svac.utils.Estado;

@Component
public class InventarioDaoImpl implements InventarioDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	private SqlParameterSource getSqlParameterByModel(Inventario inventario) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		if (inventario != null) {

			paramSource.addValue("id", inventario.getId());
			paramSource.addValue("articuloId", inventario.getArticuloId());
			paramSource.addValue("fecha", inventario.getFecha());
			paramSource.addValue("tipoMovimiento", inventario.getTipoMovimiento());
			paramSource.addValue("stockIn", inventario.getStockIn());
			paramSource.addValue("stockOut", inventario.getStockOut());
			paramSource.addValue("ordenCompraId", inventario.getOrdenCompraId());
			paramSource.addValue("documentoVentaId", inventario.getDocumentoVentaId());
			paramSource.addValue("descripcion", inventario.getDescripcion());
			paramSource.addValue("precioUnitario", inventario.getPrecioUnitario());

		}
		return paramSource;
	}

	private static final class InventarioMapper implements RowMapper<Inventario> {

		@Override
		public Inventario mapRow(ResultSet rs, int rowNum) throws SQLException {
			Inventario inventario = new Inventario();

			inventario.setId(rs.getInt(1));
			inventario.setArticuloId(rs.getInt(2));
			inventario.setFecha(rs.getDate(3));
			inventario.setTipoMovimiento(rs.getString(4));
			inventario.setStockIn(rs.getInt(5));
			inventario.setStockOut(rs.getInt(6));
			inventario.setOrdenCompraId(rs.getInt(7));
			inventario.setDocumentoVentaId(rs.getInt(8));
			inventario.setDescripcion(rs.getString(9));
			inventario.setPrecioUnitario(rs.getDouble(10));

			return inventario;
		}
	}

	private static final class WebInventarioMapper implements RowMapper<Inventario> {

		@Override
		public Inventario mapRow(ResultSet rs, int rowNum) throws SQLException {
			Inventario inventario = new Inventario();

			inventario.setId(rs.getInt(1));
			inventario.setArticuloId(rs.getInt(2));
			inventario.setFecha(rs.getDate(3));
			inventario.setTipoMovimiento(rs.getString(4));
			inventario.setStockIn(rs.getInt(5));
			inventario.setStockOut(rs.getInt(6));
			inventario.setOrdenCompraId(rs.getInt(7));
			inventario.setDocumentoVentaId(rs.getInt(8));
			inventario.setDescripcion(rs.getString(9));
			inventario.setPrecioUnitario(rs.getDouble(10));
			inventario.setArticulo(rs.getString("articulo"));
			inventario.setCodigo(rs.getString("codigo"));		
			inventario.setStock(rs.getInt("stock"));

			return inventario;
		}
	}

	@Override
	public List<Inventario> listAll() {
		 //String sql = "SELECT * FROM `inventario`";
		// String sql = "SELECT i.*, a.nombre AS articulo, a.codigo as codigo,"
		 		//+ " a.stock as stock FROM inventario i JOIN articulo a ON i.idArticulo = a.idArticulo";
		
		String sql = "{ call sp_Inventario_Select() }";
		List<Inventario> list = namedParameterJdbcTemplate.query(sql, new WebInventarioMapper());
		return list;
	}

	@Override
	public void add(Inventario inventario) {
		String sql = "{call  sp_Inventario_Insert (:articuloId, :tipoMovimiento, :stockIn, :stockOut, :ordenCompraId, :documentoVentaId, :descripcion, :precioUnitario)}";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(inventario));
	}

	@Override
	public void crearIngreso(Inventario inventario) {
		inventario.setTipoMovimiento(Estado.INGRESO);
		inventario.setDescripcion(Estado.INGRESO_DESC);
		inventario.setStockOut(0);
		add(inventario);
	}

	@Override
	public void crearSalida(Inventario inventario) {
		inventario.setTipoMovimiento(Estado.SALIDA);
		inventario.setDescripcion(Estado.SALIDA_DESC);
		inventario.setStockIn(0);
		add(inventario);
	}

	@Override
	public Inventario getById(int id) throws Exception {
		String sql = "SELECT * FROM `inventario` WHERE idInventario = :id";

		return namedParameterJdbcTemplate.queryForObject(sql, getSqlParameterByModel(new Inventario(id)),
				new InventarioMapper());
	}

	@Override
	public List<Inventario> getInvetarioByArticulo(int id) throws Exception {
		String sql = "SELECT * FROM inventario WHERE idArticulo = :articuloId ";

		Inventario inventario = new Inventario(id);
		inventario.setArticuloId(id);
		List<Inventario> list;
		list = namedParameterJdbcTemplate.query(sql, getSqlParameterByModel(inventario), new InventarioMapper());
		return list;
	}

	@Override
	public List<Inventario> getItemsByIngreso() throws Exception {

		String sql = "{ call sp_Inventario_Ingreso_Select() }";
		List<Inventario> list = namedParameterJdbcTemplate.query(sql, new WebInventarioMapper());
		return list;
	}

	@Override
	public List<Inventario> getItemsBySalida() throws Exception {

		String sql = "{ call sp_Inventario_Salida_Select() }";
		List<Inventario> list = namedParameterJdbcTemplate.query(sql, new WebInventarioMapper());
		return list;
	}

}
