package pe.edu.idat.svac.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import pe.edu.idat.svac.dao.DocumentoVentaDao;
import pe.edu.idat.svac.model.DocumentoVenta;

@Component
public class DocumentoVentaDaoImpl implements DocumentoVentaDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	private SqlParameterSource getSqlParameterByModel(DocumentoVenta venta) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		if (venta != null) {

			paramSource.addValue("id", venta.getId());
			paramSource.addValue("documentoVentaSerie", venta.getDocumentoVentaSerie());
			paramSource.addValue("documentoVentaNumero", venta.getDocumentoVentaNumero());
			paramSource.addValue("clienteId", venta.getClienteId());
			paramSource.addValue("fecha", venta.getFecha());
			paramSource.addValue("estado", venta.getEstado());
			paramSource.addValue("subtotal", venta.getSubtotal());
			paramSource.addValue("igv", venta.getIgv());
			paramSource.addValue("total", venta.getTotal());
			paramSource.addValue("empleadoId", venta.getEmpleadoId());
			paramSource.addValue("tipoDocumento", venta.getTipoDocumento());
			paramSource.addValue("cliente", venta.getCliente());
			paramSource.addValue("calificacion", venta.getCalificacion());

		}
		return paramSource;
	}

	private static final class DocumentoVentaMapper implements RowMapper<DocumentoVenta> {

		@Override
		public DocumentoVenta mapRow(ResultSet rs, int rowNum) throws SQLException {
			DocumentoVenta venta = new DocumentoVenta();

			venta.setId(rs.getInt(1));
			venta.setDocumentoVentaSerie(rs.getString(2));
			venta.setDocumentoVentaNumero(rs.getString(3));
			venta.setClienteId(rs.getInt(4));
			venta.setFecha(rs.getDate(5));
			venta.setEstado(rs.getString(6));
			venta.setSubtotal(rs.getDouble(7));
			venta.setIgv(rs.getDouble(8));
			venta.setTotal(rs.getDouble(9));
			venta.setEmpleadoId(rs.getInt(10));
			venta.setCalificacion(rs.getInt(11));
			venta.setTipoDocumento(rs.getString(12));
			venta.setCliente(rs.getString(14));

			return venta;
		}
	}

	@Override
	public List<DocumentoVenta> listAll() {

		String sql = "{call sp_Ventas_Select()}";
		List<DocumentoVenta> list = namedParameterJdbcTemplate.query(sql, new DocumentoVentaMapper());
		return list;
	}

	@Override
	public void add(DocumentoVenta venta) {

		String sql = "{call sp_DocumentoVenta_Insert (:clienteId, :fecha, :subtotal, :igv, :total, :empleadoId, :tipoDocumento)}";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(venta));
	}

	@Override
	public void updateCanceled(DocumentoVenta venta) {
		String sql = "UPDATE documento_venta SET estado = 'Anulado' WHERE idDocumentoVenta = :id";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(venta));

	}

	@Override
	public void updatePagado(DocumentoVenta venta) {
		String sql = "UPDATE documento_venta SET estado = 'Pagado' WHERE idDocumentoVenta = :id";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(venta));
	}

	@Override
	public void updateFechaPagado(DocumentoVenta venta) {
		String sql = "UPDATE documento_venta SET fecha_pagado = :fecha WHERE idDocumentoVenta = :id";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(venta));
	}

	@Override
	public void updateCalificacion(DocumentoVenta venta) {
		String sql = "UPDATE `documento_venta` SET `calificacion` = :calificacion WHERE `documento_venta`.`idDocumentoVenta` = :id";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(venta));
	}

	@Override
	public DocumentoVenta getById(int id) throws Exception {

		String sql = "{call sp_DocumentoVenta_Id(:id)}";

		return namedParameterJdbcTemplate.queryForObject(sql, getSqlParameterByModel(new DocumentoVenta(id)),
				new DocumentoVentaMapper());
	}

	@Override
	public int getLastId() throws Exception {

		String sql = "SELECT idDocumentoVenta as id FROM documento_venta ORDER BY idDocumentoVenta DESC LIMIT 1";
		return namedParameterJdbcTemplate.queryForObject(sql, new MapSqlParameterSource(), Integer.class);
	}

	@Override
	public List<DocumentoVenta> getVentasByEmpleadoId(int id) throws Exception {
		String sql = "{call sp_Ventas_all_Empleado(:empleadoId)}";

		DocumentoVenta venta = new DocumentoVenta(id);
		venta.setEmpleadoId(id);
		List<DocumentoVenta> list;
		list = namedParameterJdbcTemplate.query(sql, getSqlParameterByModel(venta), new DocumentoVentaMapper());
		return list;
	}

	@Override
	public List<DocumentoVenta> getVentasByYear() throws Exception {
		String sql = "{call sp_Ventas_By_Historial_Anual()}";
		List<DocumentoVenta> lista = namedParameterJdbcTemplate.query(sql, new DocumentoVentaMapper());
		return lista;
	}

	@Override
	public List<DocumentoVenta> getVentasByMes() throws Exception {
		String sql = "{call sp_Ventas_By_Historial_Mensual()}";
		List<DocumentoVenta> lista = namedParameterJdbcTemplate.query(sql, new DocumentoVentaMapper());
		return lista;
	}

	@Override
	public List<DocumentoVenta> getVentasByQuincena() throws Exception {
		String sql = "{call sp_Ventas_By_Historial_Quincena()}";
		List<DocumentoVenta> lista = namedParameterJdbcTemplate.query(sql, new DocumentoVentaMapper());
		return lista;
	}

	@Override
	public List<DocumentoVenta> getVentasBySemana() throws Exception {
		String sql = "{call sp_Ventas_By_Historial_Semana()}";
		List<DocumentoVenta> lista = namedParameterJdbcTemplate.query(sql, new DocumentoVentaMapper());
		return lista;
	}

}
