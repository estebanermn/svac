package pe.edu.idat.svac.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import pe.edu.idat.svac.dao.CategoriaDao;
import pe.edu.idat.svac.model.Categoria;

@Component
public class CategoriaDaoImpl implements CategoriaDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	private SqlParameterSource getSqlParameterByModel(Categoria categoria) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		if (categoria != null) {

			paramSource.addValue("id", categoria.getId());
			paramSource.addValue("codigo", categoria.getCodigo());
			paramSource.addValue("nombre", categoria.getNombre());
			paramSource.addValue("estado", categoria.getEstado());
		}
		return paramSource;
	}

	private static final class CategoriaMapper implements RowMapper<Categoria> {

		@Override
		public Categoria mapRow(ResultSet rs, int rowNum) throws SQLException {
			Categoria categoria = new Categoria();

			categoria.setId(rs.getInt(1));
			categoria.setCodigo(rs.getString(2));
			categoria.setNombre(rs.getString(3));
			categoria.setEstado(rs.getInt(4));
			return categoria;
		}
	}

	@Override
	public List<Categoria> listAll() {
		String sql = "SELECT * FROM categoria";
		List<Categoria> list = namedParameterJdbcTemplate.query(sql, new CategoriaMapper());
		return list;
	}

	@Override
	public void add(Categoria categoria) {
		String sql = "{call prcCategoriaInsert(:nombre)}";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(categoria));
	}

	@Override
	public void update(Categoria categoria) {
		String sql = "UPDATE `categoria` SET `nombre` = :nombre, `estado` = :estado WHERE `categoria`.`idCategoria` = :id";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(categoria));
	}

	@Override
	public void updateEstado(Categoria categoria) {
		String sql = "UPDATE `categoria` SET `estado` = :estado WHERE `idCategoria` = :id";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(categoria));
	}

	@Override
	public Categoria getById(int id) throws Exception {

		String sql = "SELECT * FROM categoria WHERE idCategoria = :id";
		return namedParameterJdbcTemplate.queryForObject(sql, getSqlParameterByModel(new Categoria(id)),
				new CategoriaMapper());

	}
}
