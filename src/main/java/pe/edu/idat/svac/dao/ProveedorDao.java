package pe.edu.idat.svac.dao;

import java.util.List;
import pe.edu.idat.svac.model.Proveedor;

public interface ProveedorDao {

	public List<Proveedor> listAll();

	public void add(Proveedor proveedor);

	public void update(Proveedor proveedor);

	public void updateEstado(Proveedor proveedor);

	public Proveedor getById(int id) throws Exception;

}
