package pe.edu.idat.svac.dao;

import java.util.List;

import pe.edu.idat.svac.model.DocumentoVentaDetalle;

public interface DocumentoVentaDetalleDao {

	public void add(DocumentoVentaDetalle venta);

	public List<DocumentoVentaDetalle> getItemsById(int id) throws Exception;

}
