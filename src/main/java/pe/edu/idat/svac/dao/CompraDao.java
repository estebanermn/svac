package pe.edu.idat.svac.dao;

import java.util.List;

import pe.edu.idat.svac.model.Compra;

public interface CompraDao {

	public List<Compra> listAll();

	public void add(Compra compra);

	public void updateApproved(Compra compra);

	public void updatePaid(Compra compra);

	public void updateReceived(Compra compra);

	public void updateCanceled(Compra compra);

	public Compra getById(int id) throws Exception;

	public int getLastId() throws Exception;

	public List<Compra> getVentasByYear() throws Exception;

	public List<Compra> getVentasByMes() throws Exception;

	public List<Compra> getVentasByQuincena() throws Exception;

	public List<Compra> getVentasBySemana() throws Exception;

}
