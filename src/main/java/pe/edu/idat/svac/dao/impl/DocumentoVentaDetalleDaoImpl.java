package pe.edu.idat.svac.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import pe.edu.idat.svac.dao.DocumentoVentaDetalleDao;
import pe.edu.idat.svac.model.DocumentoVentaDetalle;

@Component
public class DocumentoVentaDetalleDaoImpl implements DocumentoVentaDetalleDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	private SqlParameterSource getSqlParameterByModel(DocumentoVentaDetalle ventaDetalle) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		if (ventaDetalle != null) {

			paramSource.addValue("id", ventaDetalle.getId());
			paramSource.addValue("documentoVentaId", ventaDetalle.getDocumentoVentaId());
			paramSource.addValue("articuloId", ventaDetalle.getArticuloId());
			paramSource.addValue("cantidad", ventaDetalle.getCantidad());
			paramSource.addValue("precio", ventaDetalle.getPrecio());
			paramSource.addValue("descuento", ventaDetalle.getDescuento());
			paramSource.addValue("total", ventaDetalle.getTotal());

		}
		return paramSource;
	}

	private static final class DocumentoVentaDetalleMapper implements RowMapper<DocumentoVentaDetalle> {

		@Override
		public DocumentoVentaDetalle mapRow(ResultSet rs, int rowNum) throws SQLException {
			DocumentoVentaDetalle ventaDetalle = new DocumentoVentaDetalle();

			ventaDetalle.setId(rs.getInt(1));
			ventaDetalle.setDocumentoVentaId(rs.getInt(2));
			ventaDetalle.setArticuloId(rs.getInt(3));
			ventaDetalle.setCantidad(rs.getInt(4));
			ventaDetalle.setPrecio(rs.getDouble(5));
			ventaDetalle.setDescuento(rs.getDouble(6));
			ventaDetalle.setTotal(rs.getDouble(7));

			return ventaDetalle;
		}
	}

	@Override
	public void add(DocumentoVentaDetalle ventaDetalle) {

		String sql = "{call sp_DocumentoVentaDetalle_Insert (:documentoVentaId,  :articuloId, :cantidad, :precio, :descuento, :total)}";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(ventaDetalle));
	}

	@Override
	public List<DocumentoVentaDetalle> getItemsById(int id) throws Exception {

		String sql = "{call sp_DocumentoVentaDetalle_Select(:documentoVentaId) }";

		DocumentoVentaDetalle ventaDetalle = new DocumentoVentaDetalle(id);
		List<DocumentoVentaDetalle> list;
		list = namedParameterJdbcTemplate.query(sql, getSqlParameterByModel(ventaDetalle),
				new DocumentoVentaDetalleMapper());
		return list;
	}

}
