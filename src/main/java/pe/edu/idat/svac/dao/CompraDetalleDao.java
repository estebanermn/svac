package pe.edu.idat.svac.dao;

import java.util.List;

import pe.edu.idat.svac.model.CompraDetalle;

public interface CompraDetalleDao {

	public void add(CompraDetalle compraDetalle) throws Exception;

	public List<CompraDetalle> getItemsById(int id) throws Exception;

}
