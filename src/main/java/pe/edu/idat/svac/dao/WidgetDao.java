package pe.edu.idat.svac.dao;

import java.util.List;

import pe.edu.idat.svac.model.Widget;

public interface WidgetDao {

	public int getWidget1();

	public int getWidget2();

	public int getWidget3();

	public int getWidget4();

	public List<Widget> getWidgetProductosMasVendido();

	public List<Widget> getWidgetProductosMenosVendido();

}
