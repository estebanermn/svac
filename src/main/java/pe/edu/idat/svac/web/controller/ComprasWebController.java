package pe.edu.idat.svac.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.edu.idat.svac.model.Articulo;
import pe.edu.idat.svac.model.Compra;
import pe.edu.idat.svac.model.CompraDetalle;
import pe.edu.idat.svac.model.Proveedor;
import pe.edu.idat.svac.service.ArticuloService;
import pe.edu.idat.svac.service.CompraDetalleService;
import pe.edu.idat.svac.service.CompraService;
import pe.edu.idat.svac.service.ProveedorService;
import pe.edu.idat.svac.utils.Estado;
import pe.edu.idat.svac.utils.Util;

@Controller
@RequestMapping("/web/compras")
class ComprasWebController extends BaseWebController {

	@Autowired
	ArticuloService articuloService;

	@Autowired
	ProveedorService proveedorService;

	@Autowired
	CompraService compraService;

	@Autowired
	CompraDetalleService compraDetalleService;

	@RequestMapping(value = "/ingresos", method = RequestMethod.GET)
	public String ingresos(ModelMap model) {
		return "compras/ingresos/index";
	}

	@RequestMapping(value = "/ingresos/add", method = RequestMethod.GET)
	public ModelAndView ingresosAdd() {
		ModelAndView model = new ModelAndView("compras/ingresos/add");

		List<Articulo> articulos = articuloService.listAll();
		List<Proveedor> proveedores = proveedorService.listAll();

		Compra compra = new Compra();
		compra.setEstado(Estado.NUEVO);
		compra.setFecha(Util.getTimeNow());

		int impuesto = getImpuestoIGV();

		ObjectMapper mapper = new ObjectMapper();

		try {
			model.addObject("articuloData", mapper.writeValueAsString(articulos));
			model.addObject("proveedorData", mapper.writeValueAsString(proveedores));

			model.addObject("compraData", compra);

			model.addObject("empleadoId", getEmpleadoId());
			model.addObject("impuesto", impuesto);

		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return model;
	}

	@RequestMapping(value = "/ingresos/{id}", method = RequestMethod.GET)
	public ModelAndView ingresosShow(@PathVariable int id) throws Exception {

		ModelAndView model = new ModelAndView("compras/ingresos/show");
		ObjectMapper mapper = new ObjectMapper();

		try {

			List<Articulo> articulos = articuloService.listAll();
			List<CompraDetalle> items = compraDetalleService.getItemsById(id);

			Compra compra = compraService.getById(id);
			compra.setItems(items);

			model.addObject("empleadoId", getEmpleadoId());
			model.addObject("compraData", compra);
			model.addObject("compraDetalleData", mapper.writeValueAsString(compra.getItems()));
			model.addObject("articuloData", mapper.writeValueAsString(articulos));

		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return model;
	}

	@RequestMapping(value = "/proveedores", method = RequestMethod.GET)
	public String proveedores(ModelMap model) {
		return "proveedores/index";
	}

	@RequestMapping(value = "/historial", method = RequestMethod.GET)
	public String historialIndex() {
		return "compras/historial/index";
	}

	@RequestMapping(value = "/historial/{value}", method = RequestMethod.GET)
	public ModelAndView historial(@PathVariable String value) {

		ModelAndView model = new ModelAndView("compras/historial/historial");
		model.addObject("tipoHistorial", value);
		return model;
	}

}
