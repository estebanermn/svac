package pe.edu.idat.svac.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import pe.edu.idat.svac.model.Widget;
import pe.edu.idat.svac.service.WidgetService;

@Controller
class HomeWebController {

	@Autowired
	WidgetService widgetService;

	@RequestMapping("/")
	public ModelAndView landingPage() {
		return new ModelAndView("redirect:/web/");
	}

	@RequestMapping("/web")
	public ModelAndView dashboard() {

		ModelAndView model = new ModelAndView("dashboard/index");

		ObjectMapper mapper = new ObjectMapper();

		try {
			int widget1 = widgetService.getWidget(1);
			int widget2 = widgetService.getWidget(2);
			int widget3 = widgetService.getWidget(3);
			int widget4 = widgetService.getWidget(4);

			List<Widget> chart1 = widgetService.getProductosMasVendido();
			List<Widget> chart2 = widgetService.getProductosMenosVendido();

			model.addObject("widget1", mapper.writeValueAsString(widget1));
			model.addObject("widget2", mapper.writeValueAsString(widget2));
			model.addObject("widget3", mapper.writeValueAsString(widget3));
			model.addObject("widget4", mapper.writeValueAsString(widget4));

			model.addObject("chart1", mapper.writeValueAsString(chart1));
			model.addObject("chart2", mapper.writeValueAsString(chart2));

		} catch (Exception e) {
			e.printStackTrace();
		}

		return model;
	}
}
