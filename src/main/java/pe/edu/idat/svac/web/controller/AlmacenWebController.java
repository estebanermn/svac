package pe.edu.idat.svac.web.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.edu.idat.svac.model.Categoria;
import pe.edu.idat.svac.model.Unidad;
import pe.edu.idat.svac.service.ArticuloService;
import pe.edu.idat.svac.service.CategoriaService;
import pe.edu.idat.svac.service.InventarioService;
import pe.edu.idat.svac.service.UnidadService;

@Controller
@RequestMapping("/web/almacen")
class AlmacenWebController {

	@Autowired
	ArticuloService articuloService;

	@Autowired
	CategoriaService categoriaService;

	@Autowired
	UnidadService unidadService;

	@Autowired
	InventarioService inventarioService;

	@RequestMapping(value = "/articulos", method = RequestMethod.GET)
	public String articulos(ModelMap model) {

		try {
			List<Categoria> categorias = categoriaService.listAll();
			List<Unidad> unidades = unidadService.listAll();

			ObjectMapper mapper = new ObjectMapper();

			model.addAttribute("categoriaData", mapper.writeValueAsString(categorias));
			model.addAttribute("unidadData", mapper.writeValueAsString(unidades));

		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return "articulos/index";
	}

	@RequestMapping(value = "/categorias", method = RequestMethod.GET)
	public String categorias(ModelMap model) {
		return "categorias/index";
	}

	@RequestMapping(value = "/unidades", method = RequestMethod.GET)
	public String unidades(ModelMap model) {

		try {
			List<Categoria> categorias = categoriaService.listAll();
			ObjectMapper mapper = new ObjectMapper();

			model.addAttribute("categoriaData", mapper.writeValueAsString(categorias));

		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return "unidades/index";
	}

	@RequestMapping(value = "/inventario", method = RequestMethod.GET)
	public ModelAndView inventario() {
		ModelAndView model = new ModelAndView("almacen/inventario/index");
		model.addObject("inventarioData", inventarioService.listAll());
		return model;
	}

	@RequestMapping(value = "/inventario/ingreso", method = RequestMethod.GET)
	public ModelAndView inventarioIngreso() throws Exception {
		ModelAndView model = new ModelAndView("almacen/inventario/ingreso");
		model.addObject("inventarioData", inventarioService.getItemsByIngreso());
		return model;
	}

	@RequestMapping(value = "/inventario/salida", method = RequestMethod.GET)
	public ModelAndView inventarioSalida() throws Exception {
		ModelAndView model = new ModelAndView("almacen/inventario/salida");
		model.addObject("inventarioData", inventarioService.getItemsBySalida());
		return model;
	}

}
