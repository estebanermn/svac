package pe.edu.idat.svac.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.edu.idat.svac.model.Articulo;
import pe.edu.idat.svac.model.Cliente;
import pe.edu.idat.svac.model.DocumentoVenta;
import pe.edu.idat.svac.model.DocumentoVentaDetalle;
import pe.edu.idat.svac.service.ArticuloService;
import pe.edu.idat.svac.service.ClienteService;
import pe.edu.idat.svac.service.DocumentoVentaDetalleService;
import pe.edu.idat.svac.service.DocumentoVentaService;
import pe.edu.idat.svac.utils.Estado;

@Controller
@RequestMapping("/web/ventas")
class VentasWebController extends BaseWebController {

	@Autowired
	ArticuloService articuloService;

	@Autowired
	ClienteService clienteService;

	@Autowired
	DocumentoVentaService documentoVentaService;

	@Autowired
	DocumentoVentaDetalleService documentoVentaDetalleService;

	@RequestMapping(value = "/pedidos", method = RequestMethod.GET)
	public String pedidos(ModelMap model) {
		return "ventas/pedidos/index";
	}

	@RequestMapping(value = "/pedidos/add", method = RequestMethod.GET)
	public ModelAndView ventasAdd() {

		ModelAndView model = new ModelAndView("ventas/pedidos/add");

		List<Articulo> articulos = articuloService.listAll();
		List<Cliente> clientes = clienteService.listAll();

		DocumentoVenta venta = new DocumentoVenta();
		venta.setEstado(Estado.NUEVO);
		int empleadoId = getEmpleadoId();
		int impuesto = getImpuestoIGV();

		ObjectMapper mapper = new ObjectMapper();

		try {
			model.addObject("articuloData", mapper.writeValueAsString(articulos));
			model.addObject("clienteData", mapper.writeValueAsString(clientes));

			model.addObject("ventaData", venta);

			model.addObject("empleadoId", empleadoId);
			model.addObject("impuesto", impuesto);

		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return model;
	}

	@RequestMapping(value = "/pedidos/{id}", method = RequestMethod.GET)
	public ModelAndView ventasDetalle(@PathVariable int id) throws Exception {

		ModelAndView model = new ModelAndView("ventas/pedidos/show");

		int empleadoId = getEmpleadoId();
		int impuesto = getImpuestoIGV();

		ObjectMapper mapper = new ObjectMapper();

		try {

			List<Articulo> articulos = articuloService.listAll();

			DocumentoVenta venta = documentoVentaService.getById(id);
			List<DocumentoVentaDetalle> items = documentoVentaDetalleService.getItemsById(id);
			venta.setItems(items);

			model.addObject("articuloData", mapper.writeValueAsString(articulos));

			model.addObject("ventaData", venta);
			model.addObject("ventaDetallesData", mapper.writeValueAsString(venta.getItems()));

			model.addObject("empleadoId", empleadoId);
			model.addObject("impuesto", impuesto);

		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return model;
	}

	@RequestMapping(value = "/clientes", method = RequestMethod.GET)
	public String clientes(ModelMap model) {
		return "clientes/index";
	}

	@RequestMapping(value = "/historial", method = RequestMethod.GET)
	public String historialIndex() {
		return "ventas/historial/index";
	}

	@RequestMapping(value = "/historial/{value}", method = RequestMethod.GET)
	public ModelAndView historial(@PathVariable String value) {

		// Valores: anual, quincenal, mensual, semana

		ModelAndView model = new ModelAndView("ventas/historial/historial");
		model.addObject("tipoHistorial", value);
		return model;
	}

}
