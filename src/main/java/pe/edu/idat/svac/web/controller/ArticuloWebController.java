package pe.edu.idat.svac.web.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.edu.idat.svac.model.Categoria;
import pe.edu.idat.svac.model.Unidad;
import pe.edu.idat.svac.service.ArticuloService;
import pe.edu.idat.svac.service.CategoriaService;
import pe.edu.idat.svac.service.UnidadService;

@Controller
@RequestMapping("/web/articulos")
class ArticuloWebController {

	@Autowired
	ArticuloService articuloService;

	@Autowired
	CategoriaService categoriaService;

	@Autowired
	UnidadService unidadService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(ModelMap model) {

		List<Categoria> categorias = categoriaService.listAll();
		List<Unidad> unidades = unidadService.listAll();

		ObjectMapper mapper = new ObjectMapper();

		try {
			model.addAttribute("categoriaData", mapper.writeValueAsString(categorias));
			model.addAttribute("unidadData", mapper.writeValueAsString(unidades));

		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return "articulos/index";
	}

}
