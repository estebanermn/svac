package pe.edu.idat.svac.service;

import java.util.List;

import pe.edu.idat.svac.model.Cliente;

public interface ClienteService {

	public List<Cliente> listAll();

	public void create(Cliente cliente);

	public Cliente update(Cliente cliente) throws Exception;
	
	public Cliente updateEstado(Cliente cliente) throws Exception;

	public Cliente getById(int id) throws Exception;

}
