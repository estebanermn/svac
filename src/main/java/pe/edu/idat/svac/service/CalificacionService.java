package pe.edu.idat.svac.service;

import pe.edu.idat.svac.model.Calificacion;

public interface CalificacionService {

	public void create(Calificacion calificacion);

	public Calificacion update(Calificacion calificacion) throws Exception;

	public Calificacion getById(int id) throws Exception;

	public Calificacion getByVentaId(int id) throws Exception;

	public void calcularPromedio(int empleadoId) throws Exception;

}
