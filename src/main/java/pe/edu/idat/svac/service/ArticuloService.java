package pe.edu.idat.svac.service;

import java.util.List;

import pe.edu.idat.svac.model.Articulo;

public interface ArticuloService {

	public List<Articulo> listAll();

	public void create(Articulo articulo);

	public Articulo update(Articulo articulo) throws Exception;
	
	public Articulo updateEstado(Articulo articulo) throws Exception;

	public Articulo getById(int id) throws Exception;
	
	public void aumentarStock(int articuloId, int cantidad) throws Exception;
	
	public void descontarStock(int articuloId, int cantidad) throws Exception;

}
