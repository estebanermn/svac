package pe.edu.idat.svac.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.svac.dao.impl.ArticuloDaoImpl;
import pe.edu.idat.svac.model.Articulo;
import pe.edu.idat.svac.service.ArticuloService;

@Service("ArticuloService")
public class ArticuloServiceImpl implements ArticuloService {

	@Autowired
	ArticuloDaoImpl articuloDao;

	public void setArticuloDao(ArticuloDaoImpl articuloDao) {
		this.articuloDao = articuloDao;
	}

	@Override
	public List<Articulo> listAll() {
		return articuloDao.listAll();
	}

	@Override
	public void create(Articulo articulo) {
		articuloDao.add(articulo);
	}

	@Override
	public Articulo update(Articulo articulo) throws Exception {
		int id = articulo.getId();
		articuloDao.update(articulo);
		return getById(id);
	}
	

	@Override
	public Articulo updateEstado(Articulo articulo) throws Exception {
		int id = articulo.getId();
		articuloDao.updateEstado(articulo);
		return getById(id);
	}


	@Override
	public Articulo getById(int id) throws Exception {
		return articuloDao.getById(id);
	}

	@Override
	public void aumentarStock(int articuloId, int cantidad) throws Exception {
		articuloDao.aumentarStock(articuloId, cantidad);
	}

	@Override
	public void descontarStock(int articuloId, int cantidad) throws Exception {
		articuloDao.descontarStock(articuloId, cantidad);
	}

}
