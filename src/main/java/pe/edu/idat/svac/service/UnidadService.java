package pe.edu.idat.svac.service;

import java.util.List;

import pe.edu.idat.svac.model.Unidad;

public interface UnidadService {

	public List<Unidad> listAll();

	public void create(Unidad unidad);

	public Unidad update(Unidad unidad) throws Exception;
	
	public Unidad updateEstado(Unidad unidad) throws Exception;

	public Unidad getById(int id) throws Exception;

}
