package pe.edu.idat.svac.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.svac.dao.impl.WidgetDaoImpl;
import pe.edu.idat.svac.model.Widget;
import pe.edu.idat.svac.service.WidgetService;

@Service("WidgetService")
public class WidgetServiceImpl implements WidgetService {

	@Autowired
	WidgetDaoImpl widgetDao;

	public void setwidgetDao(WidgetDaoImpl widgetDao) {
		this.widgetDao = widgetDao;
	}

	@Override
	public int getWidget(int value) {
		int result = 0;
		try {
			switch (value) {
			case 1:
				result = widgetDao.getWidget1();
				break;

			case 2:
				result = widgetDao.getWidget2();
				break;

			case 3:
				result = widgetDao.getWidget3();
				break;

			case 4:
				result = widgetDao.getWidget4();
				break;

			default:
				result = 0;
				break;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Widget> getProductosMasVendido() {
		return widgetDao.getWidgetProductosMasVendido();
	}
	
	@Override
	public List<Widget> getProductosMenosVendido() {
		return widgetDao.getWidgetProductosMenosVendido();
	}

}
