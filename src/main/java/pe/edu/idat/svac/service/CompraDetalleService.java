package pe.edu.idat.svac.service;

import java.util.List;

import pe.edu.idat.svac.model.CompraDetalle;

public interface CompraDetalleService {


	public void create(CompraDetalle entidad) throws Exception;

	public List<CompraDetalle> getItemsById(int id) throws Exception;
}
