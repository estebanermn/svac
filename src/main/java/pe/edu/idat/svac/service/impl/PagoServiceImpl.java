package pe.edu.idat.svac.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.svac.dao.impl.PagoDaoImpl;
import pe.edu.idat.svac.model.DocumentoVenta;
import pe.edu.idat.svac.model.DocumentoVentaDetalle;
import pe.edu.idat.svac.model.Inventario;
import pe.edu.idat.svac.model.Pago;
import pe.edu.idat.svac.service.DocumentoVentaDetalleService;
import pe.edu.idat.svac.service.DocumentoVentaService;
import pe.edu.idat.svac.service.InventarioService;
import pe.edu.idat.svac.service.PagoService;
import pe.edu.idat.svac.utils.Estado;
import pe.edu.idat.svac.utils.Util;

/**
 * @author Esteban
 *
 */
@Service("PagoService")
public class PagoServiceImpl implements PagoService {

	@Autowired
	PagoDaoImpl pagoDao;

	@Autowired
	DocumentoVentaService documentoService;

	@Autowired
	DocumentoVentaDetalleService documentoDetalleService;

	@Autowired
	InventarioService inventarioService;

	public void setPagoDao(PagoDaoImpl pagoDao) {
		this.pagoDao = pagoDao;
	}

	@Override
	public List<Pago> listAll() {
		return pagoDao.listAll();
	}

	@Override
	public void create(Pago pago) {
		try {
			DocumentoVenta venta = documentoService.getById(pago.getDocumentoVentaId());
			java.sql.Date fecha = Util.getTimeNow();

			pago.setFecha(fecha);
			pago.setMonto(venta.getTotal());
			pago.setTipoPago(Estado.PAGADO);
			pagoDao.add(pago);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void updateCanceled(int ventaId) throws Exception {
		Pago pago = new Pago();
		pago.setDocumentoVentaId(ventaId);
		pagoDao.updateCanceled(pago);
	}

	@Override
	public Pago getById(int id) throws Exception {
		return pagoDao.getById(id);
	}

	@Override
	public int getLastId() throws Exception {
		return pagoDao.getLastId();
	}

	@Override
	public void runTasks(int documentoVentaId) throws Exception {

		// Tareas al crear un Pago

		// UPDATE DocumentoVenta SET Status = 'Pagado' WHERE id = {id}
		documentoService.updatePagado(documentoVentaId);

		// Al pagar una venta se debe actualizar la fecha de pago
		documentoService.updateFechaPagado(documentoVentaId);

		// Crear registro Inventario para salida de stock
		List<DocumentoVentaDetalle> items = documentoDetalleService.getItemsById(documentoVentaId);

		for (DocumentoVentaDetalle detalle : items) {
			int articuloId = detalle.getArticuloId();
			int cantidad = detalle.getCantidad();

			Inventario inventario = new Inventario();
			inventario.setArticuloId(articuloId);
			inventario.setStockOut(cantidad);
			inventario.setDocumentoVentaId(documentoVentaId);
			inventario.setPrecioUnitario(detalle.getPrecio());

			inventarioService.crearSalida(inventario);
		}
	}

}
