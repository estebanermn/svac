package pe.edu.idat.svac.service;

import java.util.List;

import pe.edu.idat.svac.model.DocumentoVenta;

public interface DocumentoVentaService {

	public List<DocumentoVenta> listAll();

	public void create(DocumentoVenta entidad) throws Exception;

	public DocumentoVenta updateCanceled(DocumentoVenta entidad) throws Exception;

	public void updatePagado(int ventaId) throws Exception;

	public void updateFechaPagado(int ventaId) throws Exception;

	public void updateCalificacion(int ventaId, int calificacion) throws Exception;

	public DocumentoVenta getById(int id) throws Exception;

	public int getLastId() throws Exception;

	public List<DocumentoVenta> getVentasByEmpleadoId(int id) throws Exception;

	public List<DocumentoVenta> getVentasByHistoria(String historial) throws Exception;

	public void retornarStock(int ventaId) throws Exception;

}
