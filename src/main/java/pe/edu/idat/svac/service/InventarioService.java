package pe.edu.idat.svac.service;

import java.util.List;

import pe.edu.idat.svac.model.Inventario;

public interface InventarioService {

	public List<Inventario> listAll();

	public void create(Inventario invetario);

	public Inventario getById(int id) throws Exception;

	public void crearIngreso(Inventario inventario) throws Exception;

	public void crearSalida(Inventario inventario) throws Exception;

	public List<Inventario> getInvetarioByArticulo(int id) throws Exception;

	public List<Inventario> getItemsByIngreso() throws Exception;

	public List<Inventario> getItemsBySalida() throws Exception;

}
