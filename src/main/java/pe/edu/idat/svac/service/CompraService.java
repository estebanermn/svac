package pe.edu.idat.svac.service;

import java.util.List;

import pe.edu.idat.svac.model.Compra;

public interface CompraService {

	public List<Compra> listAll();

	public void create(Compra entidad) throws Exception;

	public Compra updateApproved(Compra entidad) throws Exception;

	public Compra updatePaid(Compra entidad) throws Exception;

	public Compra updateReceived(Compra entidad) throws Exception;

	public Compra updateCanceled(Compra entidad) throws Exception;

	public Compra getById(int id) throws Exception;

	public int getLastId() throws Exception;

	public List<Compra> getVentasByHistoria(String historial) throws Exception;

	public void ingresarStock(int compraId) throws Exception;

	public void retornarStock(int compraId) throws Exception;

}
