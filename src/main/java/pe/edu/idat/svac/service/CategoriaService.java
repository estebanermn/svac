package pe.edu.idat.svac.service;

import java.util.List;
import pe.edu.idat.svac.model.Categoria;

public interface CategoriaService {

	public List<Categoria> listAll();

	public void create(Categoria categoria)  throws Exception;

	public Categoria update(Categoria categoria) throws Exception;
	
	public Categoria updateEstado(Categoria categoria) throws Exception;

	public Categoria getById(int id) throws Exception;

}
