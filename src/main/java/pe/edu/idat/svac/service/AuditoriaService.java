package pe.edu.idat.svac.service;

import java.util.List;

import pe.edu.idat.svac.model.Auditoria;

public interface AuditoriaService {

	public List<Auditoria> listAll();

	public void create(Auditoria auditoria);

	public Auditoria update(Auditoria auditoria) throws Exception;

	public Auditoria getById(int id) throws Exception;

}
