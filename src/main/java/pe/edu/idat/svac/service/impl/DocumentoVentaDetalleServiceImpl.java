package pe.edu.idat.svac.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.svac.dao.impl.DocumentoVentaDetalleDaoImpl;
import pe.edu.idat.svac.model.DocumentoVentaDetalle;
import pe.edu.idat.svac.service.DocumentoVentaDetalleService;

@Service("DocumentoVentaDetalleService")
public class DocumentoVentaDetalleServiceImpl implements DocumentoVentaDetalleService {

	@Autowired
	DocumentoVentaDetalleDaoImpl ventaDetalleDao;

	public void setDocumentoVentaDao(DocumentoVentaDetalleDaoImpl ventaDetalleDao) {
		this.ventaDetalleDao = ventaDetalleDao;
	}

	@Override
	public void create(DocumentoVentaDetalle entidad) throws Exception {
		ventaDetalleDao.add(entidad);
	}

	@Override
	public List<DocumentoVentaDetalle> getItemsById(int id) throws Exception {
		return ventaDetalleDao.getItemsById(id);
	}

}
