package pe.edu.idat.svac.service;

import java.util.List;

import pe.edu.idat.svac.model.Pago;

public interface PagoService {

	public List<Pago> listAll();

	public void create(Pago pago);

	public void updateCanceled(int ventaId) throws Exception;

	public Pago getById(int id) throws Exception;

	public int getLastId() throws Exception;
	
	public void runTasks(int ventaId) throws Exception;

}
