package pe.edu.idat.svac.service;


import java.util.List;

import pe.edu.idat.svac.model.DocumentoVentaDetalle;

public interface DocumentoVentaDetalleService {


	public void create(DocumentoVentaDetalle entidad) throws Exception;

	public List<DocumentoVentaDetalle> getItemsById(int id) throws Exception;

}
