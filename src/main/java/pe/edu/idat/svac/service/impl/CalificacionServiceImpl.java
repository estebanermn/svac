package pe.edu.idat.svac.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.svac.dao.impl.CalificacionDaoImpl;
import pe.edu.idat.svac.model.Calificacion;
import pe.edu.idat.svac.service.CalificacionService;

@Service("CalificacionService")
public class CalificacionServiceImpl implements CalificacionService {

	@Autowired
	CalificacionDaoImpl calificacionDao;

	@Autowired
	EmpleadoServiceImpl empleadoDao;

	@Autowired
	DocumentoVentaServiceImpl documentoVentaDao;

	public void setCalificacionDao(CalificacionDaoImpl calificacionDao) {
		this.calificacionDao = calificacionDao;
	}

	@Override
	public void create(Calificacion calificacion) {
		try {
			calificacionDao.add(calificacion);
			documentoVentaDao.updateCalificacion(calificacion.getDocumentoVentaId(), calificacion.getCalificacion());
			calcularPromedio(calificacion.getEmpleadoId());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Calificacion update(Calificacion calificacion) throws Exception {
		int id = calificacion.getId();
		calificacionDao.update(calificacion);
		return getById(id);
	}

	@Override
	public Calificacion getById(int id) throws Exception {
		return calificacionDao.getById(id);
	}

	@Override
	public Calificacion getByVentaId(int id) throws Exception {
		Calificacion entidad = new Calificacion();
		
		try {
			return calificacionDao.getByVentaId(id);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return entidad;
	}

	@Override
	public void calcularPromedio(int empleadoId) throws Exception {
		Calificacion calificacion = new Calificacion();
		calificacion.setEmpleadoId(empleadoId);
		calificacionDao.calcularPromedio(calificacion);

	}

}
