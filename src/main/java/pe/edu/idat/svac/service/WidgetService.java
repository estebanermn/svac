package pe.edu.idat.svac.service;

import java.util.List;

import pe.edu.idat.svac.model.Widget;

public interface WidgetService {

	public int getWidget(int value);
	
	public List<Widget> getProductosMasVendido();
	
	public List<Widget> getProductosMenosVendido();

}
