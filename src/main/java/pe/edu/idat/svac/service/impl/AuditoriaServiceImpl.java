package pe.edu.idat.svac.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.svac.dao.impl.AuditoriaDaoImpl;
import pe.edu.idat.svac.model.Auditoria;
import pe.edu.idat.svac.service.AuditoriaService;

@Service("AuditoriaService")
public class AuditoriaServiceImpl implements AuditoriaService {

	@Autowired
	AuditoriaDaoImpl auditoriaDao;

	public void setauditoriaDao(AuditoriaDaoImpl auditoriaDao) {
		this.auditoriaDao = auditoriaDao;
	}

	@Override
	public List<Auditoria> listAll() {
		return auditoriaDao.listAll();
	}

	@Override
	public void create(Auditoria auditoria) {
		auditoriaDao.add(auditoria);
	}

	@Override
	public Auditoria update(Auditoria auditoria) throws Exception {
		int id = auditoria.getId();
		auditoriaDao.update(auditoria);
		return getById(id);
	}

	@Override
	public Auditoria getById(int id) throws Exception {
		return auditoriaDao.getById(id);
	}

}
