package pe.edu.idat.svac.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.svac.dao.impl.ProveedorDaoImpl;
import pe.edu.idat.svac.model.Proveedor;
import pe.edu.idat.svac.service.ProveedorService;

@Service("ProveedorService")
public class ProveedorServiceImpl implements ProveedorService {

	@Autowired
	ProveedorDaoImpl proveedorDao;

	public void setProveedorDao(ProveedorDaoImpl proveedorDao) {
		this.proveedorDao = proveedorDao;
	}

	@Override
	public List<Proveedor> listAll() {
		return proveedorDao.listAll();
	}

	@Override
	public void create(Proveedor entidad) {
		proveedorDao.add(entidad);
	}

	@Override
	public Proveedor update(Proveedor entidad) throws Exception {
		int id = entidad.getId();
		proveedorDao.update(entidad);
		return getById(id);
	}

	@Override
	public Proveedor updateEstado(Proveedor entidad) throws Exception {
		int id = entidad.getId();
		proveedorDao.updateEstado(entidad);
		return getById(id);
	}

	@Override
	public Proveedor getById(int id) throws Exception {
		return proveedorDao.getById(id);
	}

}
