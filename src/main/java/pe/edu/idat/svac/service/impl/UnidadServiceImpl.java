package pe.edu.idat.svac.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.svac.dao.impl.UnidadDaoImpl;
import pe.edu.idat.svac.model.Unidad;
import pe.edu.idat.svac.service.UnidadService;

@Service("UnidadService")
public class UnidadServiceImpl implements UnidadService {

	@Autowired
	UnidadDaoImpl unidadDao;

	public void setUnidadDao(UnidadDaoImpl unidadDao) {
		this.unidadDao = unidadDao;
	}

	@Override
	public List<Unidad> listAll() {
		return unidadDao.listAll();
	}

	@Override
	public void create(Unidad unidad) {
		unidadDao.add(unidad);
	}

	@Override
	public Unidad update(Unidad unidad) throws Exception {
		int id = unidad.getId();
		unidadDao.update(unidad);
		return getById(id);
	}
	
	@Override
	public Unidad updateEstado(Unidad unidad) throws Exception {
		int id = unidad.getId();
		unidadDao.updateEstado(unidad);
		return getById(id);
	}

	@Override
	public Unidad getById(int id) throws Exception {
		return unidadDao.getById(id);
	}

}
