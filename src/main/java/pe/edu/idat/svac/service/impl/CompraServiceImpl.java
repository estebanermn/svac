package pe.edu.idat.svac.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.svac.dao.impl.CompraDaoImpl;
import pe.edu.idat.svac.model.Articulo;
import pe.edu.idat.svac.model.Compra;
import pe.edu.idat.svac.model.CompraDetalle;
import pe.edu.idat.svac.model.Inventario;
import pe.edu.idat.svac.service.ArticuloService;
import pe.edu.idat.svac.service.CompraDetalleService;
import pe.edu.idat.svac.service.CompraService;
import pe.edu.idat.svac.service.InventarioService;
import pe.edu.idat.svac.utils.Estado;
import pe.edu.idat.svac.utils.Historial;
import pe.edu.idat.svac.utils.Util;

@Service("CompraService")
public class CompraServiceImpl implements CompraService {

	@Autowired
	CompraDaoImpl compraDao;

	@Autowired
	CompraDetalleService compraDetalleService;

	@Autowired
	InventarioService inventarioService;

	@Autowired
	ArticuloService articuloService;

	public void setCompraDao(CompraDaoImpl compraDao) {
		this.compraDao = compraDao;
	}

	@Override
	public List<Compra> listAll() {
		return compraDao.listAll();
	}

	@Override
	public void create(Compra entidad) throws Exception {
		entidad.setFecha(Util.getTimeNow());
		entidad.setEstado(Estado.PENDIENTE);
		compraDao.add(entidad);

		int compraId = compraDao.getLastId();

		// Agregar detalles
		if (entidad.getItems() != null) {
			for (CompraDetalle detalle : entidad.getItems()) {

				Articulo articulo = articuloService.getById(detalle.getArticuloId());

				detalle.setCompraId(compraId);
				detalle.setPrecio(articulo.getPrecioProveedor());
				compraDetalleService.create(detalle);
			}
		}
	}

	@Override
	public Compra updateApproved(Compra entidad) throws Exception {
		int id = entidad.getId();
		compraDao.updateApproved(entidad);
		return getById(id);
	}

	@Override
	public Compra updatePaid(Compra entidad) throws Exception {
		int id = entidad.getId();
		compraDao.updatePaid(entidad);
		return getById(id);
	}

	@Override
	public Compra updateReceived(Compra entidad) throws Exception {
		int id = entidad.getId();
		compraDao.updateReceived(entidad);
		return getById(id);
	}

	@Override
	public Compra updateCanceled(Compra entidad) throws Exception {
		int id = entidad.getId();
		compraDao.updateCanceled(entidad);
		return getById(id);
	}

	@Override
	public Compra getById(int id) throws Exception {
		return compraDao.getById(id);
	}

	@Override
	public int getLastId() throws Exception {
		return compraDao.getLastId();
	}

	@Override
	public List<Compra> getVentasByHistoria(String historial) throws Exception {
		List<Compra> lista = null;

		if (historial.equals(Historial.ANUAL)) {
			lista = compraDao.getVentasByYear();
		}

		if (historial.equals(Historial.MENSUAL)) {
			lista = compraDao.getVentasByMes();
		}

		if (historial.equals(Historial.QUINCENA)) {
			lista = compraDao.getVentasByQuincena();
		}

		if (historial.equals(Historial.SEMANA)) {
			lista = compraDao.getVentasBySemana();
		}
		return lista;
	}

	@Override
	public void ingresarStock(int compraId) throws Exception {

		// Crear registros en Inventario para actualizar stock
		List<CompraDetalle> items = compraDetalleService.getItemsById(compraId);

		for (CompraDetalle detalle : items) {
			int articuloId = detalle.getArticuloId();
			int cantidad = detalle.getCantidad();

			Inventario inventario = new Inventario();
			inventario.setArticuloId(articuloId);
			inventario.setStockIn(cantidad);
			inventario.setOrdenCompraId(compraId);
			inventario.setPrecioUnitario(detalle.getPrecio());

			inventarioService.crearIngreso(inventario);
		}

	}

	@Override
	public void retornarStock(int compraId) throws Exception {

		// Crear registro Inventario para retornar stock
		List<CompraDetalle> items = compraDetalleService.getItemsById(compraId);

		for (CompraDetalle detalle : items) {
			int articuloId = detalle.getArticuloId();
			int cantidad = detalle.getCantidad();

			Inventario inventario = new Inventario();
			inventario.setArticuloId(articuloId);
			inventario.setStockOut(cantidad);
			inventario.setOrdenCompraId(compraId);
			inventario.setPrecioUnitario(detalle.getPrecio());

			inventarioService.crearSalida(inventario);
		}

	}

}
