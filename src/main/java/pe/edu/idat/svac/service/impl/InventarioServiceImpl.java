package pe.edu.idat.svac.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.svac.dao.impl.InventarioDaoImpl;
import pe.edu.idat.svac.model.Inventario;
import pe.edu.idat.svac.service.ArticuloService;
import pe.edu.idat.svac.service.InventarioService;

@Service("InventarioService")
public class InventarioServiceImpl implements InventarioService {

	@Autowired
	InventarioDaoImpl inventarioDao;

	@Autowired
	ArticuloService articuloService;

	public void setTipoInventarioDao(InventarioDaoImpl inventarioDao) {
		this.inventarioDao = inventarioDao;
	}

	@Override
	public List<Inventario> listAll() {
		return inventarioDao.listAll();
	}

	@Override
	public void create(Inventario invetario) {
		inventarioDao.add(invetario);
	}

	@Override
	public Inventario getById(int id) throws Exception {
		return inventarioDao.getById(id);
	}

	@Override
	public void crearIngreso(Inventario inventario) throws Exception {

		inventarioDao.crearIngreso(inventario);

		int articuloId = inventario.getArticuloId();
		int cantidad = inventario.getStockIn();

		articuloService.aumentarStock(articuloId, cantidad);
	}

	@Override
	public void crearSalida(Inventario inventario) throws Exception {

		inventarioDao.crearSalida(inventario);

		int articuloId = inventario.getArticuloId();
		int cantidad = inventario.getStockOut();

		articuloService.descontarStock(articuloId, cantidad);
	}

	@Override
	public List<Inventario> getInvetarioByArticulo(int id) throws Exception {
		return inventarioDao.getInvetarioByArticulo(id);
	}

	@Override
	public List<Inventario> getItemsByIngreso() throws Exception {
		return inventarioDao.getItemsByIngreso();
	}

	@Override
	public List<Inventario> getItemsBySalida() throws Exception {
		return inventarioDao.getItemsBySalida();
	}

}
