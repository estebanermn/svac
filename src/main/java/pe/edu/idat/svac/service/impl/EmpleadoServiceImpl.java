package pe.edu.idat.svac.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.svac.model.EmpleadoCumple;
import pe.edu.idat.svac.service.EmpleadoService;
import pe.edu.idat.svac.dao.impl.EmpleadoDaoImpl;
import pe.edu.idat.svac.model.Empleado;

@Service("EmpleadoService")
public class EmpleadoServiceImpl implements EmpleadoService {

	@Autowired
	EmpleadoDaoImpl empleadoDao;

	public void setEmpleadoDAO(EmpleadoDaoImpl empleadoDao) {
		this.empleadoDao = empleadoDao;
	}

	@Override
	public List<Empleado> listAll() {
		return empleadoDao.listAll();
	}

	@Override
	public void create(Empleado entidad) {
		empleadoDao.add(entidad);
	}

	@Override
	public Empleado update(Empleado entidad) throws Exception {
		int id = entidad.getIdEmpleado();
		empleadoDao.update(entidad);
		return getById(id);
	}

	@Override
	public Empleado updateEstado(Empleado entidad) throws Exception {
		int id = entidad.getIdEmpleado();
		empleadoDao.updateEstado(entidad);
		return getById(id);
	}

	@Override
	public Empleado getById(int id) throws Exception {
		return empleadoDao.getById(id);
	}

	@Override
	public List<EmpleadoCumple> listAllCumple() {
		return empleadoDao.listAllCumple();
	}

	@Override
	public List<Empleado> listAllVenta() {
		return empleadoDao.listAllVenta();
	}

}
