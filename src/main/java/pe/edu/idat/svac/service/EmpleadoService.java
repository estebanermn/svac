package pe.edu.idat.svac.service;

import java.util.List;

import pe.edu.idat.svac.model.EmpleadoCumple;
import pe.edu.idat.svac.model.Empleado;

public interface EmpleadoService {

	public List<Empleado> listAll();

	public void create(Empleado entidad);

	public Empleado update(Empleado entidad) throws Exception;
	
	public Empleado updateEstado(Empleado entidad) throws Exception;

	public Empleado getById(int id) throws Exception;

	public List<EmpleadoCumple> listAllCumple();
	
	public List<Empleado> listAllVenta();
}
