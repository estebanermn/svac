package pe.edu.idat.svac.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.svac.dao.impl.DocumentoDaoImpl;
import pe.edu.idat.svac.model.Documento;
import pe.edu.idat.svac.service.DocumentoService;

@Service("DocumentoService")
public class DocumentoServiceImpl implements DocumentoService {

	@Autowired
	DocumentoDaoImpl documentoDao;

	public void setTipoDocumentoDao(DocumentoDaoImpl documentoDao) {
		this.documentoDao = documentoDao;
	}

	@Override
	public List<Documento> listAll() {
		return documentoDao.listAll();
	}

	@Override
	public void create(Documento documento) {
		documentoDao.add(documento);
	}

	@Override
	public Documento update(Documento documento) throws Exception {
		int id = documento.getId();
		documentoDao.update(documento);
		return getById(id);
	}

	@Override
	public Documento getById(int id) throws Exception {
		return documentoDao.getById(id);
	}

}
