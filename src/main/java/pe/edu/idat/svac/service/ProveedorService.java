package pe.edu.idat.svac.service;

import java.util.List;

import pe.edu.idat.svac.model.Proveedor;

public interface ProveedorService {

	public List<Proveedor> listAll();

	public void create(Proveedor proveedor);

	public Proveedor update(Proveedor proveedor) throws Exception;
	
	public Proveedor updateEstado(Proveedor proveedor) throws Exception;

	public Proveedor getById(int id) throws Exception;

}
