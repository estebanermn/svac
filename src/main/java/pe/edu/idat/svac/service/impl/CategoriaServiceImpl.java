package pe.edu.idat.svac.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.svac.dao.impl.CategoriaDaoImpl;
import pe.edu.idat.svac.model.Categoria;
import pe.edu.idat.svac.service.CategoriaService;

@Service("CategoriaService")
public class CategoriaServiceImpl implements CategoriaService {

	@Autowired
	CategoriaDaoImpl categoriaDao;

	public void setCategoriaDao(CategoriaDaoImpl categoriaDao) {
		this.categoriaDao = categoriaDao;
	}

	@Override
	public List<Categoria> listAll() {
		return categoriaDao.listAll();
	}

	@Override
	public void create(Categoria categoria) throws Exception {
		categoriaDao.add(categoria);
	}

	@Override
	public Categoria update(Categoria categoria) throws Exception {
		int id = categoria.getId();
		categoriaDao.update(categoria);
		return getById(id);
	}

	@Override
	public Categoria updateEstado(Categoria categoria) throws Exception {
		int id = categoria.getId();
		categoriaDao.updateEstado(categoria);
		return getById(id);
	}

	@Override
	public Categoria getById(int id) throws Exception {
		return categoriaDao.getById(id);
	}

}
