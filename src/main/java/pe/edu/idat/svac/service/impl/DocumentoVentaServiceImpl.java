package pe.edu.idat.svac.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.svac.dao.impl.DocumentoVentaDaoImpl;
import pe.edu.idat.svac.model.DocumentoVenta;
import pe.edu.idat.svac.model.DocumentoVentaDetalle;
import pe.edu.idat.svac.model.Inventario;
import pe.edu.idat.svac.service.DocumentoVentaDetalleService;
import pe.edu.idat.svac.service.DocumentoVentaService;
import pe.edu.idat.svac.service.InventarioService;
import pe.edu.idat.svac.utils.Historial;
import pe.edu.idat.svac.utils.Util;

@Service("DocumentoVentaService")
public class DocumentoVentaServiceImpl implements DocumentoVentaService {

	@Autowired
	DocumentoVentaDetalleService documentoDetalleService;

	@Autowired
	InventarioService inventarioService;

	@Autowired
	DocumentoVentaDaoImpl ventaDao;

	public void setDocumentoVentaDao(DocumentoVentaDaoImpl ventaDao) {
		this.ventaDao = ventaDao;
	}

	@Override
	public List<DocumentoVenta> listAll() {
		return ventaDao.listAll();
	}

	@Override
	public void create(DocumentoVenta entidad) throws Exception {
		ventaDao.add(entidad);
	}

	@Override
	public DocumentoVenta updateCanceled(DocumentoVenta entidad) throws Exception {
		int id = entidad.getId();
		ventaDao.updateCanceled(entidad);
		return getById(id);
	}

	@Override
	public void updatePagado(int ventaId) throws Exception {
		ventaDao.updatePagado(new DocumentoVenta(ventaId));
	}

	@Override
	public void updateFechaPagado(int ventaId) throws Exception {
		DocumentoVenta venta = new DocumentoVenta(ventaId);
		venta.setFecha(Util.getTimeNow());
		ventaDao.updateFechaPagado(venta);
	}

	@Override
	public void updateCalificacion(int ventaId, int calificacion) throws Exception {
		DocumentoVenta venta = new DocumentoVenta(ventaId);
		venta.setCalificacion(calificacion);
		ventaDao.updateCalificacion(venta);
	}

	@Override
	public DocumentoVenta getById(int id) throws Exception {
		return ventaDao.getById(id);
	}

	@Override
	public int getLastId() throws Exception {
		return ventaDao.getLastId();
	}

	@Override
	public List<DocumentoVenta> getVentasByEmpleadoId(int id) throws Exception {
		return ventaDao.getVentasByEmpleadoId(id);
	}

	@Override
	public List<DocumentoVenta> getVentasByHistoria(String historial) throws Exception {

		List<DocumentoVenta> lista = null;

		if (historial.equals(Historial.ANUAL)) {
			lista = ventaDao.getVentasByYear();
		}

		if (historial.equals(Historial.MENSUAL)) {
			lista = ventaDao.getVentasByMes();
		}

		if (historial.equals(Historial.QUINCENA)) {
			lista = ventaDao.getVentasByQuincena();
		}

		if (historial.equals(Historial.SEMANA)) {
			lista = ventaDao.getVentasBySemana();
		}

		return lista;
	}

	@Override
	public void retornarStock(int ventaId) throws Exception {

		// Crear registro Inventario para retornar stock
		List<DocumentoVentaDetalle> items = documentoDetalleService.getItemsById(ventaId);

		for (DocumentoVentaDetalle detalle : items) {
			int articuloId = detalle.getArticuloId();
			int cantidad = detalle.getCantidad();

			Inventario inventario = new Inventario();
			inventario.setArticuloId(articuloId);
			inventario.setStockIn(cantidad);
			inventario.setDocumentoVentaId(ventaId);
			inventario.setPrecioUnitario(detalle.getPrecio());

			inventarioService.crearIngreso(inventario);
		}

	}

}
