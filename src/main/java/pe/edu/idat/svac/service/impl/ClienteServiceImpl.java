package pe.edu.idat.svac.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.svac.dao.impl.ClienteDaoImpl;
import pe.edu.idat.svac.model.Cliente;
import pe.edu.idat.svac.service.ClienteService;

@Service("ClienteService")
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	ClienteDaoImpl clienteDao;

	public void setClienteDao(ClienteDaoImpl clienteDao) {
		this.clienteDao = clienteDao;
	}

	@Override
	public List<Cliente> listAll() {
		return clienteDao.listAll();
	}

	@Override
	public void create(Cliente cliente) {
		clienteDao.add(cliente);
	}

	@Override
	public Cliente update(Cliente cliente) throws Exception {
		int id = cliente.getId();
		clienteDao.update(cliente);
		return getById(id);
	}
	
	@Override
	public Cliente updateEstado(Cliente cliente) throws Exception {
		int id = cliente.getId();
		clienteDao.updateEstado(cliente);
		return getById(id);
	}

	@Override
	public Cliente getById(int id) throws Exception {
		return clienteDao.getById(id);
	}

}
