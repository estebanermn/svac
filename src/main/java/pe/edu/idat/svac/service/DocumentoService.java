package pe.edu.idat.svac.service;

import java.util.List;

import pe.edu.idat.svac.model.Documento;

public interface DocumentoService {

	public List<Documento> listAll();

	public void create(Documento documento);

	public Documento update(Documento documento) throws Exception;

	public Documento getById(int id) throws Exception;

}
