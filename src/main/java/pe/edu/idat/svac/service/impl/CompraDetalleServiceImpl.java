package pe.edu.idat.svac.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.svac.dao.impl.CompraDetalleDaoImpl;
import pe.edu.idat.svac.model.CompraDetalle;
import pe.edu.idat.svac.service.CompraDetalleService;;

@Service("CompraDetalleService")
public class CompraDetalleServiceImpl implements CompraDetalleService {

	@Autowired
	CompraDetalleDaoImpl compraDetalleDao;

	public void setCompraDao(CompraDetalleDaoImpl compraDetalleDao) {
		this.compraDetalleDao = compraDetalleDao;
	}

	@Override
	public void create(CompraDetalle entidad) throws Exception {
		compraDetalleDao.add(entidad);
	}

	@Override
	public List<CompraDetalle> getItemsById(int id) throws Exception {
		return compraDetalleDao.getItemsById(id);
	}

}
