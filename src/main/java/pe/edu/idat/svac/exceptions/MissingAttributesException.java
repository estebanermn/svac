package pe.edu.idat.svac.exceptions;

import org.springframework.http.HttpStatus;

@SuppressWarnings("serial")
public class MissingAttributesException extends Exception {

	private final HttpStatus statusCode;
	private String errorMessage;

	public HttpStatus getStatusCode() {
		return statusCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public MissingAttributesException(HttpStatus status, String message) {
		super(message);
		this.statusCode = status;
		this.errorMessage = message;
	}

}
