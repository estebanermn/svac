package pe.edu.idat.svac.error;

public class ItemResponse {

	public int status;
	public String message;

	public ItemResponse(int status, String message) {
		this.status = status;
		this.message = message;
	}

} 