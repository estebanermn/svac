package pe.edu.idat.svac.error;

public class ErrorMsg {

	public static final String DATA_INVALIDA = "Error en los datos enviados.";
	public static final String RECURSO_NO_ENCONTRADO = "Recurso no encontrado.";

}
