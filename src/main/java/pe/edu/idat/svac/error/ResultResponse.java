package pe.edu.idat.svac.error;

public class ResultResponse {

	private Boolean result = true;

	public ResultResponse() {
	}

	public ResultResponse(Boolean result) {
		this.result = result;
	}

	public Boolean getResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}

}
