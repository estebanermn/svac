package pe.edu.idat.svac.error;

import java.util.ArrayList;

public class ErrorResponse {

	public ArrayList<ItemResponse> errors;

	public ErrorResponse() {
		this.errors = new ArrayList<ItemResponse>();
	}

	public void addError(int status, String message) {
		errors.add(new ItemResponse(status, message));
	}

}
