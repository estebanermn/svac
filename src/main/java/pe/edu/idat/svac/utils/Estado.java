package pe.edu.idat.svac.utils;

public class Estado {

	// @TODO Error: unmappable character for encoding UTF-8

	public static final String ANULADO = "Anulado";

	public static final String APROBADO = "Aprobado";

	public static final String NUEVO = "Nuevo";

	public static final String PAGADO = "Pagado";

	public static final String PENDIENTE = "Pendiente";

	public static final String RECIBIDO = "Recibido";

	// Almacen

	public static final String ADJUSTE = "Adjuste";
	public static final String ADJUSTE_DESC = "Adjuste de articulo";

	public static final String INGRESO = "Ingreso";
	public static final String INGRESO_DESC = "Ingreso de articulo";

	public static final String SALIDA = "Salida";
	public static final String SALIDA_DESC = "Salida de articulo";

}
