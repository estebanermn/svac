package pe.edu.idat.svac.utils;

import java.sql.Date;
import java.util.Calendar;

public class Util {

	public static boolean isEmpty(final String s) {
		return s == null || s.trim().isEmpty();
	}

	public static java.sql.Date getTimeNow() {
		java.sql.Date timeNow = new Date(Calendar.getInstance().getTimeInMillis());
		return timeNow;
	}
}
