package pe.edu.idat.svac.utils;

import java.sql.Date;

import pe.edu.idat.svac.model.Auditoria;

public class AuditoriaRegistro {

	public static final String REGISTRO_COMPRA_CREADA = "Se ha generado una nueva orden de compra.";
	public static final String REGISTRO_COMPRA_APROBADA = "Se ha aprobado una orden de compra.";
	public static final String REGISTRO_COMPRA_PAGADA = "Se ha pagado la orden de compra.";
	public static final String REGISTRO_COMPRA_RECIBIDA = "Se ha recibido la orden de compra.";
	public static final String REGISTRO_COMPRA_ANULADO = "Se ha anulado la orden de compra.";

	public static final String REGISTRO_VENTA_CREADA = "Se ha generado una nueva venta.";
	public static final String REGISTRO_VENTA_ANULADO = "Se ha anulado el documento de venta.";
	public static final String REGISTRO_VENTA_PAGADA = "Se ha pagado el documento de venta.";

	public static Auditoria crearRegistro(int empleadoId, String estado, String descripcion) {

		Date fecha = Util.getTimeNow();
		Auditoria auditoria = new Auditoria();
		auditoria.setFecha(fecha);
		auditoria.setEmpleadoId(empleadoId);
		auditoria.setEstado(estado);
		auditoria.setDescripcion(descripcion);
		return auditoria;
	}

}
