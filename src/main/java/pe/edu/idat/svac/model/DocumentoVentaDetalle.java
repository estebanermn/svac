package pe.edu.idat.svac.model;

public class DocumentoVentaDetalle {

	public int id;
	public int documentoVentaId;
	public int articuloId;
	public int cantidad;
	public Double precio;
	public Double descuento;
	public Double total;

	public DocumentoVentaDetalle() {
		super();
	}

	public DocumentoVentaDetalle(int documentoVentaId) {
		super();
		this.documentoVentaId = documentoVentaId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDocumentoVentaId() {
		return documentoVentaId;
	}

	public void setDocumentoVentaId(int documentoVentaId) {
		this.documentoVentaId = documentoVentaId;
	}

	public int getArticuloId() {
		return articuloId;
	}

	public void setArticuloId(int articuloId) {
		this.articuloId = articuloId;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public Double getDescuento() {
		return descuento;
	}

	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

}
