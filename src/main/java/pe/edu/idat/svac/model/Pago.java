package pe.edu.idat.svac.model;

import java.sql.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Pago {

	public int id;
	public int documentoVentaId;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	public Date fecha;
	public Double monto;
	public String tipoPago; // Pagado, Anulado

	public Pago() {
		super();
	}

	public Pago(int id) {
		super();
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDocumentoVentaId() {
		return documentoVentaId;
	}

	public void setDocumentoVentaId(int documentoVentaId) {
		this.documentoVentaId = documentoVentaId;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Double getMonto() {
		return monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}

	public String getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}

}
