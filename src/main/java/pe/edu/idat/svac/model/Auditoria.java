package pe.edu.idat.svac.model;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Auditoria {

	public int id;
	public int ordenCompraId;
	public int documentoVentaId;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	public Date fecha;
	public int empleadoId;
	public String estado;
	public String descripcion;

	public Auditoria() {
		super();
	}

	public Auditoria(int id) {
		super();
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getOrdenCompraId() {
		return ordenCompraId;
	}

	public void setOrdenCompraId(int ordenCompraId) {
		this.ordenCompraId = ordenCompraId;
	}

	public int getDocumentoVentaId() {
		return documentoVentaId;
	}

	public void setDocumentoVentaId(int documentoVentaId) {
		this.documentoVentaId = documentoVentaId;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public int getEmpleadoId() {
		return empleadoId;
	}

	public void setEmpleadoId(int empleadoId) {
		this.empleadoId = empleadoId;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
