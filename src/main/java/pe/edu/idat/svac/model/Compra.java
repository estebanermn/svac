package pe.edu.idat.svac.model;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Compra {

	public int id;
	public String ordenCompraNumero;
	public int proveedorId;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	public Date fecha;
	public String estado;
	public int empleadoId;
	public List<CompraDetalle> items;
	public String proveedor;

	public Compra() {
		super();
	}

	public Compra(int id) {
		super();
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOrdenCompraNumero() {
		return ordenCompraNumero;
	}

	public void setOrdenCompraNumero(String ordenCompraNumero) {
		this.ordenCompraNumero = ordenCompraNumero;
	}

	public int getProveedorId() {
		return proveedorId;
	}

	public void setProveedorId(int proveedorId) {
		this.proveedorId = proveedorId;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public int getEmpleadoId() {
		return empleadoId;
	}

	public void setEmpleadoId(int empleadoId) {
		this.empleadoId = empleadoId;
	}

	public List<CompraDetalle> getItems() {
		return items;
	}

	public void setItems(List<CompraDetalle> items) {
		this.items = items;
	}

	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

}
