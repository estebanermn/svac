package pe.edu.idat.svac.model;

public class Articulo {

	public int id;
	public int categoriaId;
	public int unidadId;
	public String codigo;
	public String nombre;
	public String descripcion;
	public int estado;
	public int stock;
	public Double precio;
	public Double precioProveedor;
	public String categoria;
	public String unidad;
	
	public Articulo() {
		super();
	}

	public Articulo(int id) {
		super();
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(int categoriaId) {
		this.categoriaId = categoriaId;
	}

	public int getUnidadId() {
		return unidadId;
	}

	public void setUnidadId(int unidadId) {
		this.unidadId = unidadId;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}
	
	
	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}
	
	

	public Double getPrecioProveedor() {
		return precioProveedor;
	}

	public void setPrecioProveedor(Double precioProveedor) {
		this.precioProveedor = precioProveedor;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

}
