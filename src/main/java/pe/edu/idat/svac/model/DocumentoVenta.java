package pe.edu.idat.svac.model;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

public class DocumentoVenta {

	public int id;
	public String documentoVentaSerie;
	public String documentoVentaNumero;
	public int clienteId;

	// @DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	public Date fecha;

	public String estado;
	public Double subtotal;
	public Double igv;
	public Double total;
	public int empleadoId;
	public int calificacion;
	public String tipoDocumento;

	public String cliente;

	public List<DocumentoVentaDetalle> items;

	public DocumentoVenta() {
		super();
	}

	public DocumentoVenta(int id) {
		super();
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDocumentoVentaSerie() {
		return documentoVentaSerie;
	}

	public void setDocumentoVentaSerie(String documentoVentaSerie) {
		this.documentoVentaSerie = documentoVentaSerie;
	}

	public String getDocumentoVentaNumero() {
		return documentoVentaNumero;
	}

	public void setDocumentoVentaNumero(String documentoVentaNumero) {
		this.documentoVentaNumero = documentoVentaNumero;
	}

	public int getClienteId() {
		return clienteId;
	}

	public void setClienteId(int clienteId) {
		this.clienteId = clienteId;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Double getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(Double subtotal) {
		this.subtotal = subtotal;
	}

	public Double getIgv() {
		return igv;
	}

	public void setIgv(Double igv) {
		this.igv = igv;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public int getEmpleadoId() {
		return empleadoId;
	}

	public void setEmpleadoId(int empleadoId) {
		this.empleadoId = empleadoId;
	}

	public int getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(int calificacion) {
		this.calificacion = calificacion;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public List<DocumentoVentaDetalle> getItems() {
		return items;
	}

	public void setItems(List<DocumentoVentaDetalle> items) {
		this.items = items;
	}

}
