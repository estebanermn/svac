package pe.edu.idat.svac.model;

import java.sql.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Inventario {

	public int id;
	public int articuloId;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	public Date fecha;
	public String tipoMovimiento;
	public int stockIn;
	public int stockOut;
	public int ordenCompraId;
	public int documentoVentaId;
	public String descripcion;
	public Double precioUnitario;
	public String articulo;
	public String codigo;
	public int stock;

	public Inventario() {
		super();
	}

	public Inventario(int id) {
		super();
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getArticuloId() {
		return articuloId;
	}

	public void setArticuloId(int articuloId) {
		this.articuloId = articuloId;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getTipoMovimiento() {
		return tipoMovimiento;
	}

	public void setTipoMovimiento(String tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

	public int getStockIn() {
		return stockIn;
	}

	public void setStockIn(int stockIn) {
		this.stockIn = stockIn;
	}

	public int getStockOut() {
		return stockOut;
	}

	public void setStockOut(int stockOut) {
		this.stockOut = stockOut;
	}

	public int getOrdenCompraId() {
		return ordenCompraId;
	}

	public void setOrdenCompraId(int ordenCompraId) {
		this.ordenCompraId = ordenCompraId;
	}

	public int getDocumentoVentaId() {
		return documentoVentaId;
	}

	public void setDocumentoVentaId(int documentoVentaId) {
		this.documentoVentaId = documentoVentaId;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Double getPrecioUnitario() {
		return precioUnitario;
	}

	public void setPrecioUnitario(Double precioUnitario) {
		this.precioUnitario = precioUnitario;
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

}
