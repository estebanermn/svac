package pe.edu.idat.svac.model;

public class Calificacion {

	public int id;
	public int documentoVentaId;
	public int empleadoId;
	public int calificacion;

	public Calificacion() {
		super();
	}

	public Calificacion(int id) {
		super();
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDocumentoVentaId() {
		return documentoVentaId;
	}

	public void setDocumentoVentaId(int documentoVentaId) {
		this.documentoVentaId = documentoVentaId;
	}

	public int getEmpleadoId() {
		return empleadoId;
	}

	public void setEmpleadoId(int empleadoId) {
		this.empleadoId = empleadoId;
	}

	public int getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(int calificacion) {
		this.calificacion = calificacion;
	}

}
