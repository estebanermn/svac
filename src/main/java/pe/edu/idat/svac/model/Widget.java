package pe.edu.idat.svac.model;

public class Widget {

	public String key;
	public int value;

	public Widget() {
		super();
	}

	public Widget(String key) {
		super();
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
