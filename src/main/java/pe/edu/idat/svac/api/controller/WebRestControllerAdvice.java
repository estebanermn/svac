package pe.edu.idat.svac.api.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import pe.edu.idat.svac.error.ErrorResponse;
import pe.edu.idat.svac.exceptions.GeneralException;
import pe.edu.idat.svac.exceptions.MissingAttributesException;
import pe.edu.idat.svac.exceptions.ResourceNotFoundException;

@ControllerAdvice
public class WebRestControllerAdvice {
	

	// Default Exception

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {

		ErrorResponse objError = new ErrorResponse();

		objError.addError(400, ex.getLocalizedMessage());

		return new ResponseEntity<ErrorResponse>(objError, HttpStatus.BAD_REQUEST);
	}

	// General Exception

	@ExceptionHandler(GeneralException.class)
	public ResponseEntity<ErrorResponse> exceptionHandler(GeneralException ex) {

		ErrorResponse objError = new ErrorResponse();

		objError.addError(ex.getStatusCode().value(), ex.getMessage());

		return new ResponseEntity<ErrorResponse>(objError, ex.getStatusCode());
	}

	// Resource No Found

	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<ErrorResponse> resourceNotFoundExceptionHandler(ResourceNotFoundException ex) {

		ErrorResponse objError = new ErrorResponse();

		objError.addError(ex.getStatusCode().value(), ex.getMessage());

		return new ResponseEntity<ErrorResponse>(objError, ex.getStatusCode());
	}

	// Missing Attributes

	@ExceptionHandler(MissingAttributesException.class)
	public ResponseEntity<ErrorResponse> missingAttributesHandler(MissingAttributesException ex) {

		ErrorResponse objError = new ErrorResponse();

		objError.addError(ex.getStatusCode().value(), ex.getMessage());

		return new ResponseEntity<ErrorResponse>(objError, ex.getStatusCode());
	}
}