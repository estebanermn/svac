package pe.edu.idat.svac.api.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.idat.svac.error.ErrorMsg;
import pe.edu.idat.svac.error.ResultResponse;
import pe.edu.idat.svac.exceptions.MissingAttributesException;
import pe.edu.idat.svac.exceptions.ResourceNotFoundException;
import pe.edu.idat.svac.model.Inventario;
import pe.edu.idat.svac.service.InventarioService;

@RestController
@RequestMapping("/api")
class AlmacenController {

	@Autowired
	InventarioService inventarioService;

	
	
	@RequestMapping(value = "/almacen", method = RequestMethod.GET)
	public List<Inventario> list() {
		return inventarioService.listAll();
	}

	@RequestMapping(value = "/almacen", method = RequestMethod.POST)
	public ResponseEntity<?> create(@RequestBody Inventario entidad) throws MissingAttributesException {
		ResponseEntity<?> responseEntity;

		try {
			inventarioService.crearIngreso(entidad);
			
			responseEntity = new ResponseEntity<Object>(new ResultResponse(true), HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			throw new MissingAttributesException(HttpStatus.UNPROCESSABLE_ENTITY, ErrorMsg.DATA_INVALIDA);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/almacen/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getById(@PathVariable int id) throws ResourceNotFoundException {
		ResponseEntity<?> responseEntity;

		try {
			Inventario inventario = inventarioService.getById(id);
			responseEntity = new ResponseEntity<Inventario>(inventario, HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, ErrorMsg.RECURSO_NO_ENCONTRADO);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/almacen/{id}/items", method = RequestMethod.GET)
	public ResponseEntity<?> getInvetarioByArticulo(@PathVariable int id) throws ResourceNotFoundException {
		ResponseEntity<?> responseEntity;

		try {
			List<Inventario> lista = inventarioService.getInvetarioByArticulo(id);
			responseEntity = new ResponseEntity<List<Inventario>>(lista, HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, ErrorMsg.RECURSO_NO_ENCONTRADO);
		}

		return responseEntity;
	}

	

	
}
