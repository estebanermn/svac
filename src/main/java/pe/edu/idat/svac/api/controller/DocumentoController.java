package pe.edu.idat.svac.api.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.idat.svac.model.Documento;
import pe.edu.idat.svac.service.DocumentoService;

@RestController
@RequestMapping("/api")
class DocumentoController {

	@Autowired
	DocumentoService service;

	@RequestMapping(value = "/documentos", method = RequestMethod.GET)
	public List<Documento> list() {
		return service.listAll();
	}

	@RequestMapping(value = "/documentos", method = RequestMethod.POST)
	public ResponseEntity<String> create(@RequestBody Documento entidad) {
		service.create(entidad);
		return new ResponseEntity<String>(HttpStatus.CREATED);
	}

	@RequestMapping(value = "/documentos/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getById(@PathVariable int id) throws Exception {
		ResponseEntity<?> responseEntity;

		try {
			Documento  documento = service.getById(id);
			responseEntity = new ResponseEntity<Documento>(documento, HttpStatus.OK);

		} catch (Exception e) {
			responseEntity = new ResponseEntity<String>("", HttpStatus.NOT_FOUND);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/documentos/{id}", method = RequestMethod.PUT)
	public Documento update(@PathVariable int id, @RequestBody Documento entidad) throws Exception {

		if (entidad.getId() <= 0) {
			entidad.setId(id);
		}

		return service.update(entidad);
	}
	
	
	


}
