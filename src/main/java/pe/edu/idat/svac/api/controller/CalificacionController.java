package pe.edu.idat.svac.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.idat.svac.error.ErrorMsg;
import pe.edu.idat.svac.error.ResultResponse;
import pe.edu.idat.svac.exceptions.MissingAttributesException;
import pe.edu.idat.svac.exceptions.ResourceNotFoundException;
import pe.edu.idat.svac.model.Calificacion;
import pe.edu.idat.svac.model.DocumentoVenta;
import pe.edu.idat.svac.service.CalificacionService;
import pe.edu.idat.svac.service.DocumentoVentaService;
import pe.edu.idat.svac.utils.Estado;

@RestController
@RequestMapping("/api")
class CalificacionController {

	@Autowired
	CalificacionService calificacionService;

	@Autowired
	DocumentoVentaService ventaService;

	@RequestMapping(value = "/calificacion/venta/{ventaId}", method = RequestMethod.POST)
	public ResponseEntity<?> create(@PathVariable int ventaId, @RequestBody Calificacion entidad)
			throws MissingAttributesException {

		ResponseEntity<?> responseEntity;
		ResultResponse resultResponse = new ResultResponse(false);

		try {

			Calificacion calificacion = calificacionService.getByVentaId(ventaId);

			if (calificacion.getId() == 0) {

				DocumentoVenta venta = ventaService.getById(ventaId);

				if (venta.getEstado().equals(Estado.PAGADO)) {

					entidad.setDocumentoVentaId(venta.getId());
					entidad.setEmpleadoId(venta.getEmpleadoId());
					entidad.setCalificacion(entidad.getCalificacion());

					calificacionService.create(entidad);
					resultResponse.setResult(true);
				}
			}

			responseEntity = new ResponseEntity<Object>(resultResponse, HttpStatus.CREATED);

		} catch (org.springframework.dao.EmptyResultDataAccessException ex) {
			ex.printStackTrace();

			resultResponse.setResult(false);
			responseEntity = new ResponseEntity<Object>(resultResponse, HttpStatus.CREATED);

		} catch (Exception e) {
			e.printStackTrace();
			throw new MissingAttributesException(HttpStatus.UNPROCESSABLE_ENTITY, ErrorMsg.DATA_INVALIDA);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/calificacion/venta/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getById(@PathVariable int id) throws ResourceNotFoundException {
		ResponseEntity<?> responseEntity;

		try {
			Calificacion entidad = calificacionService.getByVentaId(id);
			if (entidad.getId() != 0) {
				responseEntity = new ResponseEntity<Calificacion>(entidad, HttpStatus.OK);
			} else {
				throw new Exception();
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, ErrorMsg.RECURSO_NO_ENCONTRADO);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/calificacion/venta/{ventaId}", method = RequestMethod.PUT)
	public ResponseEntity<?> update(@PathVariable int ventaId, @RequestBody Calificacion entidad)
			throws MissingAttributesException {

		ResponseEntity<?> responseEntity;

		try {

			int nuevaCalificacion = entidad.getCalificacion();

			entidad = (Calificacion) calificacionService.getByVentaId(ventaId);

			entidad.setCalificacion(nuevaCalificacion);

			entidad = (Calificacion) calificacionService.update(entidad);

			calificacionService.calcularPromedio(entidad.getEmpleadoId());

			responseEntity = new ResponseEntity<Calificacion>(entidad, HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			throw new MissingAttributesException(HttpStatus.UNPROCESSABLE_ENTITY, ErrorMsg.DATA_INVALIDA);
		}

		return responseEntity;
	}

}
