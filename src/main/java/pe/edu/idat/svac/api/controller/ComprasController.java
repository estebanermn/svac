package pe.edu.idat.svac.api.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.idat.svac.error.ErrorMsg;
import pe.edu.idat.svac.error.ResultResponse;
import pe.edu.idat.svac.exceptions.MissingAttributesException;
import pe.edu.idat.svac.exceptions.ResourceNotFoundException;
import pe.edu.idat.svac.model.Compra;
import pe.edu.idat.svac.model.CompraDetalle;
import pe.edu.idat.svac.service.CompraDetalleService;
import pe.edu.idat.svac.service.CompraService;
import pe.edu.idat.svac.utils.Estado;

@RestController
@RequestMapping("/api")
class ComprasController extends BaseController {

	@Autowired
	CompraService compraService;

	@Autowired
	CompraDetalleService detalleservice;

	@RequestMapping(value = "/compras", method = RequestMethod.GET)
	public List<Compra> list() {
		return compraService.listAll();
	}

	@RequestMapping(value = "/compras", method = RequestMethod.POST)
	public ResponseEntity<?> create(@RequestBody Compra entidad) throws MissingAttributesException {
		ResponseEntity<?> responseEntity;

		try {
			compraService.create(entidad);

			int lastId = compraService.getLastId();
			registrarCompraAuditoria(lastId);
			
			entidad = (Compra) compraService.getById(lastId);

			responseEntity = new ResponseEntity<Compra>(entidad, HttpStatus.CREATED);
		} catch (Exception e) {
			// e.printStackTrace();
			throw new MissingAttributesException(HttpStatus.UNPROCESSABLE_ENTITY, ErrorMsg.DATA_INVALIDA);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/compras/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getById(@PathVariable int id) throws ResourceNotFoundException {
		ResponseEntity<?> responseEntity;

		try {
			Compra unidad = compraService.getById(id);
			responseEntity = new ResponseEntity<Compra>(unidad, HttpStatus.OK);

		} catch (Exception e) {
			// e.printStackTrace();
			throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, ErrorMsg.RECURSO_NO_ENCONTRADO);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/compras/{id}/status/aprobado", method = RequestMethod.POST)
	public ResponseEntity<?> updateApproved(@PathVariable int id) throws MissingAttributesException {

		ResponseEntity<?> responseEntity;

		try {

			Compra entidad = new Compra(id);
			entidad = compraService.updateApproved(entidad);

			AprobarCompraAuditoria(id);

			responseEntity = new ResponseEntity<Compra>(entidad, HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			throw new MissingAttributesException(HttpStatus.UNPROCESSABLE_ENTITY, ErrorMsg.DATA_INVALIDA);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/compras/{id}/status/pagado", method = RequestMethod.POST)
	public ResponseEntity<?> updatePaid(@PathVariable int id) throws MissingAttributesException {

		ResponseEntity<?> responseEntity;

		try {

			Compra entidad = compraService.getById(id);

			if (!entidad.getEstado().equals(Estado.PAGADO)) {
				entidad = compraService.updatePaid(entidad);
				pagarCompraAuditoria(id);

			}

			responseEntity = new ResponseEntity<Compra>(entidad, HttpStatus.OK);

		} catch (Exception e) {
			// e.printStackTrace();
			throw new MissingAttributesException(HttpStatus.UNPROCESSABLE_ENTITY, ErrorMsg.DATA_INVALIDA);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/compras/{id}/status/recibido", method = RequestMethod.POST)
	public ResponseEntity<?> updateReceived(@PathVariable int id) throws MissingAttributesException {

		ResponseEntity<?> responseEntity;

		try {
			Compra entidad = compraService.getById(id);

			if (!entidad.getEstado().equals("Recibido")) {
				entidad = compraService.updateReceived(entidad);
				ejecutarTareas(entidad.getId());
			}

			responseEntity = new ResponseEntity<Compra>(entidad, HttpStatus.OK);

		} catch (Exception e) {
			// e.printStackTrace();
			throw new MissingAttributesException(HttpStatus.UNPROCESSABLE_ENTITY, ErrorMsg.DATA_INVALIDA);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/compras/{id}/status/anulado", method = RequestMethod.POST)
	public ResponseEntity<?> updateCanceled(@PathVariable int id) throws MissingAttributesException {

		ResponseEntity<?> responseEntity;

		try {
			Compra entidad = compraService.getById(id);

			if (!entidad.getEstado().equals(Estado.ANULADO)) {
				entidad = compraService.updateCanceled(entidad);
				ejecutarAnularTareas(entidad.getId());
			}

			entidad = (Compra) compraService.updateCanceled(entidad);

			responseEntity = new ResponseEntity<Compra>(entidad, HttpStatus.OK);

		} catch (Exception e) {
			// e.printStackTrace();
			throw new MissingAttributesException(HttpStatus.UNPROCESSABLE_ENTITY, ErrorMsg.DATA_INVALIDA);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/compras/{id}/items", method = RequestMethod.GET)
	public ResponseEntity<?> getItemsById(@PathVariable int id) throws ResourceNotFoundException {
		ResponseEntity<?> responseEntity;

		try {
			List<CompraDetalle> lista = detalleservice.getItemsById(id);
			responseEntity = new ResponseEntity<List<CompraDetalle>>(lista, HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, ErrorMsg.RECURSO_NO_ENCONTRADO);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/compras/{id}/items", method = RequestMethod.POST)
	public ResponseEntity<?> createItem(@PathVariable int id, @RequestBody CompraDetalle entidad)
			throws MissingAttributesException {
		ResponseEntity<?> responseEntity;

		try {
			entidad.setCompraId(id);
			detalleservice.create(entidad);
			responseEntity = new ResponseEntity<Object>(new ResultResponse(true), HttpStatus.CREATED);

		} catch (Exception e) {
			// e.printStackTrace();
			throw new MissingAttributesException(HttpStatus.NOT_FOUND, ErrorMsg.DATA_INVALIDA);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/compras/historial/{value}", method = RequestMethod.GET)
	public ResponseEntity<?> getVentasByHistorialValue(@PathVariable String value) {
		ResponseEntity<?> responseEntity = null;

		try {
			List<Compra> lista = compraService.getVentasByHistoria(value);
			responseEntity = new ResponseEntity<List<Compra>>(lista, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseEntity;
	}

	private void ejecutarTareas(int compraId) {

		try {
			// Crear Auditoria
			recibirCompraAuditoria(compraId);

			// UPDATE OrdenCompra SET Status = 'Recibido' WHERE id = {id}
			compraService.updateReceived(new Compra(compraId));

			// Registra ingresos de inventario
			compraService.ingresarStock(compraId);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	private void ejecutarAnularTareas(int compraId) {

		try {
			// Crear Auditoria
			anularCompraAuditoria(compraId);

			// UPDATE OrdenCompra SET Status = 'Recibido' WHERE id = {id}
			compraService.updateCanceled(new Compra(compraId));

			// Registra ingresos de inventario
			compraService.retornarStock(compraId);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}
