package pe.edu.idat.svac.api.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.idat.svac.error.ErrorMsg;
import pe.edu.idat.svac.error.ResultResponse;
import pe.edu.idat.svac.exceptions.MissingAttributesException;
import pe.edu.idat.svac.exceptions.ResourceNotFoundException;
import pe.edu.idat.svac.model.Unidad;
import pe.edu.idat.svac.service.UnidadService;

@RestController
@RequestMapping("/api")
class UnidadController {

	@Autowired
	UnidadService unidadService;

	@RequestMapping(value = "/unidades", method = RequestMethod.GET)
	public List<Unidad> list() {
		return unidadService.listAll();
	}

	@RequestMapping(value = "/unidades", method = RequestMethod.POST)
	public ResponseEntity<?> create(@RequestBody Unidad entidad) throws MissingAttributesException {
		ResponseEntity<?> responseEntity;
		try {
			unidadService.create(entidad);
			ResultResponse resultResponse = new ResultResponse(true);
			responseEntity = new ResponseEntity<Object>(resultResponse, HttpStatus.CREATED);
		} catch (Exception e) {
			// e.printStackTrace();
			throw new MissingAttributesException(HttpStatus.UNPROCESSABLE_ENTITY, ErrorMsg.DATA_INVALIDA);
		}
		return responseEntity;
	}

	@RequestMapping(value = "/unidades/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getById(@PathVariable int id)  throws ResourceNotFoundException {
		ResponseEntity<?> responseEntity;

		try {
			Unidad unidad = unidadService.getById(id);
			responseEntity = new ResponseEntity<Unidad>(unidad, HttpStatus.OK);

		} catch (Exception e) {
			throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, ErrorMsg.RECURSO_NO_ENCONTRADO);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/unidades/{id}", method = RequestMethod.PUT)
	public Unidad update(@PathVariable int id, @RequestBody Unidad entidad) throws Exception {

		if (entidad.getId() <= 0) {
			entidad.setId(id);
		}

		return unidadService.update(entidad);
	}

	@RequestMapping(value = "/unidades/{id}/estado", method = RequestMethod.PUT)
	public Unidad estado(@PathVariable int id, @RequestBody Unidad entidad) throws Exception {
		
		entidad.setId(id);
		return unidadService.updateEstado(entidad);
	}

}
