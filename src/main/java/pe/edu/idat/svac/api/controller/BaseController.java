package pe.edu.idat.svac.api.controller;

import org.springframework.beans.factory.annotation.Autowired;

import pe.edu.idat.svac.model.Auditoria;
import pe.edu.idat.svac.service.AuditoriaService;
import pe.edu.idat.svac.utils.AuditoriaRegistro;
import pe.edu.idat.svac.utils.Estado;

class BaseController {

	@Autowired
	AuditoriaService auditoriaService;

	public int getEmpleadoId() {
		return 1; // Raw
	}

	// Helper: Auditoria

	public void registrarVentaAuditoria(int ventaId) {
		Auditoria auditoria = AuditoriaRegistro.crearRegistro(getEmpleadoId(), Estado.PENDIENTE,
				AuditoriaRegistro.REGISTRO_VENTA_CREADA);
		auditoria.setDocumentoVentaId(ventaId);
		auditoriaService.create(auditoria);
	}

	public void anularVentaAuditoria(int ventaId) {
		Auditoria auditoria = AuditoriaRegistro.crearRegistro(getEmpleadoId(), Estado.ANULADO,
				AuditoriaRegistro.REGISTRO_VENTA_ANULADO);
		auditoria.setDocumentoVentaId(ventaId);
		auditoriaService.create(auditoria);
	}

	public void pagarVentaAuditoria(int ventaId) {
		Auditoria auditoria = AuditoriaRegistro.crearRegistro(getEmpleadoId(), Estado.PAGADO,
				AuditoriaRegistro.REGISTRO_VENTA_PAGADA);
		auditoria.setDocumentoVentaId(ventaId);
		auditoriaService.create(auditoria);
	}

	public void registrarPagoAuditoria(int pagoId) {
		Auditoria auditoria = AuditoriaRegistro.crearRegistro(getEmpleadoId(), Estado.PENDIENTE,
				AuditoriaRegistro.REGISTRO_VENTA_CREADA);
		auditoria.setDocumentoVentaId(pagoId);
		auditoriaService.create(auditoria);
	}

	// Helper: Compra

	public void registrarCompraAuditoria(int compraId) {
		Auditoria auditoria = AuditoriaRegistro.crearRegistro(getEmpleadoId(), Estado.PENDIENTE,
				AuditoriaRegistro.REGISTRO_COMPRA_CREADA);
		auditoria.setOrdenCompraId(compraId);
		auditoriaService.create(auditoria);
	}

	public void AprobarCompraAuditoria(int compraId) {
		Auditoria auditoria = AuditoriaRegistro.crearRegistro(getEmpleadoId(), Estado.APROBADO,
				AuditoriaRegistro.REGISTRO_COMPRA_APROBADA);
		auditoria.setOrdenCompraId(compraId);
		auditoriaService.create(auditoria);
	}

	public void pagarCompraAuditoria(int compraId) {
		Auditoria auditoria = AuditoriaRegistro.crearRegistro(getEmpleadoId(), Estado.PAGADO,
				AuditoriaRegistro.REGISTRO_COMPRA_PAGADA);
		auditoria.setOrdenCompraId(compraId);
		auditoriaService.create(auditoria);
	}

	public void recibirCompraAuditoria(int compraId) {
		Auditoria auditoria = AuditoriaRegistro.crearRegistro(getEmpleadoId(), Estado.RECIBIDO,
				AuditoriaRegistro.REGISTRO_COMPRA_RECIBIDA);
		auditoria.setOrdenCompraId(compraId);
		auditoriaService.create(auditoria);
	}

	public void anularCompraAuditoria(int compraId) {
		Auditoria auditoria = AuditoriaRegistro.crearRegistro(getEmpleadoId(), Estado.ANULADO,
				AuditoriaRegistro.REGISTRO_COMPRA_ANULADO);
		auditoria.setOrdenCompraId(compraId);
		auditoriaService.create(auditoria);
	}

}
