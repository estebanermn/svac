package pe.edu.idat.svac.api.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.idat.svac.error.ErrorMsg;
import pe.edu.idat.svac.error.ResultResponse;
import pe.edu.idat.svac.exceptions.MissingAttributesException;
import pe.edu.idat.svac.exceptions.ResourceNotFoundException;

import pe.edu.idat.svac.model.Categoria;
import pe.edu.idat.svac.service.CategoriaService;

@RestController
@RequestMapping("/api")
class CategoriaController {

	@Autowired
	CategoriaService categoriaService;

	@RequestMapping(value = "/categorias", method = RequestMethod.GET)
	public List<Categoria> list() {
		return categoriaService.listAll();
	}

	@RequestMapping(value = "/categorias", method = RequestMethod.POST)
	public ResponseEntity<?> create(@RequestBody Categoria entidad) throws MissingAttributesException {
		ResponseEntity<?> responseEntity;

		try {
			categoriaService.create(entidad);
			responseEntity = new ResponseEntity<Object>(new ResultResponse(true), HttpStatus.CREATED);
		} catch (Exception e) {
			// e.printStackTrace();
			throw new MissingAttributesException(HttpStatus.UNPROCESSABLE_ENTITY, ErrorMsg.DATA_INVALIDA);
		}

		return responseEntity;

	}

	@RequestMapping(value = "/categorias/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getById(@PathVariable int id) throws ResourceNotFoundException {
		ResponseEntity<?> responseEntity;

		try {
			Categoria categoria = categoriaService.getById(id);
			responseEntity = new ResponseEntity<Categoria>(categoria, HttpStatus.OK);

		} catch (Exception e) {

			// e.printStackTrace();
			throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, ErrorMsg.RECURSO_NO_ENCONTRADO);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/categorias/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> update(@PathVariable int id, @RequestBody Categoria entidad)
			throws MissingAttributesException {

		ResponseEntity<?> responseEntity;

		try {

			if (entidad.getId() <= 0) {
				entidad.setId(id);
			}

			entidad = (Categoria) categoriaService.update(entidad);

			responseEntity = new ResponseEntity<Categoria>(entidad, HttpStatus.OK);

		} catch (Exception e) {
			// e.printStackTrace();
			throw new MissingAttributesException(HttpStatus.UNPROCESSABLE_ENTITY, ErrorMsg.DATA_INVALIDA);
		}

		return responseEntity;

	}

	@RequestMapping(value = "/categorias/{id}/estado", method = RequestMethod.PUT)
	public Categoria estado(@PathVariable int id, @RequestBody Categoria entidad) throws Exception {
		
		entidad.setId(id);
		return categoriaService.updateEstado(entidad);
	}

}
