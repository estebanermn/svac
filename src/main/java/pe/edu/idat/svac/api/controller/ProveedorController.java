package pe.edu.idat.svac.api.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.idat.svac.error.ErrorMsg;
import pe.edu.idat.svac.error.ResultResponse;
import pe.edu.idat.svac.exceptions.MissingAttributesException;
import pe.edu.idat.svac.exceptions.ResourceNotFoundException;
import pe.edu.idat.svac.model.Proveedor;
import pe.edu.idat.svac.service.ProveedorService;

@RestController
@RequestMapping("/api")
class ProveedorController {

	@Autowired
	ProveedorService proveedorService;

	@RequestMapping(value = "/proveedores", method = RequestMethod.GET)
	public List<Proveedor> list() {
		return proveedorService.listAll();
	}

	@RequestMapping(value = "/proveedores", method = RequestMethod.POST)
	public ResponseEntity<?> create(@RequestBody Proveedor entidad) throws MissingAttributesException {
		ResponseEntity<?> responseEntity;

		try {
			proveedorService.create(entidad);
			ResultResponse resultResponse = new ResultResponse(true);
			responseEntity = new ResponseEntity<Object>(resultResponse, HttpStatus.CREATED);
		} catch (Exception e) {
			// e.printStackTrace();
			throw new MissingAttributesException(HttpStatus.UNPROCESSABLE_ENTITY, ErrorMsg.DATA_INVALIDA);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/proveedores/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getById(@PathVariable int id) throws ResourceNotFoundException {
		ResponseEntity<?> responseEntity;

		try {
			Proveedor proveedor = proveedorService.getById(id);
			responseEntity = new ResponseEntity<Proveedor>(proveedor, HttpStatus.OK);

		} catch (Exception e) {
			throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, ErrorMsg.RECURSO_NO_ENCONTRADO);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/proveedores/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> update(@PathVariable int id, @RequestBody Proveedor entidad) throws Exception {

		ResponseEntity<?> responseEntity;

		try {

			if (entidad.getId() <= 0) {
				entidad.setId(id);
			}

			entidad = (Proveedor) proveedorService.update(entidad);

			responseEntity = new ResponseEntity<Proveedor>(entidad, HttpStatus.OK);

		} catch (Exception e) {
			// e.printStackTrace();
			throw new MissingAttributesException(HttpStatus.UNPROCESSABLE_ENTITY, ErrorMsg.DATA_INVALIDA);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/proveedores/{id}/estado", method = RequestMethod.PUT)
	public Proveedor estado(@PathVariable int id, @RequestBody Proveedor entidad) throws Exception {

		entidad.setId(id);
		return proveedorService.updateEstado(entidad);
	}

}
