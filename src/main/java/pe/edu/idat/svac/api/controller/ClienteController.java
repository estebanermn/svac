package pe.edu.idat.svac.api.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.idat.svac.error.ErrorMsg;
import pe.edu.idat.svac.error.ResultResponse;
import pe.edu.idat.svac.exceptions.MissingAttributesException;
import pe.edu.idat.svac.exceptions.ResourceNotFoundException;
import pe.edu.idat.svac.model.Cliente;
import pe.edu.idat.svac.service.ClienteService;

@RestController
@RequestMapping("/api")
class ClienteController {

	@Autowired
	ClienteService clienteService;

	@RequestMapping(value = "/clientes", method = RequestMethod.GET)
	public List<Cliente> list() {
		return clienteService.listAll();
	}

	@RequestMapping(value = "/clientes", method = RequestMethod.POST)
	public ResponseEntity<?> create(@RequestBody Cliente entidad) throws MissingAttributesException {
		ResponseEntity<?> responseEntity;

		try {
			clienteService.create(entidad);
			// responseEntity = new ResponseEntity<String>("", HttpStatus.CREATED);
			ResultResponse resultResponse = new ResultResponse(true);
			responseEntity = new ResponseEntity<Object>(resultResponse, HttpStatus.CREATED);
		} catch (Exception e) {
			// e.printStackTrace();
			throw new MissingAttributesException(HttpStatus.UNPROCESSABLE_ENTITY, ErrorMsg.DATA_INVALIDA);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/clientes/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getById(@PathVariable int id) throws ResourceNotFoundException {
		ResponseEntity<?> responseEntity;

		try {
			Cliente cliente = clienteService.getById(id);
			responseEntity = new ResponseEntity<Cliente>(cliente, HttpStatus.OK);

		} catch (Exception e) {
			throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, ErrorMsg.RECURSO_NO_ENCONTRADO);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/clientes/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> update(@PathVariable int id, @RequestBody Cliente entidad)
			throws MissingAttributesException {

		ResponseEntity<?> responseEntity;

		try {

			if (entidad.getId() <= 0) {
				entidad.setId(id);
			}

			entidad = (Cliente) clienteService.update(entidad);

			responseEntity = new ResponseEntity<Cliente>(entidad, HttpStatus.OK);

		} catch (Exception e) {
			// e.printStackTrace();
			throw new MissingAttributesException(HttpStatus.UNPROCESSABLE_ENTITY, ErrorMsg.DATA_INVALIDA);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/clientes/{id}/estado", method = RequestMethod.PUT)
	public Cliente estado(@PathVariable int id, @RequestBody Cliente entidad) throws Exception {

		entidad.setId(id);
		return clienteService.updateEstado(entidad);
	}

}
