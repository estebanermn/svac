package pe.edu.idat.svac.api.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.idat.svac.error.ErrorMsg;
import pe.edu.idat.svac.error.ResultResponse;
import pe.edu.idat.svac.exceptions.MissingAttributesException;
import pe.edu.idat.svac.exceptions.ResourceNotFoundException;
import pe.edu.idat.svac.model.Articulo;
import pe.edu.idat.svac.service.ArticuloService;

@RestController
@RequestMapping("/api")
class ArticuloController {

	@Autowired
	ArticuloService service;

	@RequestMapping(value = "/articulos", method = RequestMethod.GET)
	public List<Articulo> list() {
		return service.listAll();
	}

	@RequestMapping(value = "/articulos", method = RequestMethod.POST)
	public ResponseEntity<?> create(@RequestBody Articulo entidad) throws MissingAttributesException {

		ResponseEntity<?> responseEntity;
		try {
			service.create(entidad);
			ResultResponse resultResponse = new ResultResponse(true);
			responseEntity = new ResponseEntity<Object>(resultResponse, HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			throw new MissingAttributesException(HttpStatus.UNPROCESSABLE_ENTITY, ErrorMsg.DATA_INVALIDA);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/articulos/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getById(@PathVariable int id) throws ResourceNotFoundException {
		ResponseEntity<?> responseEntity;

		try {
			Articulo articulo = service.getById(id);
			responseEntity = new ResponseEntity<Articulo>(articulo, HttpStatus.OK);

		} catch (Exception e) {
			// e.printStackTrace();
			throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, ErrorMsg.RECURSO_NO_ENCONTRADO);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/articulos/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> update(@PathVariable int id, @RequestBody Articulo entidad) throws Exception {
		ResponseEntity<?> responseEntity;

		try {
			if (entidad.getId() <= 0) {
				entidad.setId(id);
			}

			Articulo articulo = service.update(entidad);
			responseEntity = new ResponseEntity<Articulo>(articulo, HttpStatus.OK);

		} catch (Exception e) {
			// e.printStackTrace();
			throw new ResourceNotFoundException(HttpStatus.BAD_REQUEST, ErrorMsg.DATA_INVALIDA);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/articulos/{id}/estado", method = RequestMethod.PUT)
	public Articulo estado(@PathVariable int id, @RequestBody Articulo entidad) throws Exception {
		
		entidad.setId(id);
		return service.updateEstado(entidad);
	}

}
