package pe.edu.idat.svac.api.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.idat.svac.error.ErrorMsg;
import pe.edu.idat.svac.error.ResultResponse;
import pe.edu.idat.svac.exceptions.MissingAttributesException;
import pe.edu.idat.svac.exceptions.ResourceNotFoundException;
import pe.edu.idat.svac.model.Pago;
import pe.edu.idat.svac.service.PagoService;

@RestController
@RequestMapping("/api")
class PagosController extends BaseController {

	@Autowired
	PagoService pagoService;

	@RequestMapping(value = "/pagos", method = RequestMethod.GET)
	public List<Pago> list() {
		return pagoService.listAll();
	}

	@RequestMapping(value = "/pagos", method = RequestMethod.POST)
	public ResponseEntity<?> create(@RequestBody Pago entidad) throws MissingAttributesException {
		ResponseEntity<?> responseEntity;

		try {
			
			pagoService.create(entidad);
			
			int ventaId = entidad.getDocumentoVentaId();
			ejecutarTareas(ventaId);

			responseEntity = new ResponseEntity<Object>(new ResultResponse(true), HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			throw new MissingAttributesException(HttpStatus.UNPROCESSABLE_ENTITY, ErrorMsg.DATA_INVALIDA);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/pagos/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getById(@PathVariable int id) throws ResourceNotFoundException {
		ResponseEntity<?> responseEntity;

		try {
			Pago pago = pagoService.getById(id);
			responseEntity = new ResponseEntity<Pago>(pago, HttpStatus.OK);

		} catch (Exception e) {
			throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, ErrorMsg.RECURSO_NO_ENCONTRADO);
		}

		return responseEntity;
	}

	private void ejecutarTareas(int documentoVentaId) {

		try {
			pagarVentaAuditoria(documentoVentaId); // Crear Auditoria
			pagoService.runTasks(documentoVentaId);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}
