package pe.edu.idat.svac.api.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.idat.svac.error.ErrorMsg;
import pe.edu.idat.svac.error.ResultResponse;
import pe.edu.idat.svac.exceptions.MissingAttributesException;
import pe.edu.idat.svac.exceptions.ResourceNotFoundException;
import pe.edu.idat.svac.model.DocumentoVenta;
import pe.edu.idat.svac.model.DocumentoVentaDetalle;
import pe.edu.idat.svac.service.DocumentoVentaDetalleService;
import pe.edu.idat.svac.service.DocumentoVentaService;
import pe.edu.idat.svac.service.PagoService;
import pe.edu.idat.svac.utils.Estado;

@RestController
@RequestMapping("/api")
class VentasController extends BaseController {

	@Autowired
	DocumentoVentaService documentoService;

	@Autowired
	DocumentoVentaDetalleService documentoDetalleService;

	@Autowired
	PagoService pagoService;

	@RequestMapping(value = "/ventas", method = RequestMethod.GET)
	public List<DocumentoVenta> list() {
		return documentoService.listAll();
	}

	@RequestMapping(value = "/ventas", method = RequestMethod.POST)
	public ResponseEntity<?> create(@RequestBody DocumentoVenta entidad) throws MissingAttributesException {
		ResponseEntity<?> responseEntity;

		try {
			documentoService.create(entidad);

			int lastId = documentoService.getLastId();
			registrarVentaAuditoria(lastId);

			// Agregar detalles
			if (entidad.getItems() != null) {
				for (DocumentoVentaDetalle detalle : entidad.getItems()) {
					detalle.setDocumentoVentaId(lastId);
					documentoDetalleService.create(detalle);
				}
			}

			entidad = documentoService.getById(lastId);

			responseEntity = new ResponseEntity<DocumentoVenta>(entidad, HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			throw new MissingAttributesException(HttpStatus.UNPROCESSABLE_ENTITY, ErrorMsg.DATA_INVALIDA);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/ventas/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getById(@PathVariable int id) throws ResourceNotFoundException {
		ResponseEntity<?> responseEntity;

		try {
			DocumentoVenta venta = documentoService.getById(id);
			List<DocumentoVentaDetalle> items = documentoDetalleService.getItemsById(id);
			venta.setItems(items);
			responseEntity = new ResponseEntity<DocumentoVenta>(venta, HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, ErrorMsg.RECURSO_NO_ENCONTRADO);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/ventas/{id}/status/anulado", method = RequestMethod.POST)
	public ResponseEntity<?> updateCanceled(@PathVariable int id) throws MissingAttributesException {

		ResponseEntity<?> responseEntity = null;

		try {

			DocumentoVenta entidad = documentoService.getById(id);

			// Pendiente - Actualiza el estado
			// Pagado - Actualiza el estado e inventario
			// Anulado - No hace nada

			if (entidad.getEstado().equals(Estado.PENDIENTE)) {
				entidad = documentoService.updateCanceled(entidad);
				responseEntity = new ResponseEntity<DocumentoVenta>(entidad, HttpStatus.OK);
			}

			if (entidad.getEstado().equals(Estado.PAGADO)) {
				entidad = documentoService.updateCanceled(entidad);
				ejecutarTareas(entidad.getId());
				responseEntity = new ResponseEntity<DocumentoVenta>(entidad, HttpStatus.OK);
			}

			if (entidad.getEstado().equals(Estado.ANULADO)) {
				responseEntity = new ResponseEntity<Object>(new ResultResponse(false), HttpStatus.OK);
			}

		} catch (Exception e) {
			// e.printStackTrace();
			throw new MissingAttributesException(HttpStatus.UNPROCESSABLE_ENTITY, ErrorMsg.DATA_INVALIDA);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/ventas/{id}/items", method = RequestMethod.POST)
	public ResponseEntity<?> createItem(@PathVariable int id, @RequestBody DocumentoVentaDetalle entidad)
			throws MissingAttributesException {
		ResponseEntity<?> responseEntity;

		try {
			entidad.setDocumentoVentaId(id);
			documentoDetalleService.create(entidad);
			responseEntity = new ResponseEntity<Object>(new ResultResponse(true), HttpStatus.CREATED);

		} catch (Exception e) {
			// e.printStackTrace();
			throw new MissingAttributesException(HttpStatus.NOT_FOUND, ErrorMsg.DATA_INVALIDA);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/ventas/{id}/items", method = RequestMethod.GET)
	public ResponseEntity<?> getItemsById(@PathVariable int id) throws ResourceNotFoundException {
		ResponseEntity<?> responseEntity;

		try {
			List<DocumentoVentaDetalle> lista = documentoDetalleService.getItemsById(id);
			responseEntity = new ResponseEntity<List<DocumentoVentaDetalle>>(lista, HttpStatus.OK);

		} catch (Exception e) {
			// e.printStackTrace();
			throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, ErrorMsg.RECURSO_NO_ENCONTRADO);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/ventas/empleado/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getVentasByEmpleadoId(@PathVariable int id) throws ResourceNotFoundException {
		ResponseEntity<?> responseEntity;

		try {
			List<DocumentoVenta> lista = documentoService.getVentasByEmpleadoId(id);
			responseEntity = new ResponseEntity<List<DocumentoVenta>>(lista, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, ErrorMsg.RECURSO_NO_ENCONTRADO);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/ventas/historial/{value}", method = RequestMethod.GET)
	public ResponseEntity<?> getVentasByHistorialValue(@PathVariable String value) {
		ResponseEntity<?> responseEntity = null;

		try {

			List<DocumentoVenta> lista = documentoService.getVentasByHistoria(value);
			responseEntity = new ResponseEntity<List<DocumentoVenta>>(lista, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseEntity;
	}

	/**
	 * Tarea: Al anular una Venta
	 * 
	 * @param ventaId
	 */
	private void ejecutarTareas(int ventaId) {

		try {
			// Crear Auditoria
			anularVentaAuditoria(ventaId);

			// UPDATE DocumentoVenta SET Status = 'Anulado' WHERE id = {id}
			documentoService.updateCanceled(new DocumentoVenta(ventaId));

			// UPDATE Pagos SET Status = 'Anulado' WHERE id = {id}
			pagoService.updateCanceled(ventaId);

			// Crear registros Inventario para retornar stock
			documentoService.retornarStock(ventaId);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}
