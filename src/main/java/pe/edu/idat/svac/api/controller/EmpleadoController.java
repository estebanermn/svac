package pe.edu.idat.svac.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.idat.svac.error.ErrorMsg;
import pe.edu.idat.svac.error.ResultResponse;
import pe.edu.idat.svac.exceptions.MissingAttributesException;
import pe.edu.idat.svac.model.EmpleadoCumple;
import pe.edu.idat.svac.model.Empleado;
import pe.edu.idat.svac.service.EmpleadoService;

@RestController
@RequestMapping("/api")
public class EmpleadoController {

	@Autowired
	EmpleadoService empleadoService;

	@RequestMapping(value = "/empleados", method = RequestMethod.GET)
	public List<Empleado> list() {
		return empleadoService.listAll();
	}

	@RequestMapping(value = "/empleados", method = RequestMethod.POST)
	public ResponseEntity<?> create(@RequestBody Empleado entidad) throws MissingAttributesException {
		ResponseEntity<?> responseEntity;

		try {
			empleadoService.create(entidad);
			ResultResponse resultResponse = new ResultResponse(true);
			responseEntity = new ResponseEntity<Object>(resultResponse, HttpStatus.CREATED);
		} catch (Exception e) {
			// e.printStackTrace();
			throw new MissingAttributesException(HttpStatus.UNPROCESSABLE_ENTITY, ErrorMsg.DATA_INVALIDA);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/empleados/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getById(@PathVariable int id) throws Exception {
		ResponseEntity<?> responseEntity;

		try {
			Empleado empleado = empleadoService.getById(id);
			responseEntity = new ResponseEntity<Empleado>(empleado, HttpStatus.OK);

		} catch (Exception e) {
			responseEntity = new ResponseEntity<String>("", HttpStatus.NOT_FOUND);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/empleados/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> update(@PathVariable int id, @RequestBody Empleado entidad)
			throws MissingAttributesException {

		ResponseEntity<?> responseEntity;

		try {

			if (entidad.getIdEmpleado() <= 0) {
				entidad.setIdEmpleado(id);
			}

			entidad = (Empleado) empleadoService.update(entidad);

			responseEntity = new ResponseEntity<Empleado>(entidad, HttpStatus.OK);

		} catch (Exception e) {
			// e.printStackTrace();
			throw new MissingAttributesException(HttpStatus.UNPROCESSABLE_ENTITY, ErrorMsg.DATA_INVALIDA);
		}

		return responseEntity;
	}

	@RequestMapping(value = "/empleados/{id}/estado", method = RequestMethod.PUT)
	public Empleado estado(@PathVariable int id, @RequestBody Empleado entidad) throws Exception {

		entidad.setIdEmpleado(1);

		return empleadoService.updateEstado(entidad);
	}

	@RequestMapping(value = "/empleados/cumples", method = RequestMethod.GET)
	public List<EmpleadoCumple> cumples() {
		return empleadoService.listAllCumple();
	}
	
	@RequestMapping(value = "/empleados/ventas", method = RequestMethod.GET)
	public List<Empleado> listAllVenta() {
		return empleadoService.listAllVenta();
	}

	
	
	

}
