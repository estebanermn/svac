<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ tag description="Layout de las paginas" pageEncoding="UTF-8"%>
<%@ attribute name="modal" fragment="true"%>
<%@ attribute name="footer" fragment="true"%>
<!DOCTYPE html>
<html>

<head>
<!-- Required meta tags-->
<meta charset="utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="au theme template">
<meta name="keywords" content="au theme template">

<!-- Title Page-->
<title>SVAC - Dashboard</title>

<link rel="dns-prefetch" href="<c:url value="/"/>">

<link rel="stylesheet" media="all"
	href="<c:url value="/resources/css/font-face.css"/>">
<%-- <link rel="stylesheet" media="all" href="<c:url value="/resources/vendor/font-awesome-4.7/css/font-awesome.min.css"/>">--%>
<link rel="stylesheet" media="all"
	href="<c:url value="/resources/vendor/font-awesome-5/css/fontawesome-all.min.css"/>">
<link rel="stylesheet" media="all"
	href="<c:url value="/resources/vendor/mdi-font/css/material-design-iconic-font.min.css"/>">

<!-- Bootstrap CSS-->
<link rel="stylesheet" media="all"
	href="<c:url value="/resources/vendor/bootstrap-4.1.1/bootstrap.min.css"/>">

<!-- Vendor CSS-->
<link rel="stylesheet" media="all"
	href="<c:url value="/resources/vendor/select2/select2.min.css"/>">
<link rel="stylesheet" media="all"
	href="<c:url value="/resources/vendor/animsition/animsition.min.css"/>">
<link rel="stylesheet" media="all"
	href="<c:url value="/resources/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css"/>">

<%--
<link rel="stylesheet" media="all" href="<c:url value="/resources/vendor/wow/animate.css"/>">
<link rel="stylesheet" media="all" href="<c:url value="/resources/vendor/css-hamburgers/hamburgers.min.css"/>">
<link rel="stylesheet" media="all" href="<c:url value="/resources/vendor/slick/slick.css"/>">
<link rel="stylesheet" media="all" href="<c:url value="/resources/vendor/perfect-scrollbar/perfect-scrollbar.css"/>">
--%>

<!-- Main CSS-->
<link rel="stylesheet" media="all"
	href="<c:url value="/resources/css/theme.css"/>">
<link rel="stylesheet" media="all"
	href="<c:url value="/resources/css/svac.css"/>">

<!--  DataTable -->
<link rel="stylesheet" media="all"
	href="<c:url value="/resources/vendor/datatable/dataTables.bootstrap4.min.css"/>">
<%--
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
--%>

</head>


<%-- Dashboard --%>

<spring:url value="/web" var="webURL" />

<%-- Mantenimientos --%>

<spring:url value="/trabajadores" var="trabajadorURL" />
<spring:url value="/clientes" var="clientesURL" />


<%-- Almacen --%>

<spring:url value="/web/almacen/articulos" var="articulosURL" />
<spring:url value="/web/almacen/categorias" var="categoriasURL" />
<spring:url value="/web/almacen/unidades" var="unidadesURL" />
<spring:url value="/web/almacen/inventario" var="inventarioURL" />
<spring:url value="/web/almacen/inventario/ingreso" var="inventarioIngresoURL" />
<spring:url value="/web/almacen/inventario/salida" var="inventarioSalidaURL" />

<%-- Compras --%>

<spring:url value="/web/compras/ingresos" var="ingresosURL" />
<spring:url value="/web/compras/proveedores" var="proveedoresURL" />
<spring:url value="/web/compras/historial" var="comprasHistorialURL" />

<%-- Ventas --%>

<spring:url value="/web/ventas/clientes" var="clientesURL" />
<spring:url value="/web/ventas/pedidos" var="ventasURL" />
<spring:url value="/web/ventas/historial" var="ventasHistorialURL" />


<body class="animsition">
	<div class="page-wrapper">
		<!-- HEADER MOBILE-->

		<!-- MENU SIDEBAR-->
		<aside class="menu-sidebar d-none d-lg-block">
			<div class="logo">
				<a href="${webURL}"> <img
					src="<c:url value="/resources/images/icon/logo.png"/>"
					alt="SVAC Admin" />
				</a>
			</div>
			<div class="menu-sidebar__content js-scrollbar1">
				<nav class="navbar-sidebar">
					<ul class="list-unstyled navbar__list">

						<!-- Dashboard -->
						<li class="active has-sub"><a class="js-arrow"
							href="${webURL}"> <i class="fas fa-tachometer-alt"></i>Dashboard
						</a></li>

						<!-- Mantenimientos -->

						<li class="has-sub"><a class="js-arrow" href="#"> <i
								class="fas fa-desktop"></i>Mantenimientos <span class="arrow">
									<i class="fas fa-angle-down"></i>
							</span>
						</a>
							<ul class="list-unstyled navbar__sub-list js-sub-list">
								<li><a href=""> <i class="fa fa-user"></i>Usuarios
								</a></li>

								<li><a href="${trabajadorURL}"> <i
										class="fas fa-toggle-on"></i>Trabajadores
								</a></li>

							</ul></li>

						<!-- Almacén-->
						<li><a class="js-arrow" href="#"> <i class="fa fa-edit"></i>Almacén
								<span class="arrow"> <i class="fas fa-angle-down"></i>
							</span>
						</a>
							<ul class="list-unstyled navbar__sub-list js-sub-list">
								<li><a href="${articulosURL}"> <i
										class="far fa-dot-circle"></i>Artículos
								</a></li>
								<li><a href="${categoriasURL}"> <i
										class="far fa-dot-circle"></i>Categoría
								</a></li>
								<li><a href="${unidadesURL}"> <i
										class="far fa-dot-circle"></i>Unidad de Medida
								</a></li>
								
								<li><a href="${inventarioURL}"> <i class="fas fa-box"></i>Inventario (TBD)
								</a></li>
								
								<li><a href="${inventarioIngresoURL}"> <i class="fas fa-box"></i>Inventario - Ingreso
								</a></li>
								
								<li><a href="${inventarioSalidaURL}"> <i class="fas fa-box"></i>Inventario - Salida
								</a></li>

							</ul></li>

						<!-- Compras -->

						<li><a class="js-arrow" href="#"> <i class="fa fa-table"></i>Compras

								<span class="arrow"> <i class="fas fa-angle-down"></i>
							</span>
						</a>
							<ul class="list-unstyled navbar__sub-list js-sub-list">
								<li><a href="${ingresosURL}"> <i
										class="far fa-dot-circle"></i>Ingreso
								</a></li>

								<li><a href="${proveedoresURL}"> <i class="fa fa-user"></i>Proveedores
								</a></li>

								<li><a href="${comprasHistorialURL}"> <i
										class="fas fa-history"></i>Historial
								</a></li>

							</ul></li>
						<!-- end almacen -->

						<!-- Ventas-->
						<li class="has-sub"><a class="js-arrow" href="#"> <i
								class="fa fa-shopping-cart"></i>Ventas <span class="arrow">
									<i class="fas fa-angle-down"></i>
							</span>
						</a>
							<ul class="list-unstyled navbar__sub-list js-sub-list">
								<li><a href="${ventasURL}"><i
										class="far fa-address-book"></i>Pedidos</a></li>
								<li><a href="${clientesURL}"> <i class="far fa-user"></i>Clientes
								</a></li>

								<li><a href="${ventasHistorialURL}"> <i
										class="fas fa-history"></i>Historial
								</a></li>
							</ul></li>


						<!-- Consultas -->

						<li class="has-sub"><a class="js-arrow" href="#"> <i
								class="fas fa-bed"></i>Consultas <span class="arrow"> <i
									class="fas fa-angle-down"></i>
							</span>
						</a>
							<ul class="list-unstyled navbar__sub-list js-sub-list">
								<li><a href="${ventasURL}"><i
										class="far fa-address-book"></i>Pedidos</a></li>
								<li><a href="${clientesURL}"> <i class="far fa-user"></i>Clientes
								</a></li>
							</ul></li>

					</ul>
				</nav>
			</div>
		</aside>
		<!-- END MENU SIDEBAR-->

		<!-- PAGE CONTAINER-->
		<div class="page-container">
			<!-- HEADER DESKTOP-->
			<header class="header-desktop">
				<div class="section__content section__content--p30">
					<div class="container-fluid">
						<div class="header-wrap">
							<%--<form class="form-header" action="" method="POST">
								<input class="au-input au-input--xl" type="text" name="search"
									placeholder="Search for datas &amp; reports..." />
								<button class="au-btn--submit" type="submit">
									<i class="zmdi zmdi-search"></i>
								</button>
							</form> --%>
							<div class="header-button">
								<div class="noti-wrap">
									<div class="noti__item js-item-menu">
										<i class="zmdi zmdi-comment-more"></i> <span class="quantity">1</span>
										<div class="mess-dropdown js-dropdown">
											<div class="mess__title">
												<p>You have 2 news message</p>
											</div>
											<div class="mess__item">
												<div class="image img-cir img-40">
													<img src="images/icon/avatar-06.jpg" alt="Michelle Moreno" />
												</div>
												<div class="content">
													<h6>Michelle Moreno</h6>
													<p>Have sent a photo</p>
													<span class="time">3 min ago</span>
												</div>
											</div>
											<div class="mess__item">
												<div class="image img-cir img-40">
													<img src="images/icon/avatar-04.jpg" alt="Diane Myers" />
												</div>
												<div class="content">
													<h6>Diane Myers</h6>
													<p>You are now connected on message</p>
													<span class="time">Yesterday</span>
												</div>
											</div>
											<div class="mess__footer">
												<a href="#">View all messages</a>
											</div>
										</div>
									</div>
									<div class="noti__item js-item-menu">
										<i class="zmdi zmdi-email"></i> <span class="quantity">1</span>
										<div class="email-dropdown js-dropdown">
											<div class="email__title">
												<p>You have 3 New Emails</p>
											</div>
											<div class="email__item">
												<div class="image img-cir img-40">
													<img src="images/icon/avatar-06.jpg" alt="Cynthia Harvey" />
												</div>
												<div class="content">
													<p>Meeting about new dashboard...</p>
													<span>Cynthia Harvey, 3 min ago</span>
												</div>
											</div>
											<div class="email__item">
												<div class="image img-cir img-40">
													<img src="images/icon/avatar-05.jpg" alt="Cynthia Harvey" />
												</div>
												<div class="content">
													<p>Meeting about new dashboard...</p>
													<span>Cynthia Harvey, Yesterday</span>
												</div>
											</div>
											<div class="email__item">
												<div class="image img-cir img-40">
													<img src="images/icon/avatar-04.jpg" alt="Cynthia Harvey" />
												</div>
												<div class="content">
													<p>Meeting about new dashboard...</p>
													<span>Cynthia Harvey, April 12,,2018</span>
												</div>
											</div>
											<div class="email__footer">
												<a href="#">See all emails</a>
											</div>
										</div>
									</div>
									<div class="noti__item js-item-menu">
										<i class="zmdi zmdi-notifications"></i> <span class="quantity">3</span>
										<div class="notifi-dropdown js-dropdown">
											<div class="notifi__title">
												<p>You have 3 Notifications</p>
											</div>
											<div class="notifi__item">
												<div class="bg-c1 img-cir img-40">
													<i class="zmdi zmdi-email-open"></i>
												</div>
												<div class="content">
													<p>You got a email notification</p>
													<span class="date">April 12, 2018 06:50</span>
												</div>
											</div>
											<div class="notifi__item">
												<div class="bg-c2 img-cir img-40">
													<i class="zmdi zmdi-account-box"></i>
												</div>
												<div class="content">
													<p>Your account has been blocked</p>
													<span class="date">April 12, 2018 06:50</span>
												</div>
											</div>
											<div class="notifi__item">
												<div class="bg-c3 img-cir img-40">
													<i class="zmdi zmdi-file-text"></i>
												</div>
												<div class="content">
													<p>You got a new file</p>
													<span class="date">April 12, 2018 06:50</span>
												</div>
											</div>
											<div class="notifi__footer">
												<a href="#">All notifications</a>
											</div>
										</div>
									</div>
								</div>
								<div class="account-wrap">
									<div class="account-item clearfix js-item-menu">
										<div class="image">
											<img
												src="<c:url value ="/resources/images/icon/avatar-07.jpg"/>"
												alt="Esteba Medina" />
										</div>
										<div class="content">
											<a class="js-acc-btn" href="#">Esteba Medina</a>
										</div>
										<div class="account-dropdown js-dropdown">
											<div class="info clearfix">
												<div class="image">
													<a href="#"> <img
														src="<c:url value ="resources/images/icon/avatar-07.jpg"/>"
														alt="Esteba Medina" />
													</a>

												</div>
												<div class="content">
													<h5 class="name">
														<a href="#">Esteban</a>
													</h5>
													<span class="email">esteba@gmail.com</span>
												</div>
											</div>
											<div class="account-dropdown__body">
												<div class="account-dropdown__item">
													<a href="#"> <i class="zmdi zmdi-account"></i>Account
													</a>
												</div>
												<div class="account-dropdown__item">
													<a href="#"> <i class="zmdi zmdi-settings"></i>Setting
													</a>
												</div>
												<div class="account-dropdown__item">
													<a href="#"> <i class="zmdi zmdi-money-box"></i>Billing
													</a>
												</div>
											</div>
											<div class="account-dropdown__footer">
												<a href="#"> <i class="zmdi zmdi-power"></i>Logout
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
			<!-- HEADER DESKTOP-->

			<!-- MAIN CONTENT-->
			<div class="main-content">
				<div class="section__content section__content--p30">
					<div class="container-fluid">

						<jsp:doBody />

					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT-->

			<jsp:invoke fragment="modal" />
			<!-- END MODAL CONTENT-->

			<!-- END PAGE CONTAINER-->
		</div>
	</div>


	<!-- Jquery JS-->
	<script src="<c:url value ="/resources/vendor/jquery-3.2.1.min.js"/>"></script>

	<!-- Bootstrap JS-->
	<script
		src="<c:url value ="/resources/vendor/bootstrap-4.1/popper.min.js"/>"></script>
	<script
		src="<c:url value ="/resources/vendor/bootstrap-4.1/bootstrap.min.js"/>"></script>

	<!-- Vendor JS -->

	<script
		src="<c:url value ="/resources/vendor/cleave.js/cleave.min.js"/>"></script>
	<script
		src="<c:url value ="/resources/vendor/select2/select2.min.js"/>"></script>

	<script
		src="<c:url value ="/resources/vendor/animsition/animsition.min.js"/>"></script>
	<script
		src="<c:url value ="/resources/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js"/>"></script>

	<script
		src="<c:url value ="/resources/vendor/perfect-scrollbar/perfect-scrollbar.js"/>"></script>


	<script
		src="<c:url value ="/resources/vendor/chartkick/chartkick.min.js"/>"></script>

	<script
		src="<c:url value ="/resources/vendor/chartjs/Chart.bundle.min.js"/>"></script>

	<%--
	<script src="<c:url value ="/resources/vendor/wow/wow.min.js"/>"></script>
	<script src="<c:url value ="/resources/vendor/slick/slick.min.js"/>"></script>	
	<script src="<c:url value ="/resources/vendor/counter-up/jquery.counterup.min.js"/>"></script>	
	<script src="<c:url value ="/resources/vendor/circle-progress/circle-progress.min.js"/>"></script>
	--%>

	<!-- DataTable -->
	<script
		src="<c:url value ="/resources/vendor/datatable/jquery.dataTables.min.js"/>"></script>
	<script
		src="<c:url value ="/resources/vendor/datatable/dataTables.bootstrap4.min.js"/>"></script>

	<!-- JQUERY VALIDATE -->
	<script
		src="<c:url value ="/resources/vendor/jquery.validate/jquery.validate.min.js"/>"></script>
	<script
		src="<c:url value ="/resources/vendor/jquery.validate/messages_es.js"/>"></script>

	<!-- Main JS -->
	<script src="<c:url value ="/resources/js/main.js"/>"></script>

	<!-- Main JS -->
	<script src="<c:url value ="/resources/js/app/_base.js"/>"></script>

	<!--  Custom JS -->
	<jsp:invoke fragment="footer" />

</body>
</html>
