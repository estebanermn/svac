<%-- 
    Document   : includeHead
    Created on : 26/09/2018, 02:34:48 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

    <link  rel="stylesheet" media="all" href="<c:url value="/resources/css/font-face.css"/>" >
    <link  rel="stylesheet" media="all" href="<c:url value="/resources/vendor/font-awesome-4.7/css/font-awesome.min.css"/>" >
    <link  rel="stylesheet" media="all" href="<c:url value="/resources/vendor/font-awesome-5/css/fontawesome-all.min.css"/>">
    <link  rel="stylesheet" media="all" href="<c:url value="/resources/vendor/mdi-font/css/material-design-iconic-font.min.css"/>">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" media="all" href="<c:url value="/resources/vendor/bootstrap-4.1/bootstrap.min.css"/>" >

    <!-- Vendor CSS-->
    <link rel="stylesheet" media="all" href="<c:url value="/resources/vendor/animsition/animsition.min.css"/>" >
    <link rel="stylesheet" media="all" href="<c:url value="/resources/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css"/>" >
    <link rel="stylesheet" media="all" href="<c:url value="/resources/vendor/wow/animate.css"/>" >
    <link rel="stylesheet" media="all"  href="<c:url value="/resources/vendor/css-hamburgers/hamburgers.min.css"/>" >
    <link rel="stylesheet" media="all" href="<c:url value="/resources/vendor/slick/slick.css"/>" >
    <link rel="stylesheet" media="all" href="<c:url value="/resources/vendor/select2/select2.min.css"/>" >
    <link rel="stylesheet" media="all" href="<c:url value="/resources/vendor/perfect-scrollbar/perfect-scrollbar.css"/>" >

    <!-- Main CSS-->
    <link  rel="stylesheet" media="all" href="<c:url value="/resources/css/theme.css"/>">

    <!-- data table -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
  