<%-- 
    Document   : includeEnd
    Created on : 27/09/2018, 12:34:35 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- Jquery JS-->
<script src="<c:url value ="/resources/vendor/jquery-3.2.1.min.js"/>"></script>
<!-- Bootstrap JS-->
<script src="<c:url value ="/resources/vendor/bootstrap-4.1/popper.min.js"/>"></script>
<script src="<c:url value ="/resources/vendor/bootstrap-4.1/bootstrap.min.js"/>"></script>
<!-- Vendor JS       -->

<script src="<c:url value ="/resources/vendor/slick/slick.min.js"/>"></script>

<script src="<c:url value ="/resources/vendor/wow/wow.min.js"/>"></script>

<script src="<c:url value ="/resources/vendor/animsition/animsition.min.js"/>"></script>
<script src="<c:url value ="/resources/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js"/>"></script>
<script src="<c:url value ="/resources/vendor/counter-up/jquery.counterup.min.js"/>"></script>
<script src="<c:url value ="/resources/vendor/circle-progress/circle-progress.min.js"/>"></script>
<script src="<c:url value ="/resources/vendor/perfect-scrollbar/perfect-scrollbar.js"/>"></script>
<script src="<c:url value ="/resources/vendor/chartjs/Chart.bundle.min.js"/>"></script>
<script src="<c:url value ="/resources/vendor/select2/select2.min.js"/>"></script>




<!-- Main JS-->
<script src="<c:url value ="/resources/js/main.js"/>"></script>
<!--
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
-->