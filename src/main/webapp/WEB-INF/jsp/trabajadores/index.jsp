<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<t:Layout>  
    <jsp:body>

        <div class="row">
            <div class="col-md-12">

                <h3 class="title-5 m-b-35">Trabajadores - List</h3>

                <!-- Filtros y Opciones -->
                <div class="table-data__tool">
                    <!--
                    <div class="table-data__tool-left">
                        <div class="rs-select2--light rs-select2--md">
                            <select class="js-select2" name="property">
                                <option selected="selected">All Properties</option>
                                <option value="">Option 1</option>
                                <option value="">Option 2</option>
                            </select>
                            <div class="dropDownSelect2"></div>
                        </div>
                        <div class="rs-select2--light rs-select2--sm">
                            <select class="js-select2" name="time">
                                <option selected="selected">Today</option>
                                <option value="">3 Days</option>
                                <option value="">1 Week</option>
                            </select>
                            <div class="dropDownSelect2"></div>
                        </div>
                        <button class="au-btn-filter">
                            <i class="zmdi zmdi-filter-list"></i>filters</button>
                    </div>
                    -->
                    <div class="table-data__tool-left"><p>&nbsp;</p></div>

                    <div class="table-data__tool-right">

                        <spring:url value="/trabajadores/add" var="addURL" />

                        <a class="au-btn au-btn-icon au-btn--green au-btn--small" href="${addURL}">
                            <i class="zmdi zmdi-plus"></i>Nuevo trabajador</a>
                        <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                            <select class="js-select2" name="type">
                                <option selected="selected">Export</option>
                                <option value="">Option 1</option>
                                <option value="">Option 2</option>
                            </select>
                            <div class="dropDownSelect2"></div>
                        </div>
                    </div>
                </div>


                <!-- Datos -->
                <div class="table-responsive table-responsive-data2">
                    <table class="table table-data2">
                        <thead>
                            <tr>
                                <th>C�digo</th>
                                <th>Apellido</th>
                                <th>Nombre</th>
                                <th>DNI</th>
                                <th>Direccion</th>
                                <th>Telefono</th>
                                <th>Grado</th>
                                <th>Cargo</th>
                                <th>Edad</th>
                                <th>Sexo</th>
                                <th>Fecha</th>
                                <th>Email</th>
                                <th></th>
                            </tr>
                        </thead>

                        <c:forEach items="${data}" var="trabajador">
                            <tr class="tr-shadow">
                                <td>${trabajador.codigo}</td>
                                <td>${trabajador.apellido}</td>
                                <td>${trabajador.nombre}</td>
                                <td>${trabajador.dni}</td>
                                <td>${trabajador.direccion}</td>
                                <td>${trabajador.telefono}</td>
                                <td>${trabajador.grado}</td>
                                <td>${trabajador.cargo}</td>
                                <td>${trabajador.edad}</td>
                                <td>${trabajador.sexo}</td>
                                <td>${trabajador.fechaIngreso}</td>
                                <td>${trabajador.email}</td>
                                <td>
                                    <spring:url value="/trabajadores/${trabajador.codigo}/edit" var="updateURL"></spring:url>

                                        <div class="table-data-feature">
                                            <a class="item" data-toggle="tooltip" data-placement="top" title="Edit" href="${updateURL}"> 
                                            <i class="zmdi zmdi-edit"></i> 
                                        </a>

                                        <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                            <i class="zmdi zmdi-delete"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            <tr class="spacer"></tr>
                        </c:forEach>

                        </tbody>
                    </table>
                </div>

            </div>

        </div>

    </jsp:body>
</t:Layout>


