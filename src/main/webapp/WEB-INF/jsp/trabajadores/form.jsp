<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:Layout>  
    <jsp:body>

        <div class="row">
            <div class="col-md-12">
                <h3 class="title-5 m-b-35">Trabajadores</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">Trabajador</div>

                    <spring:url value="/trabajadores/save" var="saveURL" />
                    <form:form modelAttribute="trabajadorForm" method="post" action="${saveURL}">
                        <form:hidden path="codigo"/>

                        <div class="card-body card-block">

                            
                            <div class="form-group">
                                <label for="apellido" class=" form-control-label">Apellido</label>
                                <form:input path="apellido" class="form-control"/>
                            </div>

                            <div class="form-group">
                                <label for="nombre" class=" form-control-label">Nombre</label>
                                <form:input path="nombre" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label for="dni" class=" form-control-label">DNI</label>
                                <form:input path="dni" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label for="direccion" class=" form-control-label">Direccion</label>
                                <form:input path="direccion" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label for="telefono" class=" form-control-label">Telefono</label>
                                <form:input path="telefono" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label for="grado" class=" form-control-label">Grado</label>
                                <form:input path="grado" class="form-control"/>
                            </div>

                            <div class="form-group">
                                <label for="cargo" class=" form-control-label">Cargo</label>
                                <form:input path="cargo" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label for="edad" class=" form-control-label">Edad</label>
                                <form:input path="edad" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label for="sexo" class=" form-control-label">Sexo</label>
                                <form:input path="sexo" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label for="fechaIngreso" class=" form-control-label">Fecha Ingreso</label>
                                <form:input path="fechaIngreso" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label for="email" class=" form-control-label">Email</label>
                                <form:input path="email" class="form-control"/>
                            </div>
                        </div>  


                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>

                </form:form>

            </div>
        </div>
    </div>


</jsp:body>
</t:Layout>


