<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<t:Layout>

	<jsp:attribute name="footer">
	
	<script type="text/javascript">
		var Config = {
			url : "${pageContext.request.contextPath}",
			recurso : "inventario"
		};
	</script>
    
    <script
			src="<c:url value ="/resources/js/app/almacen/inventario.js"/>"></script>
    
    </jsp:attribute>

	<jsp:body>
	
 	<div class="row">
 		<div class="col-md-12">
 			
 			<div class="map-data m-b-40">
 			
 				<h3 class="title-3 m-b-30">
						<i class="fas fa-box"></i>Ingresos a Inventario</h3>
				
				<div class="table-responsive table-responsive-data">
	                
	                <table id="data-table-default"
							class="table table-striped table-bordered" style="width: 100%">
	                            <thead>
	                            	<tr>
	                                <th>ID</th>
	                                <th>Art�culo</th>
	                                <th>Fecha de venta</th>
	                                <th>Cantidad</th>
	                                <th>Precio</th>
	                                <th>Total</th>
	                                <th>Descripci�n</th>
	                      			</tr>
	                            </thead>
	                            <tbody>
	                            <c:forEach items="${inventarioData}"
									var="item">
	                            	<c:set var="total"
										value="${item.stockIn * item.precioUnitario}" />
		                            <tr>
		                                <td>${item.codigo}</td>
		                                <td>${item.articulo}</td>
		                                <td>${item.fecha}</td>
		                                <td>${item.stockIn}</td>
		                                <td>${item.precioUnitario}</td>
		                                <td>${total}</td>
		                                <td>${item.descripcion}</td>
		                      		</tr>
	                            </c:forEach>
	                            </tbody>
	                            <tfoot>
	                            	<tr>
	                            	<th>ID</th>
	                                <th>Art�culo</th>
	                                <th>Fecha de venta</th>
	                                <th>Cantidad</th>
	                                <th>Precio</th>
	                                <th>Total</th>
	                                <th>Descripci�n</th>
	                      			</tr>
	                            </tfoot>
	                </table>
	                    
	             </div>
            </div>
        </div>
	</div>
		
    </jsp:body>

</t:Layout>
