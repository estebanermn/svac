<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<!-- modal -->
<div class="modal fade" id="modal-dialog" tabindex="-1" role="dialog"
	aria-labelledby="ModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Categor�a</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="formModal">
				<input type="hidden" id="entidadId">
				<input type="hidden" id="estado">
					<div class="form-group">
						<label for="nombre" class=" form-control-label">Nombre</label> <input
							type="text" id="nombre" name="nombre" class="form-control" minlength="4" maxlength="45" size="45"
							 required="required">
							
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-primary" id="btnModalSave">Confirmar</button>
			</div>
		</div>
	</div>
</div>
<!-- end modal -->