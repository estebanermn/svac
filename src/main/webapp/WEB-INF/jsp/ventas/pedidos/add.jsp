<%@ taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:Layout>

	<jsp:attribute name="footer">
	
		<script type="text/javascript">
			var Config = {
				url : "${pageContext.request.contextPath}",
				recurso : "ventas",
				impuesto : "${impuesto}"
			};
		</script>
	    
	    <script src="<c:url value ="/resources/js/app/_form.validaciones.js"/>"></script>
	    <script src="<c:url value ="/resources/js/app/ventas/pedidos/add/html.js"/>"></script>
	    <script src="<c:url value ="/resources/js/app/ventas/pedidos/add/form.rules.js"/>"></script>
	    <script src="<c:url value ="/resources/js/app/ventas/pedidos/add.js"/>"></script>
    
    </jsp:attribute>

	<jsp:attribute name="modal">
		<jsp:include page="modal_articulos.jsp" />
		<jsp:include page="modal_clientes.jsp" />    
    </jsp:attribute>

	<jsp:body>
	
		<script type="text/javascript">
		// @formatter:off
		var articuloData = ${articuloData};
		var clienteData = ${clienteData};
		// @formatter:on	
		</script>

        <div class="row">
            <div class="col-lg-12">
            	<div class="map-data m-b-40">
            		<h3 class="title-3 m-b-30">
            			<i class="zmdi zmdi-boat"></i>Nueva Venta</h3>
            			
            			<jsp:include page="_form.jsp" />
            	</div>			
			</div>
        </div>

    </jsp:body>
</t:Layout>


