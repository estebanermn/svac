<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!-- modal -->
<div class="modal fade show" id="modal-articulo" tabindex="-1"
	role="dialog" aria-labelledby="largeModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Art�culos</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group">

					<div class="table-responsive table-responsive-data">

						<table id="data-table-articulo"
							class="table table-striped table-bordered" style="width: 100%">
							<thead>
								<tr>
									<th>Seleccionar</th>
									<th>C�digo</th>
									<th>Categor�a</th>
									<th>Unidad</th>
									<th>Nombre</th>
									<th>Descripci�n</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>

					</div>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
<!-- end modal -->