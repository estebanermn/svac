<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<t:Layout>

	<jsp:attribute name="footer">
	
	<script type="text/javascript">
		var Config = {
			url : "${pageContext.request.contextPath}",
			recurso : "ventas"
		};
	</script>
    
    <script src="<c:url value ="/resources/js/app/ventas/pedidos/index.js"/>"></script>
    
    </jsp:attribute>

	<jsp:body>
	
	<spring:url value="/web/ventas/pedidos/add" var="addURL" />
	<spring:url value="/web/ventas/pedidos/detalle" var="detalleURL" />
	
	<div class="row">
            <div class="col-md-12">
            
            <div class="map-data m-b-40">

                <h3 class="title-3 m-b-30"><i class="zmdi zmdi-boat"></i>Ventas</h3>	
						
                <div class="table-data__tool">
                    <div class="table-data__tool-left">
						<p>&nbsp;</p>
					</div>

                    <div class="table-data__tool-right">
	
                        <a href="${addURL}"
								class="au-btn au-btn-icon au-btn--green au-btn--small">
								<i class="zmdi zmdi-plus"></i>Nueva Venta</a>
                        <div
								class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                            <select class="js-select2" name="type">
                                <option selected="selected">Exportar</option>
                                <option value="">Excel</option>
                                <option value="">PDF</option>
                            </select>
                            <div class="dropDownSelect2"></div>
                        </div>
                    </div>
                </div>

                <div class="table-responsive table-responsive-data">
                
                <table id="data-table-default"
							class="table table-striped table-bordered" style="width: 100%">
                            <thead>
                            	<tr>
                                <th>N�mero</th>
                                <th>Serie</th>
                                <th>Cliente</th>
                                <th>Fecha</th>
                                <th>Total</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                              <tr>
                                <th>N�mero</th>
                                <th>Serie</th>
                                <th>Cliente</th>
                                <th>Fecha</th>
                                <th>Total</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                                </tr>
							</tfoot>
                    </table>
                    
                </div>

            </div>

        	</div>
		</div>

    </jsp:body>

</t:Layout>
