<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<form id="formModal" action="#" method="post">
	<input type="hidden" id="id" name="id" value="0" /> <input
		type="hidden" id="empleadoId" name="empleadoId" value="${empleadoId}" />
	<input type="hidden" id="clienteId" name="clienteId" />

	<hr />

	<div class="row form-group">
		<div class="col col-md-8">
			<label for="clienteNombre" class=" form-control-label">Cliente:</label>

			<div class="input-group">
				<input type="text" id="clienteNombre" name="clienteNombre"
					placeholder="Seleccione un cliente" class="form-control"
					value="${ventaData.cliente}" required disabled>
				<div class="input-group-btn">
					<button id="btnSearchCliente" class="btn btn-primary">
						<i class="fa fa-search"></i> Buscar
					</button>
				</div>
			</div>
		</div>

		<div class="col col-md-4">
			<label for="tipo" class=" form-control-label">Tipo
				Comprobante:</label>

			<c:set var="tipoDocumento" value="${ventaData.tipoDocumento}" />

			<c:choose>

				<c:when test="${tipoDocumento.equals('Factura')}">
					<select id="tipoDocumento" name="tipoDocumento"
						class="form-control">
						<option value="Factura" selected="selected">Factura</option>
						<option value="Boleta">Boleta</option>
					</select>
				</c:when>

				<c:when test="${tipoDocumento.equals('Boleta')}">
					<select id="tipoDocumento" name="tipoDocumento"
						class="form-control">
						<option value="Factura">Factura</option>
						<option value="Boleta" selected="selected">Boleta</option>
					</select>
				</c:when>

				<c:otherwise>
					<select id="tipoDocumento" name="tipoDocumento"
						class="form-control">
						<option value="Factura">Factura</option>
						<option value="Boleta">Boleta</option>
					</select>
				</c:otherwise>
			</c:choose>

		</div>
	</div>


	<div class="row form-group">
		<div class="col col-md-12">
			<button id="btnSearchArticulo" class="btn btn-primary">
				<i class="fa fa-search"></i> Buscar Art�culos
			</button>
		</div>
	</div>

	<hr />

	<div class="table-responsive table-responsive-data">

		<table id="data-table-default"
			class="table table-striped table-bordered" style="width: 100%">
			<thead>
				<tr>
					<th>Art�culo</th>
					<th>Descripci�n</th>
					<th>Cantidad</th>
					<th>Precio</th>
					<th>Opciones</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
			<tfoot>
				<tr>
					<th>Art�culo</th>
					<th>Descripci�n</th>
					<th>Cantidad</th>
					<th>Precio</th>
					<th>Opciones</th>
				</tr>
			</tfoot>
		</table>

	</div>

	<div class="row form-group">

		<div class="col col-md-5">

			<div class="row form-group">

				<div class="col-md-6"></div>
				<div class="col-md-6">
					<label for="igv" class=" form-control-label"></label>
					<div class="input-group">
						<div class="input-group-addon">S/ Sub Total:</div>
						<input type="text" id="subtotal" name="subtotal" placeholder="0"
							class="form-control cleave-precio" value="${ventaData.subtotal}"
							disabled>
					</div>
				</div>
			</div>
		</div>

		<div class="col col-md-2">
			<label for="igv" class=" form-control-label"></label>

			<div class="input-group">
				<div class="input-group-addon">S/ IGV%:</div>
				<input type="text" id="igv" name="igv" placeholder="0"
					class="form-control cleave-precio" value="${ventaData.igv}"
					disabled>
			</div>
		</div>

		<div class="col col-md-5">

			<div class="row form-group">

				<div class="col-md-6">
					<label for="igv" class=" form-control-label"></label>
					<div class="input-group">
						<div class="input-group-addon">S/ Total:</div>
						<input type="text" id="total" name="total" placeholder="0"
							class="form-control cleave-precio" value="${ventaData.total}"
							disabled>
					</div>
				</div>

				<div class="col-md-6"></div>

			</div>
		</div>
	</div>

	<hr />

	<div class="row form-group">

		<div class="col col-md-3">
			Estado: ${ventaData.estado} <br/>
			Fecha: ${ventaData.fecha} 
		</div>


		<div class="col col-md-9 text-right">

			<c:set var="estado" value="${ventaData.estado}" />

			<c:choose>

				<c:when test="${estado.equals('Pendiente')}">
					<button id="btnPay" class="btn btn-success"
						data-id="${ventaData.id}">
						<i class="fas fa-shopping-cart"></i> Pagar Pedido
					</button>
				</c:when>

				<c:when test="${estado.equals('Pagado')}">
				</c:when>

				<c:otherwise>
					<button id="btnSave" class="btn btn-success">
						<i class="fa fa-save"></i> Registrar
					</button>
				</c:otherwise>
			</c:choose>

			<button id="btnCancelar" style="margin-left: 10px" class="btn btn-danger">
				<i class="fa fa-times"></i> Cancelar
			</button>

		</div>
	</div>


</form>