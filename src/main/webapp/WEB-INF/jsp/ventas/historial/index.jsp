<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<t:Layout>

	<jsp:attribute name="footer">
	
	<script type="text/javascript">
		var Config = {
			url : "${pageContext.request.contextPath}"
		};
	</script>
    
    </jsp:attribute>

	<jsp:body>
	
	<spring:url value="/web/ventas/historial/anual" var="anualURL" />
	<spring:url value="/web/ventas/historial/quincenal" var="quincenalURL" />
	<spring:url value="/web/ventas/historial/mensual" var="mensualURL" />
	<spring:url value="/web/ventas/historial/semana" var="semanaURL" />
	
	
	<div class="row">
            <div class="col-md-12">
            
            <div class="map-data m-b-40">

                <h3 class="title-3 m-b-30">
						<i class="fas fa-history"></i>Historial de Ventas</h3>
						
						
				
				<ul class="list-group list-group-flush">
					<li class="list-group-item">
						<a href="${anualURL}">
							<i class="fa fa-tasks"></i> Ventas Anual
						</a>
						</li>
					
					<li class="list-group-item">
						<a href="${quincenalURL}">
							<i class="fa fa-tasks"></i> Venta Quincenal
						</a>
					</li>
					
					<li class="list-group-item">
						<a href="${mensualURL}">
							<i class="fa fa-tasks"></i> Venta Mensual 
						</a>
					</li>
					
					<li class="list-group-item">
						<a href="${semanaURL}">
						<i class="fa fa-tasks"></i> Venta de la semana
						</a>
                    </li>
                
                </ul>                 

            </div>

        	</div>
		</div>

    </jsp:body>

</t:Layout>
