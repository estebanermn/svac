<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<t:Layout>

	<jsp:attribute name="footer">
	
	<%--Definir Config --%>
	
	<script type="text/javascript">
		var Config = {
			url : "${pageContext.request.contextPath}",
			recurso : "unidades"
		};
	</script>
    
    
    <script src="<c:url value ="/resources/js/app/unidades.js"/>"></script>
    
    </jsp:attribute>
    
    
    <jsp:attribute name="modal">
	<jsp:include page="modal.jsp" />   
    </jsp:attribute>
    
    <jsp:body>
    
    <script type="text/javascript">
		// @formatter:off
		var categoriaData = ${categoriaData};
	</script>
	
	
	<div class="row">
            <div class="col-md-12">
            
            <div class="map-data m-b-40">

                <h3 class="title-5 m-b-35">Unidades</h3>

                <div class="table-data__tool">
                    <div class="table-data__tool-left">
						<p>&nbsp;</p>
					</div>

                    <div class="table-data__tool-right">

                        <a 
								id="btnNewRow" class="au-btn au-btn-icon au-btn--green au-btn--small"
								href="#">
                            <i class="zmdi zmdi-plus"></i>Nuevo Unidad</a>
                        <div
								class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                            <select class="js-select2" name="type">
                                <option selected="selected">Export</option>
                                <option value="">Option 1</option>
                                <option value="">Option 2</option>
                            </select>
                            <div class="dropDownSelect2"></div>
                        </div>
                    </div>
                </div>


                <div class="table-responsive table-responsive-data">
                
                <table id="data-table-default"
							class="table table-striped table-bordered" style="width: 100%">
                            <thead>
                            	<tr>
                                <th>C�digo</th>
                                <th>Categor�a</th>
                                <th>Nombre</th>
                                <th>Prefijo</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                <th>C�digo</th>
                                <th>Categor�a</th>
                                <th>Nombre</th>
                                <th>Prefijo</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                                </tr>
                            </tfoot>
                    </table>
                    
                </div>

            </div>

        	</div>
		</div>
		

    </jsp:body>

</t:Layout>
