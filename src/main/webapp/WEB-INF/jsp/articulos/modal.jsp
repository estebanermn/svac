<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<!-- modal -->
<div class="modal fade" id="modal-dialog" tabindex="-1" role="dialog"
	aria-labelledby="ModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Art�culos</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="formModal">
					<input type="hidden" id="entidadId"> <input type="hidden"
						id="estado">

					<div class="row form-group">
						<div class="col col-md-3">
							<label for="select" class=" form-control-label">Categor�a</label>
						</div>
						<div class="col-12 col-md-9">
							<select name="categoria_select" id="categoria_select"
								class="form-control">

							</select>
						</div>
					</div>


					<div class="row form-group">
						<div class="col col-md-3">
							<label for="select" class=" form-control-label">Unidad</label>
						</div>
						<div class="col-12 col-md-9">
							<select name="unidad_select" id="unidad_select"
								class="form-control">
							</select>
						</div>
					</div>

					<div class="form-group">

						<label for="nombre" class=" form-control-label">Nombre</label> <input
							type="text" id="nombre" name="nombre" class="form-control"
							minlength="4" maxlength="10" size="10" required="required">
						<label for="descripcion" class=" form-control-label">Descripci�n</label>
						<input type="text" id="descripcion" name="descripcion"
							class="form-control" minlength="2" maxlength="20" size="20"
							required="required"> <label for="precio"
							class=" form-control-label">Precio</label> <input type="text"
							id="precio" name="precio" class="form-control"
							required="required">
						<label for="precioProveedor"
							class=" form-control-label">Precio Proveedor</label> <input type="text"
							id="precioProveedor" name="precioProveedor" class="form-control"
							required="required">	


					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-primary" id="btnModalSave">Confirmar</button>
			</div>
		</div>
	</div>
</div>
<!-- end modal -->