<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<!-- modal -->
<div class="modal fade show" id="modal-dialog" tabindex="-1"
	role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Proveedores</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="formModal">
					<input type="hidden" id="entidadId"> <input type="hidden"
						id="tipo"> <input type="hidden" id="estado">
					<div class="form-group">
						<label for="nombre" class=" form-control-label">Nombre:</label> <input
							type="text" id="nombre" name="nombre" class="form-control"
							minlength="4" maxlength="10" size="10" required="required">

					</div>


					<div class="row form-group">
						<div class="col col-md-6">
							<label for="select" class=" form-control-label">Tipo
								Documento:</label> <select name="documento_select" id="documento_select"
								class="form-control">
								<option value="DNI">DNI</option>
								<option value="RUC">RUC</option>
							</select>
						</div>
						<div class="col-12 col-md-6">

							<label for="select" class=" form-control-label">N�Documento:</label>

							<input type="text" id="numeroDocumento" name="numeroDocumento"
								class="form-control" minlength="8" maxlength="11" size="11"
								required="required">
						</div>
					</div>


					<div class="row form-group">
						<div class="col col-md-4">
							<label for="select" class=" form-control-label">Departamento:</label><input type="text" id="departamento" name="departamento"
								class="form-control" minlength="3" maxlength="20" size="20"
								required="required">

						</div>
						<div class="col-12 col-md-4">

							<label for="select" class=" form-control-label">Provincia:</label>

							<input type="text" id="provincia" name="provincia"
								class="form-control" minlength="3" maxlength="45" size="45"
								required="required">
						</div>

						<div class="col col-md-4">
							<label for="select" class=" form-control-label">Distrito:</label>
							<input type="text" id="distrito" name="distrito"
								class="form-control" minlength="3" maxlength="45" size="45"
								required="required">
						</div>
					</div>

					<div class="row form-group">
						<div class="col col-md-4">
							<label for="select" class=" form-control-label">Direcci�n:</label>
							<input type="text" id="direccion" name="direccion"
								class="form-control" minlength="3" maxlength="45" size="45"
								required="required">

						</div>
						<div class="col-12 col-md-4">

							
							<label for="text" class=" form-control-label">Tel�fono:</label> <input
								type="text" id="telefono" name="telefono" class="form-control"
								minlength="10" maxlength="11" size="11" required="required">
						</div>

						<div class="col col-md-4">
							<label for="select" class=" form-control-label">Email:</label> <input
								type="email" id="email" name="email" class="form-control"
								minlength="5" maxlength="20" size="20" required="required">
						</div>
					</div>

					<div class="row form-group">

						<div class="col-12 col-md-6">

							<label for="text" class=" form-control-label">N�Cuenta:</label> <input
								type="text" id="numeroCuenta" name="numeroCuenta"
								class="form-control" maxlength="20" size="20"
								required="required">
						</div>

						<div class="col col-md-6"></div>
					</div>



				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-primary" id="btnModalSave">Confirmar</button>
			</div>
		</div>
	</div>
</div>
<!-- end modal -->