<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<t:Layout>

	<jsp:attribute name="footer">
	
	<script type="text/javascript">
		var Config = {
			url : "${pageContext.request.contextPath}"
		};
	</script>
    
    </jsp:attribute>

	<jsp:body>
	
	<spring:url value="/web/compras/historial/anual" var="anualURL" />
	<spring:url value="/web/compras/historial/quincenal" var="quincenalURL" />
	<spring:url value="/web/compras/historial/mensual" var="mensualURL" />
	<spring:url value="/web/compras/historial/semana" var="semanaURL" />
	
	
	<div class="row">
		<div class="col-md-12">
			<div class="map-data m-b-40">
			
				<h3 class="title-3 m-b-30">
						<i class="fas fa-history"></i>Historial de Compras</h3>
				
				<ul class="list-group list-group-flush">
					<li class="list-group-item">
						<a href="${anualURL}">
							<i class="fa fa-tasks"></i> Compras en el A�o
						</a>
						</li>
					
					<li class="list-group-item">
						<a href="${mensualURL}">
							<i class="fa fa-tasks"></i> Compras del Mes 
						</a>
					</li>
					
					<li class="list-group-item">
						<a href="${quincenalURL}">
							<i class="fa fa-tasks"></i> Compras de la Quincena
						</a>
					</li>
					
					<li class="list-group-item">
						<a href="${semanaURL}">
						<i class="fa fa-tasks"></i> Compras esta semana
						</a>
                    </li>
                
                </ul>                 

            </div>
    	</div>
	</div>

    </jsp:body>

</t:Layout>
