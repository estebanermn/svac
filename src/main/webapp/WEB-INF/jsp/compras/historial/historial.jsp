<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<t:Layout>

	<jsp:attribute name="footer">
	
	<script type="text/javascript">
		var Config = {
			url : "${pageContext.request.contextPath}",
			recurso : "compras",
			tipoHistorial : "${tipoHistorial}"
		};
	</script>
    
    <script
			src="<c:url value ="/resources/js/app/compras/historial/historial.js"/>"></script>
    
    </jsp:attribute>

	<jsp:body>
	
	<div class="row">
            <div class="col-md-12">
            
            <div class="map-data m-b-40">

                <h3 class="title-3 m-b-30">
						<i class="zmdi zmdi-boat"></i>Compras</h3>

                <div class="table-responsive table-responsive-data">
                
                <table id="data-table-default"
							class="table table-striped table-bordered" style="width: 100%">
                            <thead>
                            	<tr>
                                <th>C�digo</th>
                                <th>Proveedor</th>
                                <th>Fecha</th>
                                 <th>Estado</th>
                                <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                              <tr>
                                <th>C�digo</th>
                                <th>Proveedor</th>
                                <th>Fecha</th>
                                 <th>Estado</th>
                                <th>Opciones</th>
                                </tr>
							</tfoot>
                    </table>
                    
                </div>

            </div>

        	</div>
		</div>

    </jsp:body>

</t:Layout>
