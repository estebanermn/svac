<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<form id="formModal" action="#" method="post">
	<input id="id" name="id" type="hidden" value="0" /> <input
		type="hidden" id="empleadoId" name="usuarioId" value="${empleadoId}" />
	<input type="hidden" id="proveedorId" name="proveedorId" />


	<div class="row form-group">
		<div class="col col-md-8">
			<label for="proveedorNombre" class=" form-control-label">Proveedor:</label>

			<div class="input-group">
				<input type="text" id="proveedorNombre" name="proveedorNombre"
					placeholder="Seleccione un proveedor" class="form-control"
					value="${compraData.proveedor}" disabled>
				<div class="input-group-btn">
					<button id="btnSearchProveedor" class="btn btn-primary">
						<i class="fa fa-search"></i> Buscar
					</button>
				</div>
			</div>
		</div>


		<div class="col col-md-4">
			<label for="tipo" class=" form-control-label">Tipo
				Comprobante:</label> <select id="comprobanteTipo" id="comprobanteTipo"
				name="comprobanteTipo" class="form-control">
				<option value="Factura">Factura</option>
			</select>
		</div>

	</div>

	<div class="row form-group">
		<div class="col col-md-12">
			<a id="btnSearchArticulo" class="btn btn-primary" href="#"><i
				class="fa fa-search"></i> Buscar Art�culos</a>
		</div>
	</div>

	<div class="table-responsive table-responsive-data">

		<table id="data-table-default"
			class="table table-striped table-bordered" style="width: 100%">
			<thead>
				<tr>
					<th>Art�culo</th>
					<th>Descripci�n</th>
					<th>Cantidad</th>
					<th>Precio</th>
					<th>Opciones</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
			<tfoot>
				<tr>
					<th>Art�culo</th>
					<th>Descripci�n</th>
					<th>Cantidad</th>
					<th>Precio</th>
					<th>Opciones</th>
			</tfoot>
		</table>

	</div>


	<div class="row form-group">

		<div class="col col-md-12">
			<br /> <br />
			<h4>Compra estimada en:</h4>
		</div>

		<div class="col col-md-5">

			<div class="row form-group">

				<div class="col-md-6"></div>
				<div class="col-md-6">
					<label for="igv" class=" form-control-label"></label>
					<div class="input-group">
						<div class="input-group-addon">S/ Sub Total:</div>
						<input type="text" id="subtotal" name="subtotal" placeholder="0"
							class="form-control cleave-precio" value="" disabled>
					</div>
				</div>
			</div>
		</div>

		<div class="col col-md-2">
			<label for="igv" class=" form-control-label"></label>

			<div class="input-group">
				<div class="input-group-addon">S/ IGV%:</div>
				<input type="text" id="igv" name="igv" placeholder="0"
					class="form-control cleave-precio" value="" disabled>
			</div>
		</div>

		<div class="col col-md-5">

			<div class="row form-group">

				<div class="col-md-6">
					<label for="igv" class=" form-control-label"></label>
					<div class="input-group">
						<div class="input-group-addon">S/ Total:</div>
						<input type="text" id="total" name="total" placeholder="0"
							class="form-control cleave-precio" value="" disabled>
					</div>
				</div>

				<div class="col-md-6"></div>

			</div>
		</div>
	</div>

	<hr />

	<div class="row form-group">

		<div class="col col-md-3">
			Estado: <strong>${compraData.estado}</strong> <br /> Fecha:
			${compraData.fecha}
		</div>


		<div class="col col-md-9 text-right">

			<c:set var="estado" value="${compraData.estado}" />

			<c:choose>

				<c:when test="${estado.equals('Pendiente')}">
					<button id="btnAprobar" class="btn btn-success"
						data-id="${compraData.id}">
						<i class="fas fa-check"></i> Aprobar
					</button>

					<button id="btnAnular" style="margin-left: 10px"
						class="btn btn-danger" data-id="${compraData.id}">
						<i class="fa fa-times"></i> Anular
					</button>
				</c:when>

				<c:when test="${estado.equals('Aprobado')}">
					<button id="btnPagar" class="btn btn-success"
						data-id="${compraData.id}">
						<i class="fas fa-shopping-cart"></i> Pagar
					</button>

				</c:when>

				<c:when test="${estado.equals('Pagado')}">
					<button id="btnRecibido" class="btn btn-success"
						data-id="${compraData.id}">
						<i class="fas fa-shopping-cart"></i> Marcar como recibido
					</button>

				</c:when>

				<c:when test="${estado.equals('Recibido')}">
				</c:when>

				<c:when test="${estado.equals('Anulado')}">
				</c:when>

				<c:otherwise>
					<button id="btnSave" class="btn btn-success">
						<i class="fa fa-save"></i> Registrar
					</button>
				</c:otherwise>
			</c:choose>



		</div>
	</div>

</form>