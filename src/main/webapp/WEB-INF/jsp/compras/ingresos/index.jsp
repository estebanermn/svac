<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<t:Layout>

	<jsp:attribute name="footer">
	
	<script type="text/javascript">
		var Config = {
			url : "${pageContext.request.contextPath}",
			recurso : "compras"
		};
	</script>
    
    <script src="<c:url value ="/resources/js/app/compras/ingresos/index.js"/>"></script>
    
    </jsp:attribute>

	<jsp:body>
	
	<spring:url value="/web/compras/ingresos/add" var="addURL" />
	
	<div class="row">
            <div class="col-md-12">
            
            <div class="map-data m-b-40">

                <h3 class="title-3 m-b-30"><i class="zmdi zmdi-boat"></i>�rdenes de Compra</h3>	

                <div class="table-data__tool">
                    <div class="table-data__tool-left">
						<p>&nbsp;</p>
					</div>

                    <div class="table-data__tool-right">
	
                        <a href="${addURL}"
								class="au-btn au-btn-icon au-btn--green au-btn--small">
								<i class="zmdi zmdi-plus"></i>Nueva Orden</a>
                        <div
								class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                            <select class="js-select2" name="type">
                                <option selected="selected">Export</option>
                                <option value="">Option 1</option>
                                <option value="">Option 2</option>
                            </select>
                            <div class="dropDownSelect2"></div>
                        </div>
                    </div>
                </div>

                <div class="table-responsive table-responsive-data">
                
                <table id="data-table-default"
							class="table table-striped table-bordered" style="width: 100%">
                            <thead>
                            	<tr>
                                <th>C�digo</th>
                                <th>Proveedor</th>
                                <th>Fecha</th>
                                 <th>Estado</th>
                                <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                <th>C�digo</th>
                                <th>Proveedor</th>
                                <th>Fecha</th>
                                 <th>Estado</th>
                                <th>Opciones</th>
							</tfoot>
                    </table>
                    
                </div>

            </div>

        	</div>
		</div>

    </jsp:body>

</t:Layout>
