<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<t:Layout>

	<jsp:attribute name="footer">
	
	<script type="text/javascript">
		var Config = {
			url : "${pageContext.request.contextPath}",
			recurso : "categorias"
		};
	</script>
    
    <script src="<c:url value ="/resources/js/app/dashboard.js"/>"></script>
    
    </jsp:attribute>

	<jsp:body>
	
	<script type="text/javascript">
		// @formatter:off
		var productosMasVendidosData = ${chart1};
		var productosMenosVendidosData = ${chart2};
		// @formatter:on
	</script>
	
	<spring:url value="/web/ventas/pedidos/add" var="addURL" />

<div class="row">
    <div class="col-md-12">
        <div class="overview-wrap">
            <h2 class="title-1">Resumen del mes</h2>

            <a class="au-btn au-btn-icon au-btn--blue" href="${addURL}">
                <i class="zmdi zmdi-plus"></i>Nueva venta</a>
        </div>
    </div>
</div>

<div class="row m-t-25">
    <div class="col-sm-6 col-lg-3">
        <div class="overview-item overview-item--c1">
            <div class="overview__inner">
                <div class="overview-box clearfix">
                    <div class="icon">
                        <i class="zmdi zmdi-account-o"></i>
                    </div>

                    <div class="text">
                        <h2>${widget1}</h2>
                        <span>Nuevos clientes</span>
                    </div>
                </div>

                <div class="overview-chart">
                    <canvas id="widgetChart1"></canvas>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-lg-3">
        <div class="overview-item overview-item--c2">
            <div class="overview__inner">
                <div class="overview-box clearfix">
                    <div class="icon">
                        <i class="zmdi zmdi-shopping-cart"></i>
                    </div>

                    <div class="text">
                        <h2>${widget2}</h2>
                        <span>Art�culos vendidos</span>
                    </div>
                </div>

                <div class="overview-chart">
                    <canvas id="widgetChart2"></canvas>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-lg-3">
        <div class="overview-item overview-item--c3">
            <div class="overview__inner">
                <div class="overview-box clearfix">
                    <div class="icon">
                        <i class="zmdi zmdi-calendar-note"></i>
                    </div>
                    <div class="text">
                        <h2>${widget3}</h2>
                        <span>Ventas</span>
                    </div>
                </div>
                <div class="overview-chart">
                    <canvas id="widgetChart3"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-lg-3">
        <div class="overview-item overview-item--c4">
            <div class="overview__inner">
                <div class="overview-box clearfix">
                    <div class="icon">
                        <i class="zmdi zmdi-money"></i>
                    </div>
                    <div class="text">
                        <h2>${widget4}</h2>
                        <span>Total</span>
                    </div>
                </div>
                <div class="overview-chart">
                    <canvas id="widgetChart4"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-lg-6">
        <div class="au-card chart-percent-card">
            <div class="au-card-inner">
                <h3 class="title-2 tm-b-5">Productos m�s Vendidos</h3>
                <br>
						<div id="pie_chart_mas_vendidos" style="height: 300px;"></div>    
            </div>
        </div>
    </div>
    
    <div class="col-lg-6">
        <div class="au-card chart-percent-card">
            <div class="au-card-inner">
                <h3 class="title-2 tm-b-5">Productos menos Vendidos</h3>
                <br>
						<div id="pie_chart_menos_vendidos" style="height: 300px;"></div>    
            </div>
        </div>
    </div>
</div>
	
	
	</jsp:body>
</t:Layout>
