$(document).ready(function() {

	function disableHtmlElements() {
		$("#tipoDocumento").prop("disabled", true);
		$("#btnSearchCliente").prop("disabled", true);
		$("#btnSearchArticulo").prop("disabled", true);
		$(".sum-cantidad").prop("disabled", true);

		if ($("#btnDeleteTR")) {
			$("#btnDeleteTR").remove();
		}
	}

	function generateUrl(recurso, entidadId) {
		var url = Config.url;

		if (typeof entidadId == "undefined" || entidadId == null) {
			return url + '/api/' + recurso;
		} else {
			return url + '/api/' + recurso + '/' + entidadId;
		}
	}

	function _postAPICall(url, data, callback) {

		$.ajax({
			type : 'POST',
			url : url,
			data : JSON.stringify(data),
			crossDomain : true,
			contentType : "application/json; charset=utf8",
			// dataType : "json",
			async : true,
			success : function(response) {
				callback(response);
			},
			error : function(data) {
				console.log(data);
			}
		});
	}

	ventaDetallesData.forEach(function(item) {

		var entidadId = item.articuloId;
		var producto = getObjectById(articuloData, entidadId);

		var entidad = {
			id : item.articuloId,
			nombre : producto.nombre,
			descripcion : producto.descripcion,
			cantidad : item.cantidad,
			precio : item.precio
		}

		var row = addDetalleIngresoHTML(entidad);

		$('#data-table-default> tbody:last').append(row);

		disableHtmlElements();
		refreshFormValidations();

	});

	// Pagar Venta
	$('body').on('click', '#btnPay', function(e) {
		e.preventDefault();

		var data = {
			documentoVentaId : $(this).data('id')
		}

		var url = generateUrl('pagos', null);

		_postAPICall(url, data, function() {
			alert("La venta ha sido pagada.");
			location.reload();
		})

	});

	// Cancelar
	$('body').on('click', '#btnCancelar', function(e) {
		e.preventDefault();
		var url = '/web/ventas/pedidos/';
		redirect(url);
	});

});