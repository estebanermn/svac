$(document).ready(function() {

	/*--------------- Modal: Cliente ---------------*/

	var dtColumns2 = [ {
		data : "id",
		'orderable' : false,
		'render' : function(data, type, full, meta) {
			var html = renderCheckButton(data);
			return html;
		}
	}, {
		data : "nombre"
	}, {
		data : "documento"
	}, {
		data : "numeroDocumento"
	}, {
		data : "email"
	} ];

	var dt2 = createDataTable("#data-table-cliente", dtColumns2);

	$('body').on('click', '#btnSearchCliente', function(e) {

		e.preventDefault();
		$('#modal-cliente').modal('show');
		updateDataTable('#data-table-cliente', clienteData)
	});

	// Add Item
	$('#data-table-cliente').on('click', '#btnCheckRow', function(e) {

		var entidadId = $(this).data('id');
		var entidad = getObjectById(clienteData, entidadId);

		$('#clienteId').val(entidad.id);
		$('#clienteNombre').val(entidad.nombre);
		$('#modal-cliente').modal('hide');

	}); // add item

	/*--------------- Modal: Articulo ---------------*/

	var dtColumns3 = [ {
		data : "id",
		'orderable' : false,
		'render' : function(data, type, full, meta) {
			var html = renderCheckButton(data);
			return html;
		}
	}, {
		data : "codigo"
	}, {
		data : "categoria"
	}, {
		data : "unidad"
	}, {
		data : "nombre"
	}, {
		data : "descripcion"
	} ];

	var dt2 = createDataTable("#data-table-articulo", dtColumns3);

	$('body').on('click', '#btnSearchArticulo', function(e) {

		e.preventDefault();
		$('#modal-articulo').modal('show');
		updateDataTable('#data-table-articulo', articuloData)
	});

	// Add Item
	$('#data-table-articulo').on('click', '#btnCheckRow', function(e) {

		var entidadId = $(this).data('id');
		var entidad = getObjectById(articuloData, entidadId);

		var row = addDetalleIngresoHTML(entidad);

		$('#data-table-default> tbody:last').append(row);

		refreshFormValidations();

	}); // add item

	/*--------------- Compra: Ingreso ---------------*/

	$("#formModal").validate({
		rules : formRules
	});

	$("#formModal").submit(function(e) {
		e.preventDefault();
	});

	function getFormToJSON() {

		var obj = {};

		obj.clienteId = $.trim($("#clienteId").val());
		obj.fecha = Date.now();
		obj.subtotal = getNumber($("#subtotal").val());
		obj.igv = getNumber($("#igv").val());
		obj.total = getNumber($("#total").val());
		obj.empleadoId = $.trim($("#empleadoId").val());
		obj.tipoDocumento = $('#tipoDocumento option:selected').val();

		obj.items = [];

		$('#data-table-default tbody tr').each(function() {

			var item = {};

			var precio = $(this).find("#precio").val();
			precio = Number(precio.replace(/[^0-9.-]+/g, ""));

			item.articuloId = $(this).find("#articuloId").val();
			item.cantidad = $(this).find("#cantidad").val();
			item.precio = precio;
			item.descuento = 0;
			item.total = parseFloat(item.cantidad * precio);

			// console.log(item);
			obj.items.push(item);
		});

		return obj;
	}

	// Delete TR
	$('body').on('click', '#btnDeleteTR', function(e) {

		$(this).closest('tr').remove();
		updateTotales();
		return false;
	});

	function updateTotales() {

		var formToJSON = getFormToJSON();

		var subtotal = 0;

		formToJSON.items.forEach(function(item) {

			item.total = parseFloat(item.cantidad * item.precio);
			// console.log(item);

			subtotal += item.total;
		});

		var impuesto = (Config.impuesto ? Config.impuesto : 18);
		var igv = (subtotal * impuesto) / 100;
		var total = subtotal + igv;

		$("#subtotal").val(subtotal);
		$("#igv").val(igv);
		$("#total").val(total);

		refreshFormValidations();
	}

	// Calcular montos
	$('body').on('change keyup', '.sum-cantidad', function(e) {
		updateTotales();
	});

	$('body').on('change keyup', '.sum-precio', function(e) {
		updateTotales();
	});

	// Guardar
	$('body').on('click', '#btnSave', function(e) {

		var isValid = $("#formModal").valid();

		if (!isValid) {
			return;
		}

		var formToJSON = getFormToJSON();

		console.log(formToJSON);

		// if (!getFormToJSON.clienteId) {
		if (getFormToJSON.clienteId === "") {
			alert("Error: Se debe seleccionar un cliente.")
			return;
		}

		if (formToJSON.items.length == 0) {
			alert("Error: Se debe ingresar al menos un producto.")
			return;
		}

		postAPICall(null, formToJSON, function(response) {
			alert("Se ingreso la compra correctamente.");
			var url = '/web/ventas/pedidos/' + response.id;
			console.log(url);
			redirect(url);
		});

	});

	// Cancelar
	$('body').on('click', '#btnCancelar', function(e) {
		e.preventDefault();
		var url = '/web/ventas/pedidos/';
		redirect(url);
	});

});