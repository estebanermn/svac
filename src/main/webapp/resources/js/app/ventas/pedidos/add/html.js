function addDetalleIngresoHTML(entidad) {

	var articuloId = '<input type="hidden" id="articuloId" value="'
			+ entidad.id + '">';
	var descripcion = entidad.descripcion;
	var cantidad = '<input type="text" id="cantidad" class="form-control cleave-numero sum-cantidad" required min="1">';
	var precio = '<input type="text" id="precio" class="form-control cleave-precio sum-precio" required value="'
			+ entidad.precio + '" disabled>';
	var opciones = '<button id="btnDeleteTR" class="btn btn-danger"> <i class="fa fa-times"></i></button>';

	if (typeof entidad.cantidad != 'undefined') {
		cantidad = '<input type="text" id="cantidad" class="form-control cleave-numero sum-cantidad" required value="'
				+ entidad.cantidad + '">';
	}

	var html = '<tr>';
	html += '<td>' + entidad.nombre + '</td>';
	html += '<td>' + entidad.descripcion + '</td>';
	html += '<td>' + cantidad + '</td>';
	html += '<td>' + precio + '</td>';
	html += '<td>' + articuloId + opciones + '</td>';
	html += '</tr>';

	return html;
}
