$(document).ready(function() {

	var dtColumns = [ {
		data : "documentoVentaNumero"
	}, {
		data : "documentoVentaSerie"
	}, {
		data : "cliente"
	}, {
		data : "fecha"
	}, {
		data : "total"
	}, {
		data : "estado"
	}, {
		data : "id",
		'orderable' : false,
		'render' : function(data, type, full, meta) {

			var url = Config.url + '/web/ventas/pedidos/' + data;
			var html = renderOpen();
			html += renderLookItemsButton(data, url)
			html += renderClose();
			return html;
		}
	} ];

	var dt = createDataTable('#data-table-default', dtColumns);

	function updateDataTable(tipoHistorial) {
		$('#data-table-default').dataTable().fnClearTable();
		$('#data-table-default').dataTable().fnDraw();

		var url = generateHistorialUrl('ventas', tipoHistorial)

		_getAPICall(url, function(response) {
			if (response.length > 0) {
				$('#data-table-default').dataTable().fnAddData(response);
			}
		});
	}

	var tipoHistorial = Config.tipoHistorial;

	updateDataTable(tipoHistorial);

});