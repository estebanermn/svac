$(document).ready(function() {

	var formRules = {
		nombre : {
			required : true,
			minlength : 4,
			maxlength : 40
		}
	};

	function resetForm() {
		$("#entidadId").val('');
		$("#nombre").val('');
		$("#estado").val('');
	}

	function populateForm(response) {
		$("#entidadId").val(response.id);
		$("#nombre").val(response.nombre);
		$("#estado").val(response.estado);
	}

	var dtColumns = [ {
		data : "codigo"
	}, {
		data : "nombre"
	}, {
		data : "estado",
		'orderable' : false,
		'render' : function(data, type, full, meta) {
			return (data == 1 ? "Activo" : "Inactivo");
		}
	}, {
		data : "id",
		'orderable' : false,
		'render' : function(data, type, full, meta) {
			var html = renderEditButton(data) + renderChangeButton(full);
			return html;
		}
	} ];

	var dt = $("#data-table-default").DataTable({
		responsive : true,
		columns : dtColumns,
		"language" : {
			"url" : "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
		}
	});

	function updateDataTable() {

		$('#data-table-default').dataTable().fnClearTable();
		$('#data-table-default').dataTable().fnDraw();

		getAPICall(null, function(response) {
			$('#data-table-default').dataTable().fnAddData(response);
		});

	}

	updateDataTable();

	$("#formModal").validate({
		rules : formRules
	});

	$("#formModal").submit(function(e) {
		e.preventDefault();
	});

	// Nuevo
	$('body').on('click', '#btnNewRow', function(e) {

		resetForm();
		$('#modal-dialog').modal('show');
	});

	// Editar
	$('#data-table-default').on('click', '#btnEditRow', function(e) {

		var entidadId = $(this).data('id');

		getAPICall(entidadId, function(response) {
			populateForm(response)
			$('#modal-dialog').modal();
		});

	}); // editar

	// Guardar
	$('body').on('click', '#btnModalSave', function(e) {
		var isValid = $("#formModal").valid();

		if (!isValid) {
			return;
		}

		var entidadId = $.trim($("#entidadId").val());
		var formToJSON = {};

		var nombre = $.trim($("#nombre").val());
		var estado = $.trim($("#estado").val());

		if (nombre) {
			formToJSON['nombre'] = nombre;
		}
		if (estado) {
			formToJSON['estado'] = estado;
		}

		if (entidadId.length == 0) {
			postAPICall(entidadId, formToJSON, function(response) {
				$("#modal-dialog").modal('hide');
				updateDataTable();
			});

		} else {
			putAPICall(entidadId, formToJSON, function(response) {
				$("#modal-dialog").modal('hide');
				updateDataTable();
			});
		}

	}); // guardar

	// Cambiar estado
	$('#data-table-default').on('click', '#btnChangeRow', function(e) {

		var entidadId = $(this).data('id');

		var estado = $(this).data('estado');
		estado = (estado == 0 ? "true" : "false");

		changeEstadoAPICall(entidadId, estado, function(response) {
			$("#modal-dialog").modal('hide');
			updateDataTable();
		});

	}); // Cambiar estado

});