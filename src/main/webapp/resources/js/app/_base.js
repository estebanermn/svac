var enableDebug = false;

function renderEditButton(id) {

	var html = '<div class="table-data-feature">';
	html += '<a id="btnEditRow" class="item btn-primary" data-id="'
			+ id
			+ '" data-toggle="tooltip" data-placement="top" title="Editar" href="#">';
	html += '<i class="zmdi zmdi-edit"></i></a>';
	return html;
}

function renderChangeButton(data) {
	var html = '<button  id="btnChangeRow"  class="item  btn-danger " data-id="'
			+ data.id
			+ '"data-estado="'
			+ data.estado
			+ '"data-toggle="tooltip" data-placement="top" title="" data-original-title="Cambiar estado">';
	html += '<i class="zmdi zmdi-refresh "></i> </button></div>';
	return html;
}


function renderPayButton(id) {

	var html = 	'<div class="table-data-feature">';	
	html += '<a id="btnPayRow" class="item btn-success" data-id="'
			+ id
			+ '"data-toggle="tooltip" data-placement="top" title="Pagar">';
	html += '<i class="zmdi zmdi-money"></i></a></div>';
		return html;
	
}


function renderOpen(){
	var html = '<div class="table-data-feature">'
	return html;
}

function renderClose(){
	var html = '</div>';
	return html;
}

function renderLookItemsButton(data, url) {
	var html = '<a  id="btnLookItemsRow" href="'+url+'" class="item  btn-danger" data-id="'
			+ data.id 
			+ '"data-toggle="tooltip" data-placement="top"  title="Detalle de la venta">';
	html += '<i class="fa fa-eye"></i> </a>';
	return html;
}

function renderCheckButton(data) {
	var html = '<button type="button" id="btnCheckRow" class="btn btn-primary" data-id="'
			+ data + '">Click Me!</button>'
	return html;
}


/* ============================== Generate URLs ============================== */

function getResourceUrl(entidadId) {

	if (typeof entidadId == "undefined" || entidadId == null) {
		return Config.url + '/api/' + Config.recurso;
	} else {
		return Config.url + '/api/' + Config.recurso + '/' + entidadId;
	}
}

function generateHistorialUrl(resource, value) {
	var url = Config.url;
	url = url + '/api/' + resource + '/historial/' + value;
	console.log(url);
	return url;
}

/* ============================== API Calls ============================== */

function _getAPICall(url, callback) {

	$.ajax({
		type : "GET",
		url : url,
		crossDomain : true,
		contentType : "application/json; charset=utf8",
		dataType : "json",
		async : true,
		success : function(response) {
			callback(response);
		}
	});
}


/* ============================== API Calls ============================== */

function getAPICall(entidadId, callback) {

	var url = getResourceUrl(entidadId);
	_getAPICall(url, callback);
}

function postAPICall(entidadId, formToJSON, callback) {

	var url = getResourceUrl(entidadId);

	$.ajax({
		type : 'POST',
		url : url,
		data : JSON.stringify(formToJSON),
		crossDomain : true,
		contentType : "application/json; charset=utf8",
		// dataType : "json",
		async : true,
		success : function(response) {
			callback(response);
		},
		error : function(data) {
			console.log(data);
		}
	});

}

function putAPICall(entidadId, formToJSON, callback) {

	var url = getResourceUrl(entidadId);

	if (enableDebug) {
		console.log("--- debug ---");
		console.log(formToJSON);
	}

	$.ajax({
		type : "PUT",
		url : url,
		data : JSON.stringify(formToJSON),
		crossDomain : true,
		contentType : "application/json; charset=utf8",
		// dataType : "json",
		async : true,
		success : function(response) {
			callback(response)
		},
		error : function(data) {
			console.log(data);
		}
	});
}

function changeEstadoAPICall(entidadId, estado, callback) {

	var url = getResourceUrl(entidadId);
	url = url + "/estado"

	if (enableDebug) {
		console.log("--- debug ---");
		console.log(estado);
	}

	$.ajax({
		type : "PUT",
		url : url,
		data : estado,
		crossDomain : true,
		contentType : "application/json; charset=utf8",
		// dataType : "json",
		async : true,
		success : function(response) {
			callback(response)
		},
		error : function(data) {
			console.log(data);
		}
	});
}


/* ============================== dataTable ============================== */

function createDataTable(tableId, columns) {
	
	// var url = "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json";
	var url = Config.url + '/resources/vendor/datatable/Spanish.json'; 
	
    var options = {
        responsive: true,
        // "order": [],
        "language": {
            "url": url
        }
    };

    if (typeof columns != "undefined" || columns != null) {
        options.columns = columns;
    }
    
    var dt = $(tableId).DataTable(options);
}

function updateDataTable(tableId, data) {
	$(tableId).dataTable().fnClearTable();
	$(tableId).dataTable().fnDraw();
	$(tableId).dataTable().fnAddData(data);
}

/* ============================== select2 ============================== */

function prepareSelect2Data(data) {
	var results = [];

	data.forEach(function(element) {
		console.log(element);
		var id = parseInt(element.id);
		var arr = {
			"id" : id,
			"text" : element.nombre
		};
		results.push(arr);
	});

	return results;
}

function prepareSelect2DataSelected(data) {
	var results = [];

	data.forEach(function(element) {
		var id = parseInt(element.id);
		results.push(id);
	});

	return results;
}

/* ============================== otros ============================== */

/**
 * Obtener el Objecto de un array por el Id
 * 
 * @param MyArray
 * @param value
 * @returns
 */
function getObjectById(MyArray, value) {
	var index = MyArray.findIndex(x => x.id === value);
	return MyArray[index];
}


/* ============================ jQuery.validator ============================ */

jQuery.validator.setDefaults({
    highlight: function(element) {
        jQuery(element).closest('.form-control').addClass('is-invalid');
    },
    unhighlight: function(element) {
        jQuery(element).closest('.form-control').removeClass('is-invalid');
    },
    errorElement: 'span',
    errorClass: 'label label-danger',
    errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }
});


/* ============================ Helpers ============================ */

function redirect(path){
	var url = Config.url;
	url += path;
	window.location.replace(url);
}

function getNumber(value) {
	var result = Number(value.replace(/[^0-9.-]+/g, ""));
	return result;s
}