$(document).ready(function() {

	function parsePieChartData(data) {
		var result = [];
		data.forEach(function(obj) {
			item = [ obj.key, obj.value ]
			result.push(item);
		});
		return result;
	}

	var pData = parsePieChartData(productosMasVendidosData);

	new Chartkick.PieChart("pie_chart_mas_vendidos", pData, {
		download : "pie",
		donut : true
	});

	var pData = parsePieChartData(productosMenosVendidosData);
	new Chartkick.PieChart("pie_chart_menos_vendidos", pData, {
		download : "pie",
		donut : true
	});

});