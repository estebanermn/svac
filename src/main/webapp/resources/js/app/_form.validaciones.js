function refreshFormValidations() {

	// See: https://github.com/nosir/cleave.js/blob/master/doc/options.md

	// Para códigos
	$('.cleave-codigo').toArray().forEach(function(field) {
		new Cleave(field, {
			numericOnly : true,
			delimiter : ' ',
			blocks : [ 5, 4 ]
		});
	});

	// Para números enteros
	$('.cleave-numero').toArray().forEach(function(field) {
		new Cleave(field, {
			numericOnly : true,
			blocks : [ 5 ]
		});
	});

	// Para precios
	$('.cleave-precio').toArray().forEach(function(field) {
		new Cleave(field, {
			numeral : true,
			numeralDecimalScale : 4
		});
	});

}
