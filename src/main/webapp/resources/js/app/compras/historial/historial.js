$(document).ready(function() {

	var dtColumns = [ {
		data : "ordenCompraNumero"
	}, {
		data : "proveedor"
	}, {
		data : "fecha"
	}, {
		data : "estado"
	}, {
		data : "id",
		'orderable' : false,
		'render' : function(data, type, full, meta) {

			var url = Config.url + '/web/compras/ingresos/' + data;
			var html = renderOpen();
			html += renderLookItemsButton(data, url);
			html += renderClose();
			return html;
		}
	} ];

	var dt = createDataTable('#data-table-default', dtColumns);

	function updateDataTable(tipoHistorial) {
		$('#data-table-default').dataTable().fnClearTable();
		$('#data-table-default').dataTable().fnDraw();

		var url = generateHistorialUrl('compras', tipoHistorial)

		_getAPICall(url, function(response) {
			if (response.length > 0) {
				$('#data-table-default').dataTable().fnAddData(response);
			}
		});
	}

	var tipoHistorial = Config.tipoHistorial;

	updateDataTable(tipoHistorial);

});