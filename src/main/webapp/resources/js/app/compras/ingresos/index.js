$(document).ready(function() {

	var dtColumns = [ {
		data : "ordenCompraNumero"
	}, {
		data : "proveedor"
	}, {
		data : "fecha"
	}, {
		data : "estado"
	}, {
		data : "id",
		'orderable' : false,
		'render' : function(data, type, full, meta) {

			var url = Config.url + '/web/compras/ingresos/' + data;
			var html = renderOpen() + renderLookItemsButton(data, url) + renderClose();
			return html;
		}
	} ];

	var dt = createDataTable('#data-table-default', dtColumns);

	function updateDataTable() {
		$('#data-table-default').dataTable().fnClearTable();
		$('#data-table-default').dataTable().fnDraw();

		getAPICall(null, function(response) {
			if (response.length > 0) {
				$('#data-table-default').dataTable().fnAddData(response);
			}
		});
	}

	updateDataTable();

});