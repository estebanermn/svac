$(document).ready(function() {

	function disableHtmlElements() {
		$("#btnSearchProveedor").prop("disabled", true);
		$("#comprobanteTipo").prop("disabled", true)
		$("#btnSearchArticulo").prop("disabled", true);
		$("#btnSearchArticulo").hide();
		$(".sum-cantidad").prop("disabled", true);

		if ($("#btnDeleteTR")) {
			$("#btnDeleteTR").remove();
		}
	}

	function generateUrl(recurso, entidadId) {
		var url = Config.url;

		if (typeof entidadId == "undefined" || entidadId == null) {
			return url + '/api/' + recurso;
		} else {
			return url + '/api/' + recurso + '/' + entidadId;
		}
	}

	function generateUrlEstado(recurso, entidadId, estado) {
		// URL: /recurso/{entidadId}/status/{estado}
		var url = generateUrl(recurso, entidadId);
		url = url + '/status/' + estado;
		return url;
	}

	function _postAPICall(url, data, callback) {

		$.ajax({
			type : 'POST',
			url : url,
			data : JSON.stringify(data),
			crossDomain : true,
			contentType : "application/json; charset=utf8",
			// dataType : "json",
			async : true,
			success : function(response) {
				callback(response);
			},
			error : function(data) {
				console.log(data);
			}
		});
	}

	compraDetalleData.forEach(function(item) {

		var entidadId = item.articuloId;
		var producto = getObjectById(articuloData, entidadId);

		var entidad = {
			id : item.articuloId,
			nombre : producto.nombre,
			descripcion : producto.descripcion,
			cantidad : item.cantidad,
			precioProveedor : item.precio
		}

		var row = addDetalleCompraHTML(entidad);

		$('#data-table-default> tbody:last').append(row);

		disableHtmlElements();
		updateTotales();

	});

	function getFormToJSON() {

		var obj = {};

		obj.clienteId = $.trim($("#clienteId").val());
		obj.fecha = Date.now();
		obj.subtotal = $("#subtotal").val();
		obj.igv = $("#igv").val();
		obj.total = $("#total").val();
		obj.empleadoId = $.trim($("#empleadoId").val());
		obj.tipoDocumento = $('#tipoDocumento option:selected').val();

		obj.items = [];

		$('#data-table-default tbody tr').each(function() {

			var item = {};

			var precio = $(this).find("#precio").val();
			precio = Number(precio.replace(/[^0-9.-]+/g, ""));

			item.articuloId = $(this).find("#articuloId").val();
			item.cantidad = $(this).find("#cantidad").val();
			item.precio = precio;
			item.descuento = 0;
			item.total = parseFloat(item.cantidad * precio);

			// console.log(item);
			obj.items.push(item);
		});

		return obj;
	}

	function updateTotales() {

		var formToJSON = getFormToJSON();
		var subtotal = 0;

		// console.log(formToJSON);

		formToJSON.items.forEach(function(item) {

			var row = item.cantidad * item.precio;
			subtotal += row;
		});

		var impuesto = (Config.impuesto ? Config.impuesto : 18);
		var igv = (subtotal * impuesto) / 100;
		var total = parseFloat(subtotal + igv);

		$("#subtotal").val(subtotal);
		$("#igv").val(igv);
		$("#total").val(total);

		refreshFormValidations();
	}

	/** ****************** Buttons ******************* */

	// Aprobado
	$('body').on('click', '#btnAprobar', function(e) {
		e.preventDefault();

		var ventaId = $(this).data('id');
		var data = {
			documentoVentaId : ventaId
		};

		var url = generateUrlEstado('compras', ventaId, 'aprobado');

		_postAPICall(url, data, function() {
			alert("La orden ha sido aprobada.");
			location.reload();
		})
	});

	// Anular
	$('body').on('click', '#btnAnular', function(e) {
		e.preventDefault();

		var ventaId = $(this).data('id');
		var data = {
			documentoVentaId : ventaId
		};

		var url = generateUrlEstado('compras', ventaId, 'anulado');

		_postAPICall(url, data, function() {
			alert("La orden de compra ha sido anulado.");
			location.reload();
		})
	});
	
	// Pagado
	$('body').on('click', '#btnPagar', function(e) {
		e.preventDefault();

		var ventaId = $(this).data('id');
		var data = {
			documentoVentaId : ventaId
		};

		var url = generateUrlEstado('compras', ventaId, 'pagado ');

		_postAPICall(url, data, function() {
			alert("La orden de compra ha sido pagada.");
			location.reload();
		})
	});
	
	// btnRecibido
	$('body').on('click', '#btnRecibido', function(e) {
		e.preventDefault();

		var ventaId = $(this).data('id');
		var data = {
			documentoVentaId : ventaId
		};

		var url = generateUrlEstado('compras', ventaId, 'recibido ');

		_postAPICall(url, data, function() {
			alert("La orden de compra ha sido recibida.");
			location.reload();
		})
	});

});