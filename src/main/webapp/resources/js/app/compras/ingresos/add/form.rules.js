var formRules = {

	proveedorNombre : {
		required : true
	},

	comprobanteSerie : {
		required : true,
		minlength : 8,
		maxlength : 8
	},

	comprobanteNumero : {
		required : true,
		minlength : 8,
		maxlength : 8
	},

};
