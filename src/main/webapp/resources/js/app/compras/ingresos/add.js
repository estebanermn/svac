$(document).ready(function() {

	/*--------------- Modal: Proveedor ---------------*/

	var dtColumns2 = [ {
		data : "id",
		'orderable' : false,
		'render' : function(data, type, full, meta) {
			var html = renderCheckButton(data);
			return html;
		}
	}, {
		data : "nombre"
	}, {
		data : "documento"
	}, {
		data : "numeroDocumento"
	}, {
		data : "email"
	}, {
		data : "numeroCuenta"
	} ];

	var dt2 = createDataTable("#data-table-proveedor", dtColumns2);

	$('body').on('click', '#btnSearchProveedor', function(e) {

		e.preventDefault();
		$('#modal-proveedor').modal('show');
		updateDataTable('#data-table-proveedor', proveedorData)
	});

	// Add Item
	$('#data-table-proveedor').on('click', '#btnCheckRow', function(e) {

		var entidadId = $(this).data('id');
		var entidad = getObjectById(proveedorData, entidadId);

		$('#proveedorId').val(entidad.id);
		$('#proveedorNombre').val(entidad.nombre);
		$('#modal-proveedor').modal('hide');

	}); // add item

	/*--------------- Modal: Articulo ---------------*/

	var dtColumns3 = [ {
		data : "id",
		'orderable' : false,
		'render' : function(data, type, full, meta) {
			var html = renderCheckButton(data);
			return html;
		}
	}, {
		data : "codigo"
	}, {
		data : "categoria"
	}, {
		data : "unidad"
	}, {
		data : "nombre"
	}, {
		data : "descripcion"
	} ];

	var dt2 = createDataTable("#data-table-articulo", dtColumns3);

	$('body').on('click', '#btnSearchArticulo', function(e) {

		e.preventDefault();
		$('#modal-articulo').modal('show');
		updateDataTable('#data-table-articulo', articuloData)
	});

	// Add Item
	$('#data-table-articulo').on('click', '#btnCheckRow', function(e) {

		var entidadId = $(this).data('id');
		var entidad = getObjectById(articuloData, entidadId);

		var row = addDetalleCompraHTML(entidad);

		console.log(entidad);

		$('#data-table-default> tbody:last').append(row);

		refreshFormValidations();

	}); // add item

	/*--------------- Compra: Ingreso ---------------*/

	$("#formModal").validate({
		rules : formRules
	});

	$("#formModal").submit(function(e) {
		e.preventDefault();
	});

	function getFormToJSON() {

		var obj = {};

		obj.empleadoId = $.trim($("#empleadoId").val());
		obj.proveedorId = $.trim($("#proveedorId").val());
		obj.items = [];

		$('#data-table-default tbody tr').each(function() {
			var item = {};

			item.articuloId = $(this).find("#articuloId").val();
			item.cantidad = $(this).find("#cantidad").val();

			var precio = $(this).find("#precio").val();
			item.precio = Number(precio.replace(/[^0-9.-]+/g, ""));

			// console.log(item);
			obj.items.push(item);
		});

		return obj;
	}

	// Delete TR
	$('body').on('click', '#btnDeleteTR', function(e) {

		$(this).closest('tr').remove();
		return false;
	});

	function updateTotales() {

		var formToJSON = getFormToJSON();
		var subtotal = 0;

		// console.log(formToJSON);

		formToJSON.items.forEach(function(item) {

			var row = item.cantidad * item.precio;
			subtotal += row;
		});

		var impuesto = (Config.impuesto ? Config.impuesto : 18);
		var igv = (subtotal * impuesto) / 100;
		var total = parseFloat(subtotal + igv);

		$("#subtotal").val(subtotal);
		$("#igv").val(igv);
		$("#total").val(total);

		refreshFormValidations();
	}

	// Calcular montos
	$('body').on('change keyup', '.sum-cantidad', function(e) {
		updateTotales();
	});

	// Guardar
	$('body').on('click', '#btnSave', function(e) {

		var isValid = $("#formModal").valid();

		if (!isValid) {
			return;
		}

		var formToJSON = getFormToJSON();

		if (getFormToJSON.proveedorId === "") {
			alert("Error: Se debe seleccionar un proveedor.")
			return;
		}

		if (formToJSON.items.length == 0) {
			alert("Error: Se debe ingresar al menos un producto.")
			return;
		}

		postAPICall(null, formToJSON, function(response) {
			alert("Se ingreso la orden de compra correctamente.");
			redirect('/web/compras/ingresos/' + response.id);
		});

	});

});