function addDetalleCompraHTML(entidad) {

	// console.log(entidad);

	var articuloId = '<input type="hidden" id="articuloId" value="'
			+ entidad.id + '">';
	var codigo = '<input type="text" id="codigo" class="form-control cleave-codigo" required>';
	var serie = '<input type="text" id="serie" class="form-control required">';
	var descripcion = '<input type="text" id="descripcion" class="form-control" value="'
			+ entidad.descripcion + '" required>';
	var cantidad = '<input type="text" id="cantidad" class="form-control cleave-numero sum-cantidad" required>';

	if (typeof (entidad.cantidad) != 'undefined') {
		cantidad = '<input type="text" id="cantidad" class="form-control cleave-numero sum-cantidad" value="'
				+ entidad.cantidad + '" disabled>';
	}

	var precio = '<input type="text" id="precio" class="form-control cleave-precio sum-precio" value="'
			+ entidad.precioProveedor + '" required disabled>';
	var opciones = '<button id="btnDeleteTR" class="btn btn-danger"> <i class="fa fa-times"></i></button>';

	var html = '<tr>';
	html += '<td>' + entidad.nombre + '</td>';
	html += '<td>' + entidad.descripcion + '</td>';
	html += '<td>' + cantidad + '</td>';
	html += '<td>' + precio + '</td>';
	html += '<td>' + articuloId + opciones + '</td>';
	html += '</tr>';

	return html;
}
