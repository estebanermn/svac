$(document).ready(function() {

	var formRules = {
		nombre : {
			required : true,
			minlength : 4,
			maxlength : 10
		},
		prefijo: {
			required : true,
			minlength : 2,
			maxlength : 4
	}
	
	};

	function resetForm() {
		$("#entidadId").val('');
		$("#nombre").val('');
		$("#prefijo").val('');
		$("#estado").val('');
		loadData();

	}

	function populateForm(response) {
		$("#entidadId").val(response.id);
		$("#nombre").val(response.nombre);
		$("#prefijo").val(response.prefijo);
		$("#estado").val(response.estado);
		loadData();
		$('#categoria_select').val(response.categoriaId).trigger('change');
	}

	
	
	var dtColumns = [{
		data : "codigo"
	}, {
		data : "categoria"
	}, {
		data : "nombre"
	}, {
		data : "prefijo"
	},{
		data : "estado",
		'orderable' : false,
		'render' : function(data, type, full, meta) {
			return (data == 1 ? "Activo" : "Inactivo");
		}
	}, {
		data : "id",
		'orderable' : false,
		'render' : function(data, type, full, meta) {
			var html = renderEditButton(data) + renderChangeButton(full);
			return html;
		}
	} ];

	var dt = $("#data-table-default").DataTable({
		responsive : true,
		columns : dtColumns,
		"language" : {
			"url" : "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
		}
	});

	function updateDataTable() {

		$('#data-table-default').dataTable().fnClearTable();
		$('#data-table-default').dataTable().fnDraw();

		$.ajax({
			type : "GET",
			url : Config.url + '/api/' + Config.recurso,
			crossDomain : true,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			async : true,
			success : function(response) {
				$('#data-table-default').dataTable().fnAddData(response);
			}
		});

	}

	updateDataTable();
	
	
	function loadData() {

		var data = prepareSelect2Data(categoriaData);

		$('#categoria_select').select2({
			data : data
		});
	}

	$("#formModal").validate({
		rules : formRules
	});


	$("#formModal").submit(function(e) {
		e.preventDefault();
	});

	// Nuevo
	$('body').on('click', '#btnNewRow', function(e) {

		resetForm();
		$('#modal-dialog').modal('show');
	});

	// Editar
	$('#data-table-default').on('click', '#btnEditRow', function(e) {

		resetForm();

		var entidadId = $(this).data('id');
		
		getAPICall(entidadId, function(response) {
			populateForm(response)
			$('#modal-dialog').modal();
		});	

	}); // editar

	// Guardar
	$('body').on('click', '#btnModalSave', function(e) {
		var isValid = $("#formModal").valid();

		if (!isValid) {
			return;
		}

		var entidadId = $.trim($("#entidadId").val());
		var formToJSON = {};
		
		var categoriaId = $('#categoria_select').val();
		var nombre = $.trim($("#nombre").val());
		var prefijo = $.trim($("#prefijo").val());
		var estado = $.trim($("#estado").val());
		
		if (categoriaId) {
			formToJSON['categoriaId'] = categoriaId;
		}
		if (nombre) {
			formToJSON['nombre'] = nombre;
		}
		if (prefijo) {
			formToJSON['prefijo'] = prefijo;
		}
		if (estado) {
			formToJSON['estado'] = estado;
		}

		if (entidadId.length == 0 ) {
			postAPICall(entidadId, formToJSON, function(response){
				$("#modal-dialog").modal('hide');
				updateDataTable();
			});
			
		} else {			
			putAPICall(entidadId, formToJSON, function(response){
				$("#modal-dialog").modal('hide');
				updateDataTable();
			});
		}

	}); // guardar

	// Cambiar estado
	$('#data-table-default').on('click', '#btnChangeRow', function(e) {

		var entidadId = $(this).data('id');

		var estado = $(this).data('estado');
		estado = (estado == 0 ? "true" : "false");

		changeEstadoAPICall(entidadId, estado, function(response) {
			$("#modal-dialog").modal('hide');
			updateDataTable();
		});

	}); // Cambiar estado

});