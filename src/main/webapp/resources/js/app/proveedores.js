$(document).ready(function() {

	var cleaveNumeroDocumento = new Cleave('#numeroDocumento', {
		blocks : [ 11 ],
		numericOnly : true
	});

	var cleaveNumeroCuenta = new Cleave('#numeroCuenta', {
		creditCard : true,
		creditCardStrictMode : true
	});

	var cleaveTelefono = new Cleave('#telefono', {
		blocks : [ 3, 3, 3 ],
		numericOnly : true
	});

	var formRules = {
		nombre : {
			required : true,
			minlength : 4,
			maxlength : 45
		},
		numeroDocumento : {
			required : true,
			minlength : 8,
			maxlength : 11
		},

		departamento : {
			required : true,
			minlength : 3,
			maxlength : 20
		},

		provincia : {
			required : true,
			minlength : 3,
			maxlength : 45
		},

		distrito : {
			required : true,
			minlength : 3,
			maxlength : 45
		},

		direccion : {
			required : true,
			minlength : 3,
			maxlength : 45
		},

		telefono : {
			required : true,
			minlength : 7,
			maxlength : 11
		},

		numeroCuenta : {
			required : true,
			maxlength : 20
		}

	};

	function resetForm() {
		$("#entidadId").val('');
		$("#nombre").val('');
		$("#numeroDocumento").val('');
		$("#departamento").val('');
		$("#provincia").val('');
		$("#distrito").val('');
		$("#direccion").val('');
		$("#telefono").val('');
		$("#email").val('');
		$("#numeroCuenta").val('');
		$("#estado").val('');

	}

	function populateForm(response) {
		$("#entidadId").val(response.id);
		$("#nombre").val(response.nombre);
		$("#documento").val(response.documento);
		$("#numeroDocumento").val(response.numeroDocumento);
		$("#departamento").val(response.departamento);
		$("#provincia").val(response.provincia);
		$("#distrito").val(response.distrito);
		$("#direccion").val(response.direccion);
		$("#telefono").val(response.telefono);
		$("#email").val(response.email);
		$("#numeroCuenta").val(response.numeroCuenta);
		$("#estado").val(response.estado);

		populateSelect(response);
	}

	function populateSelect(response) {
		var value = response.documento;
		var items = [ 'DNI', 'RUC' ];

		$('#documento_select').html('');

		$.each(items, function(i, item) {
			var selected = (item == value ? true : false);
			$('#documento_select').append($('<option>', {
				value : item,
				text : item,
				selected : selected
			}));
		});
	}

	var dtColumns = [ {
		data : "codigo"
	}, {
		data : "nombre"
	}, {
		data : "documento"
	}, {
		data : "numeroDocumento"
	}, {
		data : "email"
	}, {
		data : "telefono"
	}, {
		data : "direccion"
	}, {
		data : "estado",
		'orderable' : false,
		'render' : function(data, type, full, meta) {
			return (data == 1 ? "Activo" : "Inactivo");
		}
	}, {
		data : "id",
		'orderable' : false,
		'render' : function(data, type, full, meta) {
			var html = renderEditButton(data) + renderChangeButton(full);
			return html;
		}
	} ];

	var dt = $("#data-table-default").DataTable({
		responsive : true,
		columns : dtColumns,
		"language" : {
			"url" : "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
		}
	});

	function updateDataTable() {

		$('#data-table-default').dataTable().fnClearTable();
		$('#data-table-default').dataTable().fnDraw();

		$.ajax({
			type : "GET",
			url : Config.url + '/api/' + Config.recurso,
			crossDomain : true,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			async : true,
			success : function(response) {
				$('#data-table-default').dataTable().fnAddData(response);
			}
		});

	}

	updateDataTable();

	$("#formModal").validate({
		rules : formRules
	});

	$("#formModal").submit(function(e) {
		e.preventDefault();
	});

	// Nuevo
	$('body').on('click', '#btnNewRow', function(e) {

		resetForm();
		$('#modal-dialog').modal('show');
	});

	// Editar
	$('#data-table-default').on('click', '#btnEditRow', function(e) {

		resetForm();

		var entidadId = $(this).data('id');

		getAPICall(entidadId, function(response) {
			populateForm(response)
			$('#modal-dialog').modal();
		});

	}); // editar

	// Guardar
	$('body').on('click', '#btnModalSave', function(e) {
		var isValid = $("#formModal").valid();

		if (!isValid) {
			return;
		}

		var entidadId = $.trim($("#entidadId").val());
		var formToJSON = {};

		var nombre = $.trim($("#nombre").val());
		var documento = $('#documento_select option:selected').val();
		var numeroDocumento = $.trim($("#numeroDocumento").val());
		var departamento = $.trim($("#departamento").val());
		var provincia = $.trim($("#provincia").val());
		var distrito = $.trim($("#distrito").val());
		var direccion = $.trim($("#direccion").val());
		var telefono = $.trim($("#telefono").val());
		var email = $.trim($("#email").val());
		var numeroCuenta = $.trim($("#numeroCuenta").val());
		var estado = $.trim($("#estado").val());

		if (nombre) {
			formToJSON['nombre'] = nombre;
		}
		if (documento) {
			formToJSON['documento'] = documento;
		}
		if (numeroDocumento) {
			formToJSON['numeroDocumento'] = numeroDocumento;
		}
		if (departamento) {
			formToJSON['departamento'] = departamento;
		}
		if (provincia) {
			formToJSON['provincia'] = provincia;
		}
		if (distrito) {
			formToJSON['distrito'] = distrito;
		}
		if (direccion) {
			formToJSON['direccion'] = direccion;
		}
		if (telefono) {
			formToJSON['telefono'] = telefono;
		}
		if (email) {
			formToJSON['email'] = email;
		}
		if (numeroCuenta) {
			formToJSON['numeroCuenta'] = numeroCuenta;
		}

		if (estado) {
			formToJSON['estado'] = estado;
		}

		console.log(formToJSON);

		if (entidadId.length == 0) {
			postAPICall(entidadId, formToJSON, function(response) {
				$("#modal-dialog").modal('hide');
				updateDataTable();
			});

		} else {
			putAPICall(entidadId, formToJSON, function(response) {
				$("#modal-dialog").modal('hide');
				updateDataTable();
			});
		}

	}); // guardar

	// Cambiar estado
	$('#data-table-default').on('click', '#btnChangeRow', function(e) {

		var entidadId = $(this).data('id');

		var estado = $(this).data('estado');
		estado = (estado == 0 ? "true" : "false");

		changeEstadoAPICall(entidadId, estado, function(response) {
			$("#modal-dialog").modal('hide');
			updateDataTable();
		});

	}); // Cambiar estado

});