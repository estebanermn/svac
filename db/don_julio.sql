-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 12, 2019 at 02:05 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `don_julio`
--

DELIMITER $$
--
-- Procedures
--
DROP PROCEDURE IF EXISTS `prcArticuloInsert`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `prcArticuloInsert` (IN `CATEGORIA` INT(11), IN `UNIDAD` INT(11), IN `NOMBRE` VARCHAR(45), IN `DESCRIPCION` VARCHAR(200), IN `PRECIO` DOUBLE, IN `PRECIOPROVEEDOR` DOUBLE)  BEGIN	
	DECLARE COD CHAR(7);

	SET COD = (SELECT CONCAT('ART-', SUBSTRING(CONCAT ('00', SUBSTRING(MAX(codigo),-3) + 1),-3)) FROM articulo);
   
INSERT INTO `articulo` ( `idCategoria`, `idUnidad`, `codigo`, `nombre`, `descripcion`, `precio`, `precioProveedor`) VALUES (CATEGORIA,UNIDAD,COD,NOMBRE,DESCRIPCION ,PRECIO, PRECIOPROVEEDOR);

END$$

DROP PROCEDURE IF EXISTS `prcArticuloSelect`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `prcArticuloSelect` ()  BEGIN

SELECT
    a.*,
    c.nombre AS categoria,
    u.nombre AS unidad
FROM
    articulo a
JOIN categoria c ON
    a.idCategoria = c.idCategoria
JOIN unidad_medida u ON
    a.idUnidad = u.idUnidad;

END$$

DROP PROCEDURE IF EXISTS `prcCategoriaInsert`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `prcCategoriaInsert` (IN `NOMBRE` VARCHAR(45))  BEGIN	
	DECLARE COD CHAR(7);

	SET COD = (SELECT CONCAT('CAT-', SUBSTRING(CONCAT ('00', SUBSTRING(MAX(codigo),-3) + 1),-3)) FROM categoria);
   
INSERT INTO `categoria` ( codigo ,nombre) VALUES (COD, NOMBRE);

END$$

DROP PROCEDURE IF EXISTS `prcClienteInsert`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `prcClienteInsert` (IN `NOMBRE` VARCHAR(45), IN `TIPODOCUMENTO` VARCHAR(45), IN `NUMDOCUMENTO` VARCHAR(15), IN `DEPARTAMENTO` VARCHAR(45), IN `PROVINCIA` VARCHAR(45), IN `DISTRITO` VARCHAR(45), IN `DIRECCION` VARCHAR(45), IN `TELEFONO` VARCHAR(11), IN `EMAIL` VARCHAR(45), IN `NUMCUENTA` VARCHAR(40))  BEGIN	
	DECLARE COD CHAR(7);

	SET COD = (SELECT CONCAT('CLI-', SUBSTRING(CONCAT ('00', SUBSTRING(MAX(codigo),-3) + 1),-3)) FROM cliente);
   
    INSERT INTO `cliente` (`codigo`, `tipo_persona`, `nombre`, `tipo_documento`, `numero_documento`,`departamento`, 
    `provincia`, `distrito`, `direccion`,`telefono`, `email`, `numero_cuenta`) 
    VALUES (COD,'CLIENTE',NOMBRE,TIPODOCUMENTO, NUMDOCUMENTO,DEPARTAMENTO 
    ,PROVINCIA,DISTRITO,DIRECCION,TELEFONO,EMAIL,NUMCUENTA);

    END$$

DROP PROCEDURE IF EXISTS `prcClienteUpdate`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `prcClienteUpdate` (IN `ID` INT(11), IN `NOMBRE` VARCHAR(45), IN `TIPODOCUMENTO` VARCHAR(10), IN `NUMDOCUMENTO` VARCHAR(15), IN `DEPARTAMENTO` VARCHAR(45), IN `PROVINCIA` VARCHAR(45), IN `DISTRITO` VARCHAR(45), IN `DIRECCION` VARCHAR(45), IN `TELEFONO` VARCHAR(11), IN `EMAIL` VARCHAR(45), IN `NUMCUENTA` VARCHAR(20), IN `ESTADO` INT(11))  BEGIN
    UPDATE
        cliente
    SET
        `nombre` = `NOMBRE`,
        `tipo_documento` = `TIPODOCUMENTO`,
        `numero_documento` = `NUMDOCUMENTO`,
        `departamento` = `DEPARTAMENTO`,
        `provincia` = `PROVINCIA`,
        `distrito` = `DISTRITO`,
        `direccion` = `DIRECCION`,
        `telefono` = `TELEFONO`,
        `email` = `EMAIL`,
        `numero_cuenta` = `NUMCUENTA`,
        `estado` = `ESTADO`
    WHERE
        idCliente = `ID` ;
END$$

DROP PROCEDURE IF EXISTS `prcCompraDetalleInsert`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `prcCompraDetalleInsert` (IN `compraId` INT(11), IN `articuloId` INT(11), IN `codigo` VARCHAR(45), IN `serie` VARCHAR(45), IN `descripcion` VARCHAR(200), IN `stockIngreso` INT(11), IN `precioCompra` DECIMAL(8,2), IN `precioVentaDistribuidor` DECIMAL(8,2), IN `precioVentaPublico` DECIMAL(8,2))  BEGIN

INSERT INTO `detalle_ingreso` (`idIngreso`, `idArticulo`, `codigo`, `serie`, `descripcion`, `stock_ingreso`, `precio_compra`, `precio_ventadistribuidor`, `precio_ventapublico`) VALUES (compraId, articuloId, codigo, serie, descripcion, stockIngreso, precioCompra, precioVentaDistribuidor, precioVentaPublico);
    
    
END$$

DROP PROCEDURE IF EXISTS `prcCompraInsert`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `prcCompraInsert` (IN `USUARIO` INT(11), IN `PROVEEDOR` INT(11), IN `comprobanteTipo` VARCHAR(45), IN `comprobanteSerie` VARCHAR(10), IN `comprobanteNumero` VARCHAR(10), IN `fecha` DATE, IN `impuesto` DECIMAL(10), IN `total` DECIMAL(10))  BEGIN

DECLARE COD CHAR(7);

	SET COD = (SELECT CONCAT('ING-', SUBSTRING(CONCAT ('00', SUBSTRING(MAX(codigo),-3) + 1),-3)) FROM ingreso);


INSERT INTO `ingreso` (`codigo`, `idUsuario`, `idSucursal`, `idProveedor`, `tipo_comprobante`, `serie_comprobante`, `numero_comprobante`, `fecha`, `impuesto`, `total`, `estado`) VALUES
(COD, USUARIO, 1, PROVEEDOR, comprobanteTipo, comprobanteSerie, comprobanteNumero, fecha, impuesto, total, 1);
COMMIT;


END$$

DROP PROCEDURE IF EXISTS `prcEmpleadoInsert`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `prcEmpleadoInsert` (IN `APELLIDOS` VARCHAR(45), IN `NOMBRE` VARCHAR(45), IN `DNI` VARCHAR(11), IN `DIRECCION` VARCHAR(45), IN `TELEFONO` VARCHAR(11), IN `EMAIL` VARCHAR(70), IN `FECHA` DATE)  BEGIN	

DECLARE COD CHAR(7);

	SET COD = (SELECT CONCAT('EMP-', SUBSTRING(CONCAT ('00', SUBSTRING(MAX(codigo),-3) + 1),-3)) FROM empleado);



INSERT INTO `empleado` (`codigo`, `apellidos`, `nombre`, `dni`, `direccion`, `telefono`, `email`, `fecha_nacimiento`)
 VALUES ( COD, APELLIDOS, NOMBRE, DNI, DIRECCION, TELEFONO, EMAIL, FECHA);


END$$

DROP PROCEDURE IF EXISTS `prcEmpleadoUpdate`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `prcEmpleadoUpdate` (IN `ID` INT(11), IN `APELLIDOS` VARCHAR(45), IN `NOMBRE` VARCHAR(45), IN `DNI` VARCHAR(11), IN `DIRECCION` VARCHAR(45), IN `TELEFONO` VARCHAR(11), IN `EMAIL` VARCHAR(70), IN `FECHA` DATE, IN `ESTADO` INT(1))  BEGIN	

UPDATE `empleado` SET `apellidos` = APELLIDOS, `nombre` = NOMBRE, `dni` = DNI, `direccion` = DIRECCION, `telefono` = TELEFONO, `email` = EMAIL, `fecha_nacimiento` = FECHA, `estado` = ESTADO WHERE `empleado`.`idEmpleado` = ID;

END$$

DROP PROCEDURE IF EXISTS `prcProveedorInsert`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `prcProveedorInsert` (IN `NOMBRE` VARCHAR(45), IN `TIPODOCUMENTO` VARCHAR(45), IN `NUMDOCUMENTO` VARCHAR(15), IN `DEPARTAMENTO` VARCHAR(45), IN `PROVINCIA` VARCHAR(45), IN `DISTRITO` VARCHAR(45), IN `DIRECCION` VARCHAR(45), IN `TELEFONO` VARCHAR(11), IN `EMAIL` VARCHAR(45), IN `NUMCUENTA` VARCHAR(40))  BEGIN	
	DECLARE COD CHAR(7);

	SET COD = (SELECT CONCAT('PRV-', SUBSTRING(CONCAT ('00', SUBSTRING(MAX(codigo),-3) + 1),-3)) FROM proveedor);
   
    INSERT INTO `proveedor` (`codigo`, `tipo_persona`, `nombre`, `tipo_documento`, `numero_documento`,`departamento`, 
    `provincia`, `distrito`, `direccion`,`telefono`, `email`, `numero_cuenta`) 
    VALUES (COD, 'PROVEEDOR', NOMBRE,TIPODOCUMENTO, NUMDOCUMENTO,DEPARTAMENTO 
    ,PROVINCIA,DISTRITO,DIRECCION,TELEFONO,EMAIL,NUMCUENTA);

    END$$

DROP PROCEDURE IF EXISTS `prcProveedorUpdate`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `prcProveedorUpdate` (IN `ID` INT(11), IN `NOMBRE` VARCHAR(45), IN `TIPODOCUMENTO` VARCHAR(45), IN `NUMDOCUMENTO` VARCHAR(15), IN `DEPARTAMENTO` VARCHAR(45), IN `PROVINCIA` VARCHAR(45), IN `DISTRITO` VARCHAR(45), IN `DIRECCION` VARCHAR(45), IN `TELEFONO` VARCHAR(11), IN `EMAIL` VARCHAR(45), IN `NUMCUENTA` VARCHAR(40), IN `ESTADO` INT(11))  BEGIN
    UPDATE
        proveedor
    SET
        `nombre` = `NOMBRE`,
        `tipo_documento` = `TIPODOCUMENTO`,
        `numero_documento` = `NUMDOCUMENTO`,
        `departamento` = `DEPARTAMENTO`,
        `provincia` = `PROVINCIA`,
        `distrito` = `DISTRITO`,
        `direccion` = `DIRECCION`,
        `telefono` = `TELEFONO`,
        `email` = `EMAIL`,
        `numero_cuenta` = `NUMCUENTA`,
        `estado` = `ESTADO`
    WHERE
        idProveedor = `ID`;
END$$

DROP PROCEDURE IF EXISTS `prcUnidadInsert`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `prcUnidadInsert` (IN `NOMBRE` VARCHAR(45), IN `PREFIJO` VARCHAR(5), IN `CATEGORIA` INT)  BEGIN	
	DECLARE COD CHAR(7);

	SET COD = (SELECT CONCAT('UNI-', SUBSTRING(CONCAT ('00', SUBSTRING(MAX(codigo),-3) + 1),-3)) FROM `unidad_medida`);
   
INSERT INTO `unidad_medida` ( idCategoria ,codigo,nombre, prefijo) VALUES (CATEGORIA,COD, NOMBRE ,PREFIJO);

END$$

DROP PROCEDURE IF EXISTS `prcUnidadSelect`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `prcUnidadSelect` ()  BEGIN

SELECT
    u.*,
    c.nombre AS categoria
FROM
    unidad_medida u
JOIN categoria c ON
    u.idCategoria = c.idCategoria;


END$$

DROP PROCEDURE IF EXISTS `sp_Articulo_Aumentar_Stock`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Articulo_Aumentar_Stock` (IN `ARTICULOID` INT, IN `AUMENTAR` INT)  BEGIN	
	DECLARE NUEVO_STOCK INT;

	SET NUEVO_STOCK = (SELECT `stock` FROM `articulo` WHERE `idArticulo` = ArticuloId ) + AUMENTAR;
    
    UPDATE `articulo` SET `stock` = NUEVO_STOCK WHERE `articulo`.`idArticulo` = ARTICULOID;

END$$

DROP PROCEDURE IF EXISTS `sp_Articulo_Descontar_Stock`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Articulo_Descontar_Stock` (IN `ARTICULOID` INT, IN `DESCONTAR` INT)  BEGIN	
	DECLARE NUEVO_STOCK INT;

	SET NUEVO_STOCK = (SELECT `stock` FROM `articulo` WHERE `idArticulo` = ArticuloId ) - DESCONTAR;
    -- nuevo_stock = nuevo_stock - descontar;
    
    UPDATE `articulo` SET `stock` = NUEVO_STOCK WHERE `articulo`.`idArticulo` = ARTICULOID;

END$$

DROP PROCEDURE IF EXISTS `sp_Auditoria_Insert`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Auditoria_Insert` (IN `OrdenCompra` INT, IN `DocumentoVenta` INT, IN `FECHA` DATE, IN `EmpleadoID` INT, IN `ESTADO` VARCHAR(120), IN `DESCRIPCION` VARCHAR(120))  BEGIN	

INSERT INTO `auditoria` (`idOrdenCompra`, `idDocumentoVenta`, `fecha`, `idEmpleado`, `estado`, `descripcion`) 
VALUES ( OrdenCompra,DocumentoVenta, FECHA, EmpleadoID, ESTADO, DESCRIPCION);

END$$

DROP PROCEDURE IF EXISTS `sp_Calificacion_calcularPromedio`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Calificacion_calcularPromedio` (IN `EmpleadoId` INT)  BEGIN

-- PROMEDIO = SUMA DE NOTAS / Cantidad

DECLARE PROMEDIO DOUBLE;


SET PROMEDIO = ( SELECT SUM(calificacion) / COUNT(idCalificacion) AS promedio FROM `calificacion` WHERE `idEmpleado` = EmpleadoId );


UPDATE `empleado` SET `calificacion` = PROMEDIO WHERE `empleado`.`idEmpleado` = EmpleadoId;

                
END$$

DROP PROCEDURE IF EXISTS `sp_DocumentoVentaDetalle_Insert`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_DocumentoVentaDetalle_Insert` (IN `DocumentoVenta` INT, IN `ARTICULO` INT, IN `CANTIDAD` INT, IN `PRECIO` DOUBLE, IN `DESCUENTO` DOUBLE, IN `TOTAL` DOUBLE)  BEGIN	


INSERT INTO `documento_venta_detalle` (`idDocumentoVenta`, `idArticulo`, `cantidad`, `precio`, `descuento`, `total`) 
VALUES (DocumentoVenta, ARTICULO, CANTIDAD, PRECIO, DESCUENTO, TOTAL);

END$$

DROP PROCEDURE IF EXISTS `sp_DocumentoVentaDetalle_Select`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_DocumentoVentaDetalle_Select` (IN `documentoVentaId` INT)  BEGIN	

SELECT d.* FROM documento_venta_detalle AS d WHERE d.idDocumentoVenta = documentoVentaId;
                
END$$

DROP PROCEDURE IF EXISTS `sp_DocumentoVenta_Id`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_DocumentoVenta_Id` (IN `documentoVentaId` INT)  BEGIN	

SELECT
    d.*,
    c.nombre AS cliente
FROM
    documento_venta d
JOIN cliente c ON
    d.idCliente = c.idCliente

WHERE d.idDocumentoVenta = documentoVentaId;

                
END$$

DROP PROCEDURE IF EXISTS `sp_DocumentoVenta_Insert`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_DocumentoVenta_Insert` (IN `CLIENTE` INT, IN `FECHA` DATETIME, IN `SUBTOTAL` DOUBLE, IN `IGV` DOUBLE, IN `TOTAL` DOUBLE, IN `EMPLEADO` INT, IN `TIPO_DOCUMENTO` VARCHAR(20))  BEGIN	
	
	DECLARE SERIE VARCHAR (11);
    DECLARE NUMERO VARCHAR(11);
	
    SET SERIE = (SELECT CONCAT('0', SUBSTRING(CONCAT ('000', SUBSTRING(MAX(documento_venta_serie),-3) + 1),-3)) FROM `documento_venta` );
    
	SET NUMERO = (SELECT CONCAT('0', SUBSTRING(CONCAT ('000', SUBSTRING(MAX(documento_venta_numero),-3) + 1),-3)) FROM `documento_venta` );
   	
   
INSERT INTO `documento_venta`(`documento_venta_serie`, `documento_venta_numero`, `idCliente`, `fecha`, `estado`, `subtotal`, `igv`, `total` ,`idEmpleado`, `tipo_documento`)
 VALUES ( SERIE ,NUMERO, CLIENTE,FECHA ,'Pendiente', SUBTOTAL ,IGV , TOTAL ,EMPLEADO, TIPO_DOCUMENTO);

END$$

DROP PROCEDURE IF EXISTS `sp_Empleado_Ventas`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Empleado_Ventas` ()  BEGIN
SELECT DISTINCT e.* FROM empleado e INNER JOIN documento_venta v ON e.idEmpleado = v.idEmpleado WHERE v.estado = 'Pagado';
END$$

DROP PROCEDURE IF EXISTS `sp_Inventario_Ingreso_Select`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Inventario_Ingreso_Select` ()  BEGIN

SELECT i.*, CONCAT(a.nombre, ' - ', a.descripcion) as articulo, a.codigo as codigo ,a.stock as stock
FROM inventario i
LEFT JOIN orden_compra o ON i.idOrdenCompra = o.idOrdenCompra
LEFT JOIN orden_compra_detalle d ON d.idOrdenCompra = o.idOrdenCompra
LEFT JOIN articulo a ON a.idArticulo = i.idArticulo
WHERE tipo_movimiento = 'Ingreso'
AND i.idArticulo = d.idArticulo;


END$$

DROP PROCEDURE IF EXISTS `sp_Inventario_Insert`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Inventario_Insert` (IN `ArticuloId` INT, IN `TipoMovimiento` VARCHAR(45), IN `StockIn` INT, IN `StockOut` INT, IN `idOrdenCompra` INT, IN `idDocumentoVenta` INT, IN `Descripcion` VARCHAR(100), IN `PrecioUnitario` DOUBLE)  BEGIN	

INSERT INTO `inventario` (`idArticulo`, `fecha`, `tipo_movimiento`, `stock_in`, `stock_out`, `idOrdenCompra`, `idDocumentoVenta`, `descripcion`, `precioUnitario`)
VALUES (ArticuloId, CURRENT_TIMESTAMP, TipoMovimiento, StockIn, StockOut, idOrdenCompra, idDocumentoVenta, Descripcion, PrecioUnitario );

END$$

DROP PROCEDURE IF EXISTS `sp_Inventario_Salida_Select`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Inventario_Salida_Select` ()  BEGIN

SELECT i.*, CONCAT(a.nombre, ' - ', a.descripcion) as articulo,  a.codigo as codigo,  a.stock as stock 
FROM inventario i
LEFT JOIN orden_compra o ON i.idOrdenCompra = o.idOrdenCompra
LEFT JOIN orden_compra_detalle d ON d.idOrdenCompra = o.idOrdenCompra
LEFT JOIN articulo a ON a.idArticulo = i.idArticulo
WHERE tipo_movimiento = 'Salida'
AND i.idArticulo = d.idArticulo;


END$$

DROP PROCEDURE IF EXISTS `sp_Inventario_Select`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Inventario_Select` ()  BEGIN

SELECT i.*, a.nombre AS articulo, a.codigo as codigo,
a.stock as stock FROM inventario i 
JOIN articulo a ON i.idArticulo = a.idArticulo;

END$$

DROP PROCEDURE IF EXISTS `SP_ObtenerFechasCumpleEmple`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ObtenerFechasCumpleEmple` ()  BEGIN
SELECT
 idEmpleado,
 codigo,
 apellidos,
 nombre,
 dni,
 direccion,
 telefono,
 email,
 fecha_nacimiento,
 estado,
 DAY(fecha_nacimiento) AS 'dia',
 MONTH(fecha_nacimiento) AS 'mes',
 YEAR(CURDATE()) as 'aÃ±o'
 FROM empleado
 WHERE 
    MAKEDATE(YEAR(CURDATE()), DAYOFYEAR(fecha_nacimiento)) BETWEEN CURDATE() AND  ADDDATE(CURDATE(), 10)
    AND MAKEDATE(YEAR(CURDATE()), DAYOFYEAR(fecha_nacimiento)) BETWEEN  CURDATE() AND ADDDATE(CURDATE(), 10)
    and estado=1
 ORDER BY MONTH(fecha_nacimiento), DAY(fecha_nacimiento);
 END$$

DROP PROCEDURE IF EXISTS `sp_OrdenCompraDetalle_Insert`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_OrdenCompraDetalle_Insert` (IN `compraId` INT(11), IN `articuloId` INT(11), IN `cantidad` INT(11), IN `precio` DOUBLE)  BEGIN


INSERT INTO `orden_compra_detalle` (`idOrdenCompra`, `idArticulo`, `cantidad`, `precio`) 

VALUES (compraId, articuloId, cantidad, precio);

    
    
END$$

DROP PROCEDURE IF EXISTS `sp_OrdenCompraDetalle_Select`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_OrdenCompraDetalle_Select` (IN `compraId` INT)  BEGIN	

SELECT d.* FROM orden_compra_detalle AS d WHERE d.idOrdenCompra =compraId;
                
END$$

DROP PROCEDURE IF EXISTS `sp_OrdenCompra_By_Historial_Anual`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_OrdenCompra_By_Historial_Anual` ()  BEGIN

SELECT
    t.*,
    p.nombre AS proveedor
FROM
    orden_compra t
JOIN proveedor p ON
    t.idProveedor = p.idProveedor
WHERE t.estado = 'Pagado'
AND YEAR(t.fecha) = YEAR(CURRENT_DATE());

END$$

DROP PROCEDURE IF EXISTS `sp_OrdenCompra_By_Historial_Mensual`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_OrdenCompra_By_Historial_Mensual` ()  BEGIN

SELECT
    t.*,
    p.nombre AS proveedor
FROM
    orden_compra t
JOIN proveedor p ON
    t.idProveedor = p.idProveedor
WHERE t.estado = 'Pagado'
AND MONTH(t.fecha) = MONTH(CURRENT_DATE())
AND YEAR(t.fecha) = YEAR(CURRENT_DATE());

END$$

DROP PROCEDURE IF EXISTS `sp_OrdenCompra_By_Historial_Quincena`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_OrdenCompra_By_Historial_Quincena` ()  BEGIN

SELECT
    t.*,
    p.nombre AS proveedor
FROM
    orden_compra t
JOIN proveedor p ON
    t.idProveedor = p.idProveedor
WHERE t.estado = 'Pagado'
AND t.fecha >= DATE_SUB(NOW(), INTERVAL 14 DAY);

END$$

DROP PROCEDURE IF EXISTS `sp_OrdenCompra_By_Historial_Semana`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_OrdenCompra_By_Historial_Semana` ()  BEGIN

SELECT
    t.*,
    p.nombre AS proveedor
FROM
    orden_compra t
JOIN proveedor p ON
    t.idProveedor = p.idProveedor
WHERE t.estado = 'Pagado'
AND YEARWEEK(t.fecha)=YEARWEEK(NOW());

END$$

DROP PROCEDURE IF EXISTS `sp_OrdenCompra_Insert`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_OrdenCompra_Insert` (IN `PROVEEDOR` INT, IN `FECHA` DATETIME, IN `EMPLEADO` INT)  BEGIN	
	DECLARE COD VARCHAR(45);

	SET COD = (SELECT CONCAT('0', SUBSTRING(CONCAT ('000', SUBSTRING(MAX(orden_compra_numero),-3) + 1),-3)) FROM `orden_compra` );
   
INSERT INTO `orden_compra` ( orden_compra_numero ,idProveedor ,fecha, estado ,idEmpleado) VALUES (COD, PROVEEDOR ,FECHA ,'Pendiente',EMPLEADO);

END$$

DROP PROCEDURE IF EXISTS `sp_OrdenCompra_Select`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_OrdenCompra_Select` ()  BEGIN

SELECT
    t.*,
    p.nombre AS proveedor
FROM
    orden_compra t
JOIN proveedor p ON
    t.idProveedor = p.idProveedor;

END$$

DROP PROCEDURE IF EXISTS `sp_Pagos_Insert`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Pagos_Insert` (IN `DocumentoVenta` INT, IN `FECHA` DATE, IN `MONTO` DOUBLE, IN `TIPO_PAGO` VARCHAR(45))  BEGIN	

INSERT INTO `pagos` (`idDocumentoVenta`, `fecha`, `monto`, `tipo_pago`) VALUES (DocumentoVenta, FECHA, MONTO, TIPO_PAGO);

END$$

DROP PROCEDURE IF EXISTS `sp_Unidad_Id`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Unidad_Id` (IN `unidadId` INT)  BEGIN
    SELECT
        u.*,
        c.nombre AS categoria
    FROM
        unidad_medida u
    JOIN categoria c ON
        u.idCategoria = c.idCategoria
    WHERE
        u.idUnidad = unidadId ;
        
        END$$

DROP PROCEDURE IF EXISTS `sp_Ventas_all_Empleado`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Ventas_all_Empleado` (IN `empleadoId` INT)  BEGIN

SELECT t.*, c.nombre AS cliente 
FROM `documento_venta` t 
JOIN cliente c ON
    t.idCliente = c.idCliente
WHERE t.estado = 'Pagado' 
AND t.idEmpleado = `empleadoId`;
                
END$$

DROP PROCEDURE IF EXISTS `sp_Ventas_By_Historial_Anual`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Ventas_By_Historial_Anual` ()  BEGIN

SELECT
    d.*,
    c.nombre AS cliente
FROM
    documento_venta d
JOIN cliente c ON
    d.idCliente = c.idCliente
WHERE YEAR(d.fecha) = YEAR(CURRENT_DATE());

END$$

DROP PROCEDURE IF EXISTS `sp_Ventas_By_Historial_Mensual`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Ventas_By_Historial_Mensual` ()  BEGIN

SELECT
    d.*,
    c.nombre AS cliente
FROM
    documento_venta d
JOIN cliente c ON
    d.idCliente = c.idCliente
WHERE MONTH(d.fecha) = MONTH(CURRENT_DATE())
AND YEAR(d.fecha) = YEAR(CURRENT_DATE());

END$$

DROP PROCEDURE IF EXISTS `sp_Ventas_By_Historial_Quincena`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Ventas_By_Historial_Quincena` ()  BEGIN

SELECT
    d.*,
    c.nombre AS cliente
FROM
    documento_venta d
JOIN cliente c ON
    d.idCliente = c.idCliente
WHERE d.fecha >= DATE_SUB(NOW(), INTERVAL 14 DAY);

END$$

DROP PROCEDURE IF EXISTS `sp_Ventas_By_Historial_Semana`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Ventas_By_Historial_Semana` ()  BEGIN

SELECT
    d.*,
    c.nombre AS cliente
FROM
    documento_venta d
JOIN cliente c ON
    d.idCliente = c.idCliente
WHERE YEARWEEK(d.fecha)=YEARWEEK(NOW());

END$$

DROP PROCEDURE IF EXISTS `sp_Ventas_Select`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Ventas_Select` ()  BEGIN

SELECT
    d.*,
    c.nombre AS cliente
FROM
    documento_venta d
JOIN cliente c ON
    d.idCliente = c.idCliente
ORDER BY d.fecha DESC;    

END$$

DROP PROCEDURE IF EXISTS `sp_widget_1_total_clientes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_widget_1_total_clientes` ()  BEGIN

SELECT COUNT(*) as total FROM cliente 
WHERE MONTH(fecha_creacion) = MONTH(CURRENT_DATE())
AND YEAR(fecha_creacion) = YEAR(CURRENT_DATE());
                
END$$

DROP PROCEDURE IF EXISTS `sp_widget_2_productos_vendidos`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_widget_2_productos_vendidos` ()  BEGIN

SELECT
    SUM(dv.cantidad) AS Vendidos
FROM
    `documento_venta_detalle` dv
JOIN documento_venta v ON
    v.idDocumentoVenta = dv.idDocumentoVenta
WHERE
    v.estado = "Pagado" AND MONTH(fecha) = MONTH(CURRENT_DATE()) AND YEAR(fecha) = YEAR(CURRENT_DATE());                
END$$

DROP PROCEDURE IF EXISTS `sp_widget_3_total_ventas_este_mes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_widget_3_total_ventas_este_mes` ()  BEGIN

SELECT COUNT(total)
FROM `documento_venta`
WHERE estado = 'Pagado'
AND MONTH(fecha) = MONTH(CURRENT_DATE())
AND YEAR(fecha) = YEAR(CURRENT_DATE());
                
END$$

DROP PROCEDURE IF EXISTS `sp_widget_4_total_vendido_mes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_widget_4_total_vendido_mes` ()  BEGIN

SELECT SUM(total)
FROM `documento_venta`
WHERE estado = 'Pagado'
AND MONTH(fecha) = MONTH(CURRENT_DATE())
AND YEAR(fecha) = YEAR(CURRENT_DATE());
                
END$$

DROP PROCEDURE IF EXISTS `sp_widget_chart_productos_mas_vendidos`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_widget_chart_productos_mas_vendidos` ()  BEGIN


SELECT
    CONCAT(a.nombre, ' - ', a.descripcion) as nombre,
    SUM(dv.cantidad) AS cantidad
FROM
    documento_venta_detalle dv
JOIN articulo a ON
    a.idArticulo = dv.idArticulo
JOIN documento_venta v ON
    v.idDocumentoVenta = dv.idDocumentoVenta
WHERE
    v.estado = "Pagado"
GROUP BY
    dv.idArticulo   
ORDER BY cantidad DESC  
LIMIT 0, 5;

                
END$$

DROP PROCEDURE IF EXISTS `sp_widget_chart_productos_menos_vendidos`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_widget_chart_productos_menos_vendidos` ()  BEGIN


SELECT
    CONCAT(a.nombre, ' - ', a.descripcion) as nombre,
    SUM(dv.cantidad) AS cantidad
FROM
    documento_venta_detalle dv
JOIN articulo a ON
    a.idArticulo = dv.idArticulo
JOIN documento_venta v ON
    v.idDocumentoVenta = dv.idDocumentoVenta
WHERE
    v.estado = "Pagado"
GROUP BY
    dv.idArticulo   
ORDER BY cantidad ASC
LIMIT 0, 5;

                
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `articulo`
--

DROP TABLE IF EXISTS `articulo`;
CREATE TABLE IF NOT EXISTS `articulo` (
  `idArticulo` int(11) NOT NULL AUTO_INCREMENT,
  `idCategoria` int(11) NOT NULL,
  `idUnidad` int(11) NOT NULL,
  `codigo` char(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(11) DEFAULT '1',
  `stock` int(11) NOT NULL DEFAULT '0',
  `precio` double NOT NULL,
  `precioProveedor` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`idArticulo`),
  KEY `fk_idCategoria` (`idCategoria`),
  KEY `fk_idUnidad` (`idUnidad`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `articulo`
--

INSERT INTO `articulo` (`idArticulo`, `idCategoria`, `idUnidad`, `codigo`, `nombre`, `descripcion`, `estado`, `stock`, `precio`, `precioProveedor`) VALUES
(1, 3, 1, 'ART-001', 'Coca cola', '1L', 1, 750, 3, 2.5),
(2, 2, 2, 'ART-002', 'Pilsen', '630ml', 1, 699, 5, 3),
(3, 2, 4, 'ART-003', 'Pilsen', '12 unidades 630ml', 1, 856, 50, 45);

-- --------------------------------------------------------

--
-- Table structure for table `auditoria`
--

DROP TABLE IF EXISTS `auditoria`;
CREATE TABLE IF NOT EXISTS `auditoria` (
  `idItem` int(11) NOT NULL AUTO_INCREMENT,
  `idOrdenCompra` int(11) DEFAULT '0',
  `idDocumentoVenta` int(11) DEFAULT '0',
  `fecha` datetime NOT NULL,
  `idEmpleado` int(11) NOT NULL,
  `estado` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idItem`)
) ENGINE=MyISAM AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auditoria`
--

INSERT INTO `auditoria` (`idItem`, `idOrdenCompra`, `idDocumentoVenta`, `fecha`, `idEmpleado`, `estado`, `descripcion`) VALUES
(1, 0, 2, '2018-11-28 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva venta.'),
(2, 0, 2, '2018-11-28 00:00:00', 1, 'Pagado', 'Se ha pagado el documento de venta.'),
(3, 0, 3, '2018-11-28 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva venta.'),
(4, 0, 4, '2018-11-29 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva venta.'),
(5, 0, 3, '2018-11-29 00:00:00', 1, 'Pagado', 'Se ha pagado el documento de venta.'),
(6, 0, 4, '2018-11-29 00:00:00', 1, 'Pagado', 'Se ha pagado el documento de venta.'),
(7, 0, 5, '2018-11-29 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva venta.'),
(8, 0, 5, '2018-11-29 00:00:00', 1, 'Pagado', 'Se ha pagado el documento de venta.'),
(9, 0, 6, '2018-11-29 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva venta.'),
(10, 0, 6, '2018-11-29 00:00:00', 1, 'Pagado', 'Se ha pagado el documento de venta.'),
(11, 0, 7, '2018-11-29 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva venta.'),
(12, 0, 7, '2018-11-29 00:00:00', 1, 'Pagado', 'Se ha pagado el documento de venta.'),
(13, 0, 8, '2018-11-29 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva venta.'),
(14, 0, 8, '2018-11-29 00:00:00', 1, 'Pagado', 'Se ha pagado el documento de venta.'),
(15, 0, 9, '2018-11-29 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva venta.'),
(16, 0, 10, '2018-11-29 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva venta.'),
(17, 0, 10, '2018-11-29 00:00:00', 1, 'Pagado', 'Se ha pagado el documento de venta.'),
(18, 0, 9, '2018-11-29 00:00:00', 1, 'Pagado', 'Se ha pagado el documento de venta.'),
(19, 0, 11, '2018-11-29 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva venta.'),
(20, 0, 12, '2018-11-29 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva venta.'),
(21, 0, 13, '2018-11-29 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva venta.'),
(22, 0, 14, '2018-11-29 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva venta.'),
(23, 0, 11, '2018-11-29 00:00:00', 1, 'Pagado', 'Se ha pagado el documento de venta.'),
(24, 0, 12, '2018-11-29 00:00:00', 1, 'Pagado', 'Se ha pagado el documento de venta.'),
(25, 0, 13, '2018-11-29 00:00:00', 1, 'Pagado', 'Se ha pagado el documento de venta.'),
(26, 0, 14, '2018-11-29 00:00:00', 1, 'Pagado', 'Se ha pagado el documento de venta.'),
(27, 0, 15, '2018-11-29 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva venta.'),
(28, 0, 16, '2018-11-29 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva venta.'),
(29, 0, 15, '2018-11-29 00:00:00', 1, 'Pagado', 'Se ha pagado el documento de venta.'),
(30, 0, 16, '2018-11-29 00:00:00', 1, 'Pagado', 'Se ha pagado el documento de venta.'),
(31, 0, 1, '2018-11-30 00:00:00', 1, 'Pagado', 'Se ha pagado el documento de venta.'),
(32, 0, 17, '2018-11-30 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva venta.'),
(33, 0, 18, '2018-11-30 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva venta.'),
(34, 0, 19, '2018-11-30 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva venta.'),
(35, 0, 20, '2018-11-30 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva venta.'),
(36, 0, 21, '2018-11-30 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva venta.'),
(37, 0, 22, '2018-11-30 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva venta.'),
(38, 0, 23, '2018-11-30 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva venta.'),
(39, 0, 24, '2018-11-30 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva venta.'),
(40, 0, 24, '2018-11-30 00:00:00', 1, 'Pagado', 'Se ha pagado el documento de venta.'),
(41, 0, 23, '2018-11-30 00:00:00', 1, 'Pagado', 'Se ha pagado el documento de venta.'),
(42, 2, 0, '2018-11-30 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva orden de compra.'),
(43, 2, 0, '2018-12-01 00:00:00', 1, 'Aprobado', 'Se ha aprobado una orden de compra.'),
(44, 2, 0, '2018-12-01 00:00:00', 1, 'Aprobado', 'Se ha aprobado una orden de compra.'),
(45, 2, 0, '2018-12-01 00:00:00', 1, 'Aprobado', 'Se ha aprobado una orden de compra.'),
(46, 2, 0, '2018-12-01 00:00:00', 1, 'Anulado', 'Se ha anulado la orden de compra.'),
(47, 2, 0, '2018-12-01 00:00:00', 1, 'Aprobado', 'Se ha aprobado una orden de compra.'),
(48, 2, 0, '2018-12-01 00:00:00', 1, 'Pagado', 'Se ha pagado la orden de compra.'),
(49, 0, 22, '2018-12-03 00:00:00', 1, 'Pagado', 'Se ha pagado el documento de venta.'),
(50, 3, 0, '2018-12-03 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva orden de compra.'),
(51, 3, 0, '2018-12-03 00:00:00', 1, 'Aprobado', 'Se ha aprobado una orden de compra.'),
(52, 3, 0, '2018-12-03 00:00:00', 1, 'Pagado', 'Se ha pagado la orden de compra.'),
(53, 0, 24, '2018-12-04 00:00:00', 1, 'Pagado', 'Se ha pagado el documento de venta.'),
(54, 0, 24, '2018-12-04 00:00:00', 1, 'Pagado', 'Se ha pagado el documento de venta.'),
(55, 0, 25, '2018-12-04 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva venta.'),
(56, 4, 0, '2018-12-04 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva orden de compra.'),
(57, 4, 0, '2018-12-04 00:00:00', 1, 'Aprobado', 'Se ha aprobado una orden de compra.'),
(58, 4, 0, '2018-12-04 00:00:00', 1, 'Pagado', 'Se ha pagado la orden de compra.'),
(59, 5, 0, '2019-06-11 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva orden de compra.'),
(60, 5, 0, '2019-06-11 00:00:00', 1, 'Aprobado', 'Se ha aprobado una orden de compra.'),
(61, 5, 0, '2019-06-11 00:00:00', 1, 'Pagado', 'Se ha pagado la orden de compra.'),
(62, 5, 0, '2019-06-11 00:00:00', 1, 'Recibido', 'Se ha recibido la orden de compra.'),
(63, 0, 26, '2019-06-11 00:00:00', 1, 'Pendiente', 'Se ha generado una nueva venta.'),
(64, 0, 26, '2019-06-11 00:00:00', 1, 'Pagado', 'Se ha pagado el documento de venta.');

-- --------------------------------------------------------

--
-- Table structure for table `calificacion`
--

DROP TABLE IF EXISTS `calificacion`;
CREATE TABLE IF NOT EXISTS `calificacion` (
  `idCalificacion` int(11) NOT NULL AUTO_INCREMENT,
  `idDocumentoVenta` int(11) NOT NULL,
  `idEmpleado` int(11) NOT NULL,
  `calificacion` int(11) NOT NULL,
  PRIMARY KEY (`idCalificacion`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `calificacion`
--

INSERT INTO `calificacion` (`idCalificacion`, `idDocumentoVenta`, `idEmpleado`, `calificacion`) VALUES
(1, 1, 1, 2),
(4, 2, 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
CREATE TABLE IF NOT EXISTS `categoria` (
  `idCategoria` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` char(7) DEFAULT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `estado` int(11) DEFAULT '1',
  PRIMARY KEY (`idCategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categoria`
--

INSERT INTO `categoria` (`idCategoria`, `codigo`, `nombre`, `estado`) VALUES
(1, 'CAT-001', 'Rehidratantes', 1),
(2, 'CAT-002', 'Cervezas', 1),
(3, 'CAT-003', 'Gaseosas', 1),
(4, 'CAT-004', 'Agua mineral', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
CREATE TABLE IF NOT EXISTS `cliente` (
  `idCliente` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` char(7) DEFAULT NULL,
  `tipo_persona` varchar(10) DEFAULT NULL,
  `nombre` varchar(45) NOT NULL,
  `tipo_documento` varchar(10) DEFAULT NULL,
  `numero_documento` varchar(15) DEFAULT NULL,
  `departamento` varchar(45) DEFAULT NULL,
  `provincia` varchar(45) DEFAULT NULL,
  `distrito` varchar(45) DEFAULT NULL,
  `direccion` varchar(60) DEFAULT NULL,
  `telefono` varchar(11) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `numero_cuenta` varchar(20) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT '1',
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idCliente`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cliente`
--

INSERT INTO `cliente` (`idCliente`, `codigo`, `tipo_persona`, `nombre`, `tipo_documento`, `numero_documento`, `departamento`, `provincia`, `distrito`, `direccion`, `telefono`, `email`, `numero_cuenta`, `estado`, `fecha_creacion`) VALUES
(1, 'CLI-001', 'CLIENTE', 'Juan Campos Sanchez', 'DNI', '79852101', 'Lima', 'Lima', 'Lima', 'AV .28 de Julio', '999558120', 'juancamppos@gmail.com', '122399658745', 1, '2018-12-04 17:37:06'),
(2, 'CLI-002', 'CLIENTE', 'Leticia Ramirez Perez', 'DNI', '78523012', 'Lima', 'Lima', 'Chorrillos', 'Av sol', '987512023', 'periciarp@gmail.com', '126345698745', 1, '2018-12-04 17:37:06'),
(3, 'CLI-003', 'CLIENTE', 'Pedro Gerardo Del Condado ', 'DNI', '78596301', 'Lima', 'Lima', 'Ate', 'Nicolas Ayllon', '78549012', 'pedritogdl@gmail.com', '147823651478', 1, '2018-12-04 17:37:06');

-- --------------------------------------------------------

--
-- Table structure for table `documento_venta`
--

DROP TABLE IF EXISTS `documento_venta`;
CREATE TABLE IF NOT EXISTS `documento_venta` (
  `idDocumentoVenta` int(11) NOT NULL AUTO_INCREMENT,
  `documento_venta_serie` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `documento_venta_numero` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `idCliente` int(11) NOT NULL,
  `fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estado` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `subtotal` double NOT NULL,
  `igv` double NOT NULL,
  `total` double NOT NULL,
  `idEmpleado` int(11) NOT NULL,
  `calificacion` int(11) NOT NULL DEFAULT '0',
  `tipo_documento` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Factura',
  `fecha_pagado` datetime NOT NULL,
  PRIMARY KEY (`idDocumentoVenta`),
  KEY `fk_idCliente` (`idCliente`),
  KEY `fk_idEmpleado` (`idEmpleado`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `documento_venta`
--

INSERT INTO `documento_venta` (`idDocumentoVenta`, `documento_venta_serie`, `documento_venta_numero`, `idCliente`, `fecha`, `estado`, `subtotal`, `igv`, `total`, `idEmpleado`, `calificacion`, `tipo_documento`, `fecha_pagado`) VALUES
(1, '0001', '0001', 1, '2018-11-28 00:00:00', 'Pagado', 500, 90, 590, 1, 0, 'Boleta', '2018-12-04 09:15:32'),
(2, '0002', '0002', 2, '2018-11-28 02:02:04', 'Pagado', 100, 18, 118, 1, 4, 'Factura', '2018-12-04 09:15:32'),
(17, '0017', '0017', 0, '2018-11-30 11:23:31', 'Pendiente', 5, 0.9, 5.9, 1, 0, 'Factura', '2018-12-04 09:15:32'),
(3, '0003', '0003', 3, '2018-11-28 13:23:41', 'Pagado', 250, 45, 295, 1, 0, 'Factura', '2018-12-04 09:15:32'),
(4, '0004', '0004', 2, '2018-11-29 00:00:00', 'Pagado', 500, 90, 590, 2, 0, 'Factura', '2018-12-04 09:15:32'),
(5, '0005', '0005', 2, '2018-11-22 00:00:00', 'Pagado', 650, 117, 767, 2, 0, 'Factura', '2018-12-04 09:15:32'),
(6, '0006', '0006', 3, '2018-11-22 00:00:00', 'Pagado', 360, 64.8, 424.8, 3, 0, 'Factura', '2018-12-04 09:15:32'),
(7, '0007', '0007', 1, '2018-11-22 00:00:00', 'Pagado', 360, 64.8, 424.8, 4, 0, 'Factura', '2018-12-04 09:15:32'),
(8, '0008', '0008', 3, '2018-11-22 00:00:00', 'Pagado', 60, 10.8, 70.8, 5, 0, 'Factura', '2018-12-04 09:15:32'),
(9, '0009', '0009', 2, '2018-11-22 00:00:00', 'Pagado', 60, 10.8, 70.8, 6, 0, 'Factura', '2018-12-04 09:15:32'),
(10, '0010', '0010', 3, '2018-11-22 00:00:00', 'Pagado', 60, 10.8, 70.8, 7, 0, 'Factura', '2018-12-04 09:15:32'),
(11, '0011', '0011', 1, '2018-11-29 10:45:06', 'Pagado', 100, 18, 118, 1, 0, 'Factura', '2018-12-04 09:15:32'),
(12, '0012', '0012', 3, '2018-11-13 01:07:00', 'Pagado', 100, 18, 188, 7, 0, 'Factura', '2018-12-04 09:15:32'),
(13, '0013', '0013', 2, '2018-11-22 07:07:00', 'Pagado', 100, 18, 188, 3, 0, 'Factura', '2018-12-04 09:15:32'),
(14, '0014', '0014', 3, '2018-11-29 10:51:00', 'Pagado', 100, 18, 188, 4, 0, 'Factura', '2018-12-04 09:15:32'),
(15, '0015', '0015', 1, '2018-11-29 10:52:00', 'Pagado', 100, 18, 188, 3, 0, 'Factura', '2018-12-04 09:15:32'),
(16, '0016', '0016', 2, '2018-11-29 10:54:00', 'Pagado', 100, 18, 188, 1, 0, 'Factura', '2018-12-04 09:15:32'),
(18, '0018', '0018', 1, '2018-11-30 11:25:45', 'Pendiente', 50, 9, 59, 1, 0, 'Factura', '2018-12-04 09:15:32'),
(19, '0019', '0019', 2, '2018-12-30 11:25:57', 'Pendiente', 50, 9, 59, 1, 0, 'Factura', '2018-12-04 09:15:32'),
(20, '0020', '0020', 3, '2018-12-30 11:27:32', 'Pendiente', 5, 0.9, 5.9, 1, 0, 'Factura', '2018-12-04 09:15:32'),
(21, '0021', '0021', 1, '2018-12-30 11:30:34', 'Pendiente', 5, 0.9, 5.9, 1, 0, 'Factura', '2018-12-04 09:15:32'),
(22, '0022', '0022', 2, '2018-12-30 11:32:12', 'Pagado', 5, 0.9, 5.9, 1, 0, 'Factura', '2018-12-04 09:15:32'),
(23, '0023', '0023', 2, '2018-12-30 11:43:10', 'Pagado', 50, 9, 59, 1, 0, 'Factura', '2018-12-04 09:15:32'),
(24, '0024', '0024', 2, '2018-12-30 11:44:08', 'Pagado', 50, 9, 59, 1, 0, 'Factura', '2018-12-04 00:00:00'),
(25, '0025', '0025', 1, '2018-12-04 18:33:06', 'Pendiente', 5000, 900, 5900, 1, 0, 'Factura', '0000-00-00 00:00:00'),
(26, '0026', '0026', 2, '2019-06-11 20:11:53', 'Pagado', 550, 99, 649, 1, 0, 'Factura', '2019-06-11 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `documento_venta_detalle`
--

DROP TABLE IF EXISTS `documento_venta_detalle`;
CREATE TABLE IF NOT EXISTS `documento_venta_detalle` (
  `idItem` int(11) NOT NULL AUTO_INCREMENT,
  `idDocumentoVenta` int(11) NOT NULL,
  `idArticulo` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` double NOT NULL,
  `descuento` double NOT NULL DEFAULT '0',
  `total` double NOT NULL,
  PRIMARY KEY (`idItem`),
  KEY `fk_idDocumentoVenta` (`idDocumentoVenta`),
  KEY `fk_idArticulo` (`idArticulo`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `documento_venta_detalle`
--

INSERT INTO `documento_venta_detalle` (`idItem`, `idDocumentoVenta`, `idArticulo`, `cantidad`, `precio`, `descuento`, `total`) VALUES
(1, 1, 3, 10, 50, 0, 500),
(2, 2, 3, 1, 100, 0, 100),
(3, 3, 2, 50, 5, 0, 250),
(4, 4, 2, 100, 5, 0, 500),
(5, 5, 2, 100, 5, 0, 500),
(6, 5, 1, 50, 3, 0, 150),
(7, 6, 2, 12, 5, 0, 60),
(8, 6, 1, 50, 3, 0, 150),
(9, 6, 3, 3, 50, 0, 150),
(10, 7, 2, 12, 5, 0, 60),
(11, 7, 1, 50, 3, 0, 150),
(12, 7, 3, 3, 50, 0, 150),
(13, 8, 2, 12, 5, 0, 60),
(14, 9, 2, 12, 5, 0, 60),
(15, 10, 2, 12, 5, 0, 60),
(16, 11, 3, 2, 50, 0, 100),
(17, 12, 3, 2, 50, 0, 100),
(18, 13, 3, 2, 50, 0, 100),
(19, 14, 3, 2, 50, 0, 100),
(20, 15, 3, 2, 50, 0, 100),
(21, 16, 3, 2, 50, 0, 100),
(22, 17, 2, 1, 5, 0, 5),
(23, 18, 3, 1, 50, 0, 50),
(24, 19, 3, 1, 50, 0, 50),
(25, 20, 2, 1, 5, 0, 5),
(26, 21, 2, 1, 5, 0, 5),
(27, 22, 2, 1, 5, 0, 5),
(28, 23, 3, 1, 50, 0, 50),
(29, 24, 3, 1, 50, 0, 50),
(30, 25, 3, 100, 50, 0, 5000),
(31, 26, 3, 11, 50, 0, 550);

-- --------------------------------------------------------

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
CREATE TABLE IF NOT EXISTS `empleado` (
  `idEmpleado` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` char(7) DEFAULT NULL,
  `apellidos` varchar(45) DEFAULT NULL,
  `nombre` varchar(40) DEFAULT NULL,
  `dni` int(11) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `telefono` varchar(11) DEFAULT NULL,
  `email` varchar(70) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `estado` int(11) DEFAULT '1',
  `calificacion` double DEFAULT '0',
  PRIMARY KEY (`idEmpleado`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `empleado`
--

INSERT INTO `empleado` (`idEmpleado`, `codigo`, `apellidos`, `nombre`, `dni`, `direccion`, `telefono`, `email`, `fecha_nacimiento`, `estado`, `calificacion`) VALUES
(1, 'EMP-001', 'Medina NapurÃ­', 'Esteban', 76514045, 'SJM', '946450293', 'esteban@gmail.com', '1998-08-02', 1, 3),
(2, 'EMP-002', 'Mendoza', 'Jhony', 85263012, 'CHORRILLOS', '987654125', 'jhony@gmail.com', '2018-11-30', 1, 0),
(3, 'EMP-003', 'Hilario', 'Adriana', 7963105, 'SJM', '996321052', 'adriana@gmail.com', '2018-11-29', 1, 0),
(4, 'EMP-004', 'Peralta', 'Pedro Manuel', 78839620, 'LIMA', '955632032', 'pedroPer@gmail.com', '2018-11-15', 1, 0),
(5, 'EMP-005', 'Sanchez Herrera', 'Alvaro', 71439620, 'LIMA', '961023559', 'shaguiZlanta9@gmail.com', '2018-11-15', 1, 0),
(6, 'EMP-006', 'Mendoza Segura', 'Cristian', 72039620, 'LIMA', '960023552', 'cr7ElRashoo@gmail.com', '1998-12-02', 1, 0),
(7, 'EMP-007', 'Vilchez', 'Diego', 78639620, 'LIMA', '988023559', 'diegoVilchez@gmail.com', '1998-12-10', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `inventario`
--

DROP TABLE IF EXISTS `inventario`;
CREATE TABLE IF NOT EXISTS `inventario` (
  `idInventario` int(11) NOT NULL AUTO_INCREMENT,
  `idArticulo` int(11) NOT NULL,
  `fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tipo_movimiento` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `stock_in` int(11) NOT NULL DEFAULT '0',
  `stock_out` int(11) NOT NULL DEFAULT '0',
  `idOrdenCompra` int(11) NOT NULL,
  `idDocumentoVenta` int(11) NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `precioUnitario` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`idInventario`),
  KEY `fk_idOrdenCompra` (`idOrdenCompra`),
  KEY `fk_idArticulo` (`idArticulo`),
  KEY `fk_idDocumentoVenta` (`idDocumentoVenta`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `inventario`
--

INSERT INTO `inventario` (`idInventario`, `idArticulo`, `fecha`, `tipo_movimiento`, `stock_in`, `stock_out`, `idOrdenCompra`, `idDocumentoVenta`, `descripcion`, `precioUnitario`) VALUES
(1, 3, '2018-11-28 12:01:10', 'Ingreso', 500, 0, 1, 0, 'Ingreso de artÃ­culo', 45),
(2, 3, '2018-11-28 12:06:29', 'Salida', 0, 1, 0, 2, 'Salida de artÃ­culo', 0),
(3, 2, '2018-11-29 10:03:12', 'Salida', 0, 50, 0, 3, 'Salida de articulo', 0),
(4, 2, '2018-11-29 10:03:50', 'Salida', 0, 100, 0, 4, 'Salida de articulo', 0),
(5, 2, '2018-11-29 10:09:57', 'Salida', 0, 100, 0, 5, 'Salida de articulo', 0),
(6, 1, '2018-11-29 10:09:57', 'Salida', 0, 50, 0, 5, 'Salida de articulo', 0),
(7, 2, '2018-11-29 10:18:54', 'Salida', 0, 12, 0, 6, 'Salida de articulo', 0),
(8, 1, '2018-11-29 10:18:54', 'Salida', 0, 50, 0, 6, 'Salida de articulo', 0),
(9, 3, '2018-11-29 10:18:54', 'Salida', 0, 3, 0, 6, 'Salida de articulo', 0),
(10, 2, '2018-11-29 10:23:59', 'Salida', 0, 12, 0, 7, 'Salida de articulo', 0),
(11, 1, '2018-11-29 10:23:59', 'Salida', 0, 50, 0, 7, 'Salida de articulo', 0),
(12, 3, '2018-11-29 10:23:59', 'Salida', 0, 3, 0, 7, 'Salida de articulo', 0),
(13, 2, '2018-11-29 10:33:29', 'Salida', 0, 12, 0, 8, 'Salida de articulo', 0),
(14, 2, '2018-11-29 10:41:03', 'Salida', 0, 12, 0, 10, 'Salida de articulo', 0),
(15, 2, '2018-11-29 10:41:12', 'Salida', 0, 12, 0, 9, 'Salida de articulo', 0),
(16, 3, '2018-11-29 10:47:56', 'Salida', 0, 2, 0, 11, 'Salida de articulo', 0),
(17, 3, '2018-11-29 10:48:00', 'Salida', 0, 2, 0, 12, 'Salida de articulo', 0),
(18, 3, '2018-11-29 10:48:09', 'Salida', 0, 2, 0, 13, 'Salida de articulo', 0),
(19, 3, '2018-11-29 10:51:31', 'Salida', 0, 2, 0, 14, 'Salida de articulo', 0),
(20, 3, '2018-11-29 10:54:03', 'Salida', 0, 2, 0, 15, 'Salida de articulo', 0),
(21, 3, '2018-11-29 10:54:05', 'Salida', 0, 2, 0, 16, 'Salida de articulo', 0),
(22, 3, '2018-11-30 10:37:14', 'Salida', 0, 10, 0, 1, 'Salida de articulo', 0),
(23, 3, '2018-11-30 11:45:54', 'Salida', 0, 1, 0, 24, 'Salida de articulo', 0),
(24, 3, '2018-11-30 11:54:19', 'Salida', 0, 1, 0, 23, 'Salida de articulo', 0),
(25, 1, '2018-12-01 10:37:47', 'Salida', 0, 100, 2, 0, 'Salida de articulo', 0),
(26, 2, '2018-12-03 10:04:59', 'Salida', 0, 1, 0, 22, 'Salida de articulo', 0),
(27, 3, '2018-12-04 09:18:12', 'Salida', 0, 1, 0, 24, 'Salida de articulo', 0),
(28, 3, '2018-12-04 09:24:08', 'Salida', 0, 1, 0, 24, 'Salida de articulo', 0),
(29, 2, '2019-06-11 20:08:40', 'Ingreso', 10, 0, 5, 0, 'Ingreso de articulo', 3),
(30, 3, '2019-06-11 20:11:55', 'Salida', 0, 11, 0, 26, 'Salida de articulo', 50);

-- --------------------------------------------------------

--
-- Table structure for table `orden_compra`
--

DROP TABLE IF EXISTS `orden_compra`;
CREATE TABLE IF NOT EXISTS `orden_compra` (
  `idOrdenCompra` int(11) NOT NULL AUTO_INCREMENT,
  `orden_compra_numero` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `idProveedor` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `estado` varchar(120) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `idEmpleado` int(11) NOT NULL,
  PRIMARY KEY (`idOrdenCompra`),
  KEY `fk_idProveedor` (`idProveedor`),
  KEY `fk_idEmpleado` (`idEmpleado`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orden_compra`
--

INSERT INTO `orden_compra` (`idOrdenCompra`, `orden_compra_numero`, `idProveedor`, `fecha`, `estado`, `idEmpleado`) VALUES
(1, '0001', 1, '2018-11-28 00:00:00', 'Pendiente', 1),
(2, '0002', 2, '2018-11-30 00:00:00', 'Pagado', 1),
(3, '0003', 4, '2018-12-03 00:00:00', 'Pagado', 1),
(4, '0004', 3, '2018-12-04 00:00:00', 'Pagado', 1),
(5, '0005', 2, '2019-06-11 00:00:00', 'Recibido', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orden_compra_detalle`
--

DROP TABLE IF EXISTS `orden_compra_detalle`;
CREATE TABLE IF NOT EXISTS `orden_compra_detalle` (
  `idOrdenCompraDetalle` int(11) NOT NULL AUTO_INCREMENT,
  `idOrdenCompra` int(11) NOT NULL,
  `idArticulo` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` double NOT NULL,
  PRIMARY KEY (`idOrdenCompraDetalle`),
  KEY `fk_idOrdenCompra` (`idOrdenCompra`),
  KEY `fk_idArticulo` (`idArticulo`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orden_compra_detalle`
--

INSERT INTO `orden_compra_detalle` (`idOrdenCompraDetalle`, `idOrdenCompra`, `idArticulo`, `cantidad`, `precio`) VALUES
(1, 1, 3, 900, 45000),
(2, 2, 1, 100, 2.5),
(3, 3, 3, 20, 45),
(4, 4, 3, 100, 45),
(5, 5, 2, 10, 3);

-- --------------------------------------------------------

--
-- Table structure for table `pagos`
--

DROP TABLE IF EXISTS `pagos`;
CREATE TABLE IF NOT EXISTS `pagos` (
  `idPago` int(11) NOT NULL AUTO_INCREMENT,
  `idDocumentoVenta` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `monto` double NOT NULL,
  `tipo_pago` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idPago`),
  KEY `fk_idDocumentoVenta` (`idDocumentoVenta`),
  KEY `fk_fecha` (`fecha`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pagos`
--

INSERT INTO `pagos` (`idPago`, `idDocumentoVenta`, `fecha`, `monto`, `tipo_pago`) VALUES
(1, 2, '2018-11-28 00:00:00', 118, 'Pagado'),
(2, 3, '2018-11-29 00:00:00', 295, 'Pagado'),
(3, 4, '2018-11-29 00:00:00', 590, 'Pagado'),
(4, 5, '2018-11-29 00:00:00', 767, 'Pagado'),
(5, 6, '2018-11-29 00:00:00', 424.8, 'Pagado'),
(6, 7, '2018-11-29 00:00:00', 424.8, 'Pagado'),
(7, 8, '2018-11-29 00:00:00', 70.8, 'Pagado'),
(8, 10, '2018-11-29 00:00:00', 70.8, 'Pagado'),
(9, 9, '2018-11-29 00:00:00', 70.8, 'Pagado'),
(10, 11, '2018-11-29 00:00:00', 118, 'Pagado'),
(11, 12, '2018-11-29 00:00:00', 188, 'Pagado'),
(12, 13, '2018-11-29 00:00:00', 188, 'Pagado'),
(13, 14, '2018-11-29 00:00:00', 188, 'Pagado'),
(14, 15, '2018-11-29 00:00:00', 188, 'Pagado'),
(15, 16, '2018-11-29 00:00:00', 188, 'Pagado'),
(16, 1, '2018-11-30 00:00:00', 590, 'Pagado'),
(17, 24, '2018-11-30 00:00:00', 59, 'Pagado'),
(18, 23, '2018-11-30 00:00:00', 59, 'Pagado'),
(19, 22, '2018-12-03 00:00:00', 5.9, 'Pagado'),
(20, 24, '2018-12-04 00:00:00', 59, 'Pagado'),
(21, 24, '2018-12-04 00:00:00', 59, 'Pagado'),
(22, 26, '2019-06-11 00:00:00', 649, 'Pagado');

-- --------------------------------------------------------

--
-- Table structure for table `proveedor`
--

DROP TABLE IF EXISTS `proveedor`;
CREATE TABLE IF NOT EXISTS `proveedor` (
  `idProveedor` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(7) DEFAULT NULL,
  `tipo_persona` varchar(10) DEFAULT NULL,
  `nombre` varchar(45) NOT NULL,
  `tipo_documento` varchar(10) DEFAULT NULL,
  `numero_documento` varchar(15) DEFAULT NULL,
  `departamento` varchar(45) DEFAULT NULL,
  `provincia` varchar(45) DEFAULT NULL,
  `distrito` varchar(45) DEFAULT NULL,
  `direccion` varchar(60) DEFAULT NULL,
  `telefono` varchar(11) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `numero_cuenta` varchar(20) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idProveedor`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `proveedor`
--

INSERT INTO `proveedor` (`idProveedor`, `codigo`, `tipo_persona`, `nombre`, `tipo_documento`, `numero_documento`, `departamento`, `provincia`, `distrito`, `direccion`, `telefono`, `email`, `numero_cuenta`, `estado`) VALUES
(1, 'PRV-001', 'PROVEEDOR', 'Backus ', 'RUC', '120369558742', 'LIMA', 'LIMA', 'LIMA', 'Av. NicolÃ¡s AyllÃ³n 3986, Ate 15012', '311-3000', 'BACKUS@gmail.com', '123465987412', 1),
(2, 'PRV-002', 'PROVEEDOR', 'Coca cola SAC', 'RUC', '12361523698', 'Lima', 'Lima', 'Surquillo', 'Avenida RepÃºblica de PanamÃ¡ 4050', '4114200', 'cocacola@gmail.com', '125385239652', 1),
(3, 'PRV-003', 'PROVEEDOR', 'Pepsico Inc Sucursal del PerÃº. ', 'Ruc', '11659647852', 'Lima', 'Lima', 'San Isidro', 'Calle Esquilache 371 Int. 15', '711-7111', 'pepsico@gmail.com', '123654789632', 1),
(4, 'PRV-004', 'PROVEEDOR', 'Comercial Huarcaya', 'RUC', '12365478952', 'Lima', 'Lima', 'La Victoria', 'Avenida Las AmÃ©ricas 1281  A', '623 - 1215', 'ventas@grupohuarcaya.com', '123658749520', 1);

-- --------------------------------------------------------

--
-- Table structure for table `unidad_medida`
--

DROP TABLE IF EXISTS `unidad_medida`;
CREATE TABLE IF NOT EXISTS `unidad_medida` (
  `idUnidad` int(11) NOT NULL AUTO_INCREMENT,
  `idCategoria` int(11) NOT NULL,
  `codigo` varchar(7) DEFAULT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `prefijo` varchar(5) NOT NULL,
  `estado` int(11) DEFAULT '1',
  PRIMARY KEY (`idUnidad`),
  KEY `fk_idCategoria` (`idCategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `unidad_medida`
--

INSERT INTO `unidad_medida` (`idUnidad`, `idCategoria`, `codigo`, `nombre`, `prefijo`, `estado`) VALUES
(1, 3, 'UNI-001', 'Litro', 'Lt', 1),
(2, 2, 'UNI-002', 'Medio Litr', '1/2L', 1),
(3, 2, 'UNI-003', 'Unidad', 'Ud', 1),
(4, 2, 'UNI-004', 'Caja', 'caja', 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
